package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserSetting;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userSetting")
@XmlType(propOrder = {"guid", "user", "homePage", "selfBio", "userUrlDomain", "userUrlDomainNormalized", "autoRedirectEnabled", "viewEnabled", "shareEnabled", "defaultDomain", "defaultTokenType", "defaultRedirectType", "defaultFlashDuration", "defaultAccessType", "defaultViewType", "defaultShareType", "defaultPassphrase", "defaultSignature", "useSignature", "defaultEmblem", "useEmblem", "defaultBackground", "useBackground", "sponsorUrl", "sponsorBanner", "sponsorNote", "useSponsor", "extraParams", "expirationDuration", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserSettingStub extends PersonalSettingStub implements UserSetting, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserSettingStub.class.getName());

    private String user;
    private String homePage;
    private String selfBio;
    private String userUrlDomain;
    private String userUrlDomainNormalized;
    private Boolean autoRedirectEnabled;
    private Boolean viewEnabled;
    private Boolean shareEnabled;
    private String defaultDomain;
    private String defaultTokenType;
    private String defaultRedirectType;
    private Long defaultFlashDuration;
    private String defaultAccessType;
    private String defaultViewType;
    private String defaultShareType;
    private String defaultPassphrase;
    private String defaultSignature;
    private Boolean useSignature;
    private String defaultEmblem;
    private Boolean useEmblem;
    private String defaultBackground;
    private Boolean useBackground;
    private String sponsorUrl;
    private String sponsorBanner;
    private String sponsorNote;
    private Boolean useSponsor;
    private String extraParams;
    private Long expirationDuration;

    public UserSettingStub()
    {
        this(null);
    }
    public UserSettingStub(UserSetting bean)
    {
        super(bean);
        if(bean != null) {
            this.user = bean.getUser();
            this.homePage = bean.getHomePage();
            this.selfBio = bean.getSelfBio();
            this.userUrlDomain = bean.getUserUrlDomain();
            this.userUrlDomainNormalized = bean.getUserUrlDomainNormalized();
            this.autoRedirectEnabled = bean.isAutoRedirectEnabled();
            this.viewEnabled = bean.isViewEnabled();
            this.shareEnabled = bean.isShareEnabled();
            this.defaultDomain = bean.getDefaultDomain();
            this.defaultTokenType = bean.getDefaultTokenType();
            this.defaultRedirectType = bean.getDefaultRedirectType();
            this.defaultFlashDuration = bean.getDefaultFlashDuration();
            this.defaultAccessType = bean.getDefaultAccessType();
            this.defaultViewType = bean.getDefaultViewType();
            this.defaultShareType = bean.getDefaultShareType();
            this.defaultPassphrase = bean.getDefaultPassphrase();
            this.defaultSignature = bean.getDefaultSignature();
            this.useSignature = bean.isUseSignature();
            this.defaultEmblem = bean.getDefaultEmblem();
            this.useEmblem = bean.isUseEmblem();
            this.defaultBackground = bean.getDefaultBackground();
            this.useBackground = bean.isUseBackground();
            this.sponsorUrl = bean.getSponsorUrl();
            this.sponsorBanner = bean.getSponsorBanner();
            this.sponsorNote = bean.getSponsorNote();
            this.useSponsor = bean.isUseSponsor();
            this.extraParams = bean.getExtraParams();
            this.expirationDuration = bean.getExpirationDuration();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getHomePage()
    {
        return this.homePage;
    }
    public void setHomePage(String homePage)
    {
        this.homePage = homePage;
    }

    @XmlElement
    public String getSelfBio()
    {
        return this.selfBio;
    }
    public void setSelfBio(String selfBio)
    {
        this.selfBio = selfBio;
    }

    @XmlElement
    public String getUserUrlDomain()
    {
        return this.userUrlDomain;
    }
    public void setUserUrlDomain(String userUrlDomain)
    {
        this.userUrlDomain = userUrlDomain;
    }

    @XmlElement
    public String getUserUrlDomainNormalized()
    {
        return this.userUrlDomainNormalized;
    }
    public void setUserUrlDomainNormalized(String userUrlDomainNormalized)
    {
        this.userUrlDomainNormalized = userUrlDomainNormalized;
    }

    @XmlElement
    public Boolean isAutoRedirectEnabled()
    {
        return this.autoRedirectEnabled;
    }
    public void setAutoRedirectEnabled(Boolean autoRedirectEnabled)
    {
        this.autoRedirectEnabled = autoRedirectEnabled;
    }

    @XmlElement
    public Boolean isViewEnabled()
    {
        return this.viewEnabled;
    }
    public void setViewEnabled(Boolean viewEnabled)
    {
        this.viewEnabled = viewEnabled;
    }

    @XmlElement
    public Boolean isShareEnabled()
    {
        return this.shareEnabled;
    }
    public void setShareEnabled(Boolean shareEnabled)
    {
        this.shareEnabled = shareEnabled;
    }

    @XmlElement
    public String getDefaultDomain()
    {
        return this.defaultDomain;
    }
    public void setDefaultDomain(String defaultDomain)
    {
        this.defaultDomain = defaultDomain;
    }

    @XmlElement
    public String getDefaultTokenType()
    {
        return this.defaultTokenType;
    }
    public void setDefaultTokenType(String defaultTokenType)
    {
        this.defaultTokenType = defaultTokenType;
    }

    @XmlElement
    public String getDefaultRedirectType()
    {
        return this.defaultRedirectType;
    }
    public void setDefaultRedirectType(String defaultRedirectType)
    {
        this.defaultRedirectType = defaultRedirectType;
    }

    @XmlElement
    public Long getDefaultFlashDuration()
    {
        return this.defaultFlashDuration;
    }
    public void setDefaultFlashDuration(Long defaultFlashDuration)
    {
        this.defaultFlashDuration = defaultFlashDuration;
    }

    @XmlElement
    public String getDefaultAccessType()
    {
        return this.defaultAccessType;
    }
    public void setDefaultAccessType(String defaultAccessType)
    {
        this.defaultAccessType = defaultAccessType;
    }

    @XmlElement
    public String getDefaultViewType()
    {
        return this.defaultViewType;
    }
    public void setDefaultViewType(String defaultViewType)
    {
        this.defaultViewType = defaultViewType;
    }

    @XmlElement
    public String getDefaultShareType()
    {
        return this.defaultShareType;
    }
    public void setDefaultShareType(String defaultShareType)
    {
        this.defaultShareType = defaultShareType;
    }

    @XmlElement
    public String getDefaultPassphrase()
    {
        return this.defaultPassphrase;
    }
    public void setDefaultPassphrase(String defaultPassphrase)
    {
        this.defaultPassphrase = defaultPassphrase;
    }

    @XmlElement
    public String getDefaultSignature()
    {
        return this.defaultSignature;
    }
    public void setDefaultSignature(String defaultSignature)
    {
        this.defaultSignature = defaultSignature;
    }

    @XmlElement
    public Boolean isUseSignature()
    {
        return this.useSignature;
    }
    public void setUseSignature(Boolean useSignature)
    {
        this.useSignature = useSignature;
    }

    @XmlElement
    public String getDefaultEmblem()
    {
        return this.defaultEmblem;
    }
    public void setDefaultEmblem(String defaultEmblem)
    {
        this.defaultEmblem = defaultEmblem;
    }

    @XmlElement
    public Boolean isUseEmblem()
    {
        return this.useEmblem;
    }
    public void setUseEmblem(Boolean useEmblem)
    {
        this.useEmblem = useEmblem;
    }

    @XmlElement
    public String getDefaultBackground()
    {
        return this.defaultBackground;
    }
    public void setDefaultBackground(String defaultBackground)
    {
        this.defaultBackground = defaultBackground;
    }

    @XmlElement
    public Boolean isUseBackground()
    {
        return this.useBackground;
    }
    public void setUseBackground(Boolean useBackground)
    {
        this.useBackground = useBackground;
    }

    @XmlElement
    public String getSponsorUrl()
    {
        return this.sponsorUrl;
    }
    public void setSponsorUrl(String sponsorUrl)
    {
        this.sponsorUrl = sponsorUrl;
    }

    @XmlElement
    public String getSponsorBanner()
    {
        return this.sponsorBanner;
    }
    public void setSponsorBanner(String sponsorBanner)
    {
        this.sponsorBanner = sponsorBanner;
    }

    @XmlElement
    public String getSponsorNote()
    {
        return this.sponsorNote;
    }
    public void setSponsorNote(String sponsorNote)
    {
        this.sponsorNote = sponsorNote;
    }

    @XmlElement
    public Boolean isUseSponsor()
    {
        return this.useSponsor;
    }
    public void setUseSponsor(Boolean useSponsor)
    {
        this.useSponsor = useSponsor;
    }

    @XmlElement
    public String getExtraParams()
    {
        return this.extraParams;
    }
    public void setExtraParams(String extraParams)
    {
        this.extraParams = extraParams;
    }

    @XmlElement
    public Long getExpirationDuration()
    {
        return this.expirationDuration;
    }
    public void setExpirationDuration(Long expirationDuration)
    {
        this.expirationDuration = expirationDuration;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("user", this.user);
        dataMap.put("homePage", this.homePage);
        dataMap.put("selfBio", this.selfBio);
        dataMap.put("userUrlDomain", this.userUrlDomain);
        dataMap.put("userUrlDomainNormalized", this.userUrlDomainNormalized);
        dataMap.put("autoRedirectEnabled", this.autoRedirectEnabled);
        dataMap.put("viewEnabled", this.viewEnabled);
        dataMap.put("shareEnabled", this.shareEnabled);
        dataMap.put("defaultDomain", this.defaultDomain);
        dataMap.put("defaultTokenType", this.defaultTokenType);
        dataMap.put("defaultRedirectType", this.defaultRedirectType);
        dataMap.put("defaultFlashDuration", this.defaultFlashDuration);
        dataMap.put("defaultAccessType", this.defaultAccessType);
        dataMap.put("defaultViewType", this.defaultViewType);
        dataMap.put("defaultShareType", this.defaultShareType);
        dataMap.put("defaultPassphrase", this.defaultPassphrase);
        dataMap.put("defaultSignature", this.defaultSignature);
        dataMap.put("useSignature", this.useSignature);
        dataMap.put("defaultEmblem", this.defaultEmblem);
        dataMap.put("useEmblem", this.useEmblem);
        dataMap.put("defaultBackground", this.defaultBackground);
        dataMap.put("useBackground", this.useBackground);
        dataMap.put("sponsorUrl", this.sponsorUrl);
        dataMap.put("sponsorBanner", this.sponsorBanner);
        dataMap.put("sponsorNote", this.sponsorNote);
        dataMap.put("useSponsor", this.useSponsor);
        dataMap.put("extraParams", this.extraParams);
        dataMap.put("expirationDuration", this.expirationDuration);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = homePage == null ? 0 : homePage.hashCode();
        _hash = 31 * _hash + delta;
        delta = selfBio == null ? 0 : selfBio.hashCode();
        _hash = 31 * _hash + delta;
        delta = userUrlDomain == null ? 0 : userUrlDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = userUrlDomainNormalized == null ? 0 : userUrlDomainNormalized.hashCode();
        _hash = 31 * _hash + delta;
        delta = autoRedirectEnabled == null ? 0 : autoRedirectEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = viewEnabled == null ? 0 : viewEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = shareEnabled == null ? 0 : shareEnabled.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultDomain == null ? 0 : defaultDomain.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultTokenType == null ? 0 : defaultTokenType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultRedirectType == null ? 0 : defaultRedirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultFlashDuration == null ? 0 : defaultFlashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultAccessType == null ? 0 : defaultAccessType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultViewType == null ? 0 : defaultViewType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultShareType == null ? 0 : defaultShareType.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultPassphrase == null ? 0 : defaultPassphrase.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultSignature == null ? 0 : defaultSignature.hashCode();
        _hash = 31 * _hash + delta;
        delta = useSignature == null ? 0 : useSignature.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultEmblem == null ? 0 : defaultEmblem.hashCode();
        _hash = 31 * _hash + delta;
        delta = useEmblem == null ? 0 : useEmblem.hashCode();
        _hash = 31 * _hash + delta;
        delta = defaultBackground == null ? 0 : defaultBackground.hashCode();
        _hash = 31 * _hash + delta;
        delta = useBackground == null ? 0 : useBackground.hashCode();
        _hash = 31 * _hash + delta;
        delta = sponsorUrl == null ? 0 : sponsorUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = sponsorBanner == null ? 0 : sponsorBanner.hashCode();
        _hash = 31 * _hash + delta;
        delta = sponsorNote == null ? 0 : sponsorNote.hashCode();
        _hash = 31 * _hash + delta;
        delta = useSponsor == null ? 0 : useSponsor.hashCode();
        _hash = 31 * _hash + delta;
        delta = extraParams == null ? 0 : extraParams.hashCode();
        _hash = 31 * _hash + delta;
        delta = expirationDuration == null ? 0 : expirationDuration.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static UserSettingStub convertBeanToStub(UserSetting bean)
    {
        UserSettingStub stub = null;
        if(bean instanceof UserSettingStub) {
            stub = (UserSettingStub) bean;
        } else {
            if(bean != null) {
                stub = new UserSettingStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserSettingStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserSettingStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserSettingStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserSettingStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserSettingStub object as a string.", e);
        }
        
        return null;
    }
    public static UserSettingStub fromJsonString(String jsonStr)
    {
        try {
            UserSettingStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserSettingStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserSettingStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserSettingStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserSettingStub object.", e);
        }
        
        return null;
    }

}
