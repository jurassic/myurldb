package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.UserUsercode;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "userUsercodes")
@XmlType(propOrder = {"userUsercode"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserUsercodeListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(UserUsercodeListStub.class.getName());

    private List<UserUsercodeStub> userUsercodes = null;

    public UserUsercodeListStub()
    {
        this(new ArrayList<UserUsercodeStub>());
    }
    public UserUsercodeListStub(List<UserUsercodeStub> userUsercodes)
    {
        this.userUsercodes = userUsercodes;
    }

    public boolean isEmpty()
    {
        if(userUsercodes == null) {
            return true;
        } else {
            return userUsercodes.isEmpty();
        }
    }
    public int getSize()
    {
        if(userUsercodes == null) {
            return 0;
        } else {
            return userUsercodes.size();
        }
    }

    @XmlElement(name = "userUsercode")
    public List<UserUsercodeStub> getUserUsercode()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<UserUsercodeStub> getList()
    {
        return userUsercodes;
    }
    public void setList(List<UserUsercodeStub> userUsercodes)
    {
        this.userUsercodes = userUsercodes;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<UserUsercodeStub> it = this.userUsercodes.iterator();
        while(it.hasNext()) {
            UserUsercodeStub userUsercode = it.next();
            sb.append(userUsercode.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static UserUsercodeListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of UserUsercodeListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write UserUsercodeListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write UserUsercodeListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write UserUsercodeListStub object as a string.", e);
        }
        
        return null;
    }
    public static UserUsercodeListStub fromJsonString(String jsonStr)
    {
        try {
            UserUsercodeListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, UserUsercodeListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into UserUsercodeListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into UserUsercodeListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into UserUsercodeListStub object.", e);
        }
        
        return null;
    }

}
