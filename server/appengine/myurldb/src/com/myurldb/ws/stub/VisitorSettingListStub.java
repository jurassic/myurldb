package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "visitorSettings")
@XmlType(propOrder = {"visitorSetting"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class VisitorSettingListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VisitorSettingListStub.class.getName());

    private List<VisitorSettingStub> visitorSettings = null;

    public VisitorSettingListStub()
    {
        this(new ArrayList<VisitorSettingStub>());
    }
    public VisitorSettingListStub(List<VisitorSettingStub> visitorSettings)
    {
        this.visitorSettings = visitorSettings;
    }

    public boolean isEmpty()
    {
        if(visitorSettings == null) {
            return true;
        } else {
            return visitorSettings.isEmpty();
        }
    }
    public int getSize()
    {
        if(visitorSettings == null) {
            return 0;
        } else {
            return visitorSettings.size();
        }
    }

    @XmlElement(name = "visitorSetting")
    public List<VisitorSettingStub> getVisitorSetting()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<VisitorSettingStub> getList()
    {
        return visitorSettings;
    }
    public void setList(List<VisitorSettingStub> visitorSettings)
    {
        this.visitorSettings = visitorSettings;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<VisitorSettingStub> it = this.visitorSettings.iterator();
        while(it.hasNext()) {
            VisitorSettingStub visitorSetting = it.next();
            sb.append(visitorSetting.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static VisitorSettingListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of VisitorSettingListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write VisitorSettingListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write VisitorSettingListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write VisitorSettingListStub object as a string.", e);
        }
        
        return null;
    }
    public static VisitorSettingListStub fromJsonString(String jsonStr)
    {
        try {
            VisitorSettingListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, VisitorSettingListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into VisitorSettingListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into VisitorSettingListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into VisitorSettingListStub object.", e);
        }
        
        return null;
    }

}
