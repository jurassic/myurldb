package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.VisitorSetting;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "visitorSetting")
@XmlType(propOrder = {"guid", "user", "redirectType", "flashDuration", "privacyType", "createdTime", "modifiedTime"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class VisitorSettingStub extends PersonalSettingStub implements VisitorSetting, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(VisitorSettingStub.class.getName());

    private String user;
    private String redirectType;
    private Long flashDuration;
    private String privacyType;

    public VisitorSettingStub()
    {
        this(null);
    }
    public VisitorSettingStub(VisitorSetting bean)
    {
        super(bean);
        if(bean != null) {
            this.user = bean.getUser();
            this.redirectType = bean.getRedirectType();
            this.flashDuration = bean.getFlashDuration();
            this.privacyType = bean.getPrivacyType();
        }
    }


    @XmlElement
    public String getGuid()
    {
        return super.getGuid();
    }
    public void setGuid(String guid)
    {
        super.setGuid(guid);
    }

    @XmlElement
    public String getUser()
    {
        return this.user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    @XmlElement
    public String getRedirectType()
    {
        return this.redirectType;
    }
    public void setRedirectType(String redirectType)
    {
        this.redirectType = redirectType;
    }

    @XmlElement
    public Long getFlashDuration()
    {
        return this.flashDuration;
    }
    public void setFlashDuration(Long flashDuration)
    {
        this.flashDuration = flashDuration;
    }

    @XmlElement
    public String getPrivacyType()
    {
        return this.privacyType;
    }
    public void setPrivacyType(String privacyType)
    {
        this.privacyType = privacyType;
    }

    @XmlElement
    public Long getCreatedTime()
    {
        return super.getCreatedTime();
    }
    public void setCreatedTime(Long createdTime)
    {
        super.setCreatedTime(createdTime);
    }

    @XmlElement
    public Long getModifiedTime()
    {
        return super.getModifiedTime();
    }
    public void setModifiedTime(Long modifiedTime)
    {
        super.setModifiedTime(modifiedTime);
    }


    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = super.getDataMap();

        dataMap.put("user", this.user);
        dataMap.put("redirectType", this.redirectType);
        dataMap.put("flashDuration", this.flashDuration);
        dataMap.put("privacyType", this.privacyType);

        return dataMap;
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = user == null ? 0 : user.hashCode();
        _hash = 31 * _hash + delta;
        delta = redirectType == null ? 0 : redirectType.hashCode();
        _hash = 31 * _hash + delta;
        delta = flashDuration == null ? 0 : flashDuration.hashCode();
        _hash = 31 * _hash + delta;
        delta = privacyType == null ? 0 : privacyType.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static VisitorSettingStub convertBeanToStub(VisitorSetting bean)
    {
        VisitorSettingStub stub = null;
        if(bean instanceof VisitorSettingStub) {
            stub = (VisitorSettingStub) bean;
        } else {
            if(bean != null) {
                stub = new VisitorSettingStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static VisitorSettingStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of VisitorSettingStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write VisitorSettingStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write VisitorSettingStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write VisitorSettingStub object as a string.", e);
        }
        
        return null;
    }
    public static VisitorSettingStub fromJsonString(String jsonStr)
    {
        try {
            VisitorSettingStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, VisitorSettingStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into VisitorSettingStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into VisitorSettingStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into VisitorSettingStub object.", e);
        }
        
        return null;
    }

}
