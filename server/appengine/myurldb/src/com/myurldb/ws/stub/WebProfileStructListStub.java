package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.WebProfileStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "webProfileStructs")
@XmlType(propOrder = {"webProfileStruct"})
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebProfileStructListStub implements Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(WebProfileStructListStub.class.getName());

    private List<WebProfileStructStub> webProfileStructs = null;

    public WebProfileStructListStub()
    {
        this(new ArrayList<WebProfileStructStub>());
    }
    public WebProfileStructListStub(List<WebProfileStructStub> webProfileStructs)
    {
        this.webProfileStructs = webProfileStructs;
    }

    public boolean isEmpty()
    {
        if(webProfileStructs == null) {
            return true;
        } else {
            return webProfileStructs.isEmpty();
        }
    }
    public int getSize()
    {
        if(webProfileStructs == null) {
            return 0;
        } else {
            return webProfileStructs.size();
        }
    }

    @XmlElement(name = "webProfileStruct")
    public List<WebProfileStructStub> getWebProfileStruct()
    {
        return getList();
    }

    @XmlTransient
    @JsonIgnore
    public List<WebProfileStructStub> getList()
    {
        return webProfileStructs;
    }
    public void setList(List<WebProfileStructStub> webProfileStructs)
    {
        this.webProfileStructs = webProfileStructs;
    }

    public String toDebugString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        Iterator<WebProfileStructStub> it = this.webProfileStructs.iterator();
        while(it.hasNext()) {
            WebProfileStructStub webProfileStruct = it.next();
            sb.append(webProfileStruct.toString());
            if(it.hasNext()) {
                sb.append(",");
            }
        }
        sb.append("}");
        return sb.toString();
    }

    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static WebProfileStructListStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of WebProfileStructListStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write WebProfileStructListStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write WebProfileStructListStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write WebProfileStructListStub object as a string.", e);
        }
        
        return null;
    }
    public static WebProfileStructListStub fromJsonString(String jsonStr)
    {
        try {
            WebProfileStructListStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, WebProfileStructListStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into WebProfileStructListStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into WebProfileStructListStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into WebProfileStructListStub object.", e);
        }
        
        return null;
    }

}
