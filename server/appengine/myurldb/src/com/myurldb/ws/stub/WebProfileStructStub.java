package com.myurldb.ws.stub;

import java.io.Serializable;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.JsonMappingException;

import com.myurldb.ws.WebProfileStruct;
import com.myurldb.ws.util.JsonUtil;


@XmlRootElement(name = "webProfileStruct")
@XmlType(propOrder = {"uuid", "type", "serviceName", "serviceUrl", "profileUrl", "note"})
// @JsonSerialize(include=Inclusion.NON_EMPTY)
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WebProfileStructStub implements WebProfileStruct, Serializable
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(WebProfileStructStub.class.getName());

    private String uuid;
    private String type;
    private String serviceName;
    private String serviceUrl;
    private String profileUrl;
    private String note;

    public WebProfileStructStub()
    {
        this(null);
    }
    public WebProfileStructStub(WebProfileStruct bean)
    {
        if(bean != null) {
            this.uuid = bean.getUuid();
            this.type = bean.getType();
            this.serviceName = bean.getServiceName();
            this.serviceUrl = bean.getServiceUrl();
            this.profileUrl = bean.getProfileUrl();
            this.note = bean.getNote();
        }
    }


    @XmlElement
    public String getUuid()
    {
        return this.uuid;
    }
    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

    @XmlElement
    public String getType()
    {
        return this.type;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    @XmlElement
    public String getServiceName()
    {
        return this.serviceName;
    }
    public void setServiceName(String serviceName)
    {
        this.serviceName = serviceName;
    }

    @XmlElement
    public String getServiceUrl()
    {
        return this.serviceUrl;
    }
    public void setServiceUrl(String serviceUrl)
    {
        this.serviceUrl = serviceUrl;
    }

    @XmlElement
    public String getProfileUrl()
    {
        return this.profileUrl;
    }
    public void setProfileUrl(String profileUrl)
    {
        this.profileUrl = profileUrl;
    }

    @XmlElement
    public String getNote()
    {
        return this.note;
    }
    public void setNote(String note)
    {
        this.note = note;
    }


    @XmlTransient
    @JsonIgnore
    public boolean isEmpty()
    {
        boolean atLeastOneFieldNonEmpty = false;
        if( atLeastOneFieldNonEmpty == false && getType() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getServiceName() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getServiceUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getProfileUrl() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        if( atLeastOneFieldNonEmpty == false && getNote() != null ) {
            atLeastOneFieldNonEmpty = true;
        }
        return (atLeastOneFieldNonEmpty == false);
    }

    protected Map<String, Object> getDataMap()
    {
        Map<String, Object> dataMap = new HashMap<String, Object>();

        dataMap.put("uuid", this.uuid);
        dataMap.put("type", this.type);
        dataMap.put("serviceName", this.serviceName);
        dataMap.put("serviceUrl", this.serviceUrl);
        dataMap.put("profileUrl", this.profileUrl);
        dataMap.put("note", this.note);

        return dataMap;
    }

    public String toDebugString()
    {
        Map<String, Object> dataMap = getDataMap();
        Iterator<Map.Entry<String,Object>> it = dataMap.entrySet().iterator();
        StringBuilder sb = new StringBuilder();
        while(it.hasNext()) {
            Map.Entry<String,Object> m =(Map.Entry<String,Object>) it.next();
            String k = (String) m.getKey();
            String v = (m.getValue() == null) ? "" : m.getValue().toString();
            if(it.hasNext()) {
                sb.append(k).append(":").append(v).append(";");
            } else {
                sb.append(k).append(":").append(v);
            }
        }
        return sb.toString();
    }

    @Override
    public int hashCode()
    {
        int _hash = super.hashCode() + 7;
        int delta = 0;
        delta = uuid == null ? 0 : uuid.hashCode();
        _hash = 31 * _hash + delta;
        delta = type == null ? 0 : type.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceName == null ? 0 : serviceName.hashCode();
        _hash = 31 * _hash + delta;
        delta = serviceUrl == null ? 0 : serviceUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = profileUrl == null ? 0 : profileUrl.hashCode();
        _hash = 31 * _hash + delta;
        delta = note == null ? 0 : note.hashCode();
        _hash = 31 * _hash + delta;
        return _hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) {
            return false;
        } else {
            return (this.hashCode() == obj.hashCode());
        }
    }

    public static WebProfileStructStub convertBeanToStub(WebProfileStruct bean)
    {
        WebProfileStructStub stub = null;
        if(bean instanceof WebProfileStructStub) {
            stub = (WebProfileStructStub) bean;
        } else {
            if(bean != null) {
                stub = new WebProfileStructStub(bean);
            } else {
                //stub = null;
            }
        }
        return stub;
    }


    @Override
    public String toString()
    {
        return toJsonString();
    }
    public static WebProfileStructStub fromString(String str)
    {
        return fromJsonString(str);
    }
 
    public String toJsonString()
    {
        try {
            String jsonStr = JsonUtil.getJsonObjectMapper().writeValueAsString(this);
            if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Json string representation of WebProfileStructStub object: " + jsonStr);
            return jsonStr;
        } catch (JsonGenerationException e) {
            log.log(Level.WARNING, "Failed to write WebProfileStructStub object as a string.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to write WebProfileStructStub object as a string.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write WebProfileStructStub object as a string.", e);
        }
        
        return null;
    }
    public static WebProfileStructStub fromJsonString(String jsonStr)
    {
        try {
            WebProfileStructStub stub = JsonUtil.getJsonObjectMapper().readValue(jsonStr, WebProfileStructStub.class);
            return stub;
        } catch (JsonParseException e) {
            log.log(Level.WARNING, "Failed to parse string into WebProfileStructStub object.", e);
        } catch (JsonMappingException e) {
            log.log(Level.WARNING, "Failed to parse string into WebProfileStructStub object.", e);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to parse string into WebProfileStructStub object.", e);
        }
        
        return null;
    }

}
