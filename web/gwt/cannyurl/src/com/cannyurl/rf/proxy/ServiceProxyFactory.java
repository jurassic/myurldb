package com.cannyurl.rf.proxy;

import java.util.logging.Logger;
import java.util.logging.Level;


public class ServiceProxyFactory
{
    private static final Logger log = Logger.getLogger(ServiceProxyFactory.class.getName());

    private AppCustomDomainServiceProxy appCustomDomainService = null;
    private SiteCustomDomainServiceProxy siteCustomDomainService = null;
    private UserServiceProxy userService = null;
    private UserUsercodeServiceProxy userUsercodeService = null;
    private UserResourcePermissionServiceProxy userResourcePermissionService = null;
    private UserResourceProhibitionServiceProxy userResourceProhibitionService = null;
    private RolePermissionServiceProxy rolePermissionService = null;
    private UserRoleServiceProxy userRoleService = null;
    private AppClientServiceProxy appClientService = null;
    private ClientUserServiceProxy clientUserService = null;
    private UserCustomDomainServiceProxy userCustomDomainService = null;
    private ClientSettingServiceProxy clientSettingService = null;
    private UserSettingServiceProxy userSettingService = null;
    private VisitorSettingServiceProxy visitorSettingService = null;
    private TwitterSummaryCardServiceProxy twitterSummaryCardService = null;
    private TwitterPhotoCardServiceProxy twitterPhotoCardService = null;
    private TwitterGalleryCardServiceProxy twitterGalleryCardService = null;
    private TwitterAppCardServiceProxy twitterAppCardService = null;
    private TwitterPlayerCardServiceProxy twitterPlayerCardService = null;
    private TwitterProductCardServiceProxy twitterProductCardService = null;
    private ShortPassageServiceProxy shortPassageService = null;
    private ShortLinkServiceProxy shortLinkService = null;
    private GeoLinkServiceProxy geoLinkService = null;
    private QrCodeServiceProxy qrCodeService = null;
    private LinkPassphraseServiceProxy linkPassphraseService = null;
    private LinkMessageServiceProxy linkMessageService = null;
    private LinkAlbumServiceProxy linkAlbumService = null;
    private AlbumShortLinkServiceProxy albumShortLinkService = null;
    private KeywordFolderServiceProxy keywordFolderService = null;
    private BookmarkFolderServiceProxy bookmarkFolderService = null;
    private KeywordLinkServiceProxy keywordLinkService = null;
    private BookmarkLinkServiceProxy bookmarkLinkService = null;
    private SpeedDialServiceProxy speedDialService = null;
    private KeywordFolderImportServiceProxy keywordFolderImportService = null;
    private BookmarkFolderImportServiceProxy bookmarkFolderImportService = null;
    private KeywordCrowdTallyServiceProxy keywordCrowdTallyService = null;
    private BookmarkCrowdTallyServiceProxy bookmarkCrowdTallyService = null;
    private DomainInfoServiceProxy domainInfoService = null;
    private UrlRatingServiceProxy urlRatingService = null;
    private UserRatingServiceProxy userRatingService = null;
    private AbuseTagServiceProxy abuseTagService = null;
    private ServiceInfoServiceProxy serviceInfoService = null;
    private FiveTenServiceProxy fiveTenService = null;

    private ServiceProxyFactory()
    {
    }

    // Initialization-on-demand holder.
    private static class ServiceProxyFactoryHolder
    {
        private static final ServiceProxyFactory INSTANCE = new ServiceProxyFactory();
    }

    // Singleton method
    public static ServiceProxyFactory getInstance()
    {
        return ServiceProxyFactoryHolder.INSTANCE;
    }

    public AppCustomDomainServiceProxy getAppCustomDomainServiceProxy()
    {
        if(appCustomDomainService == null) {
            appCustomDomainService = new AppCustomDomainServiceProxy();
        }
        return appCustomDomainService;
    }

    public SiteCustomDomainServiceProxy getSiteCustomDomainServiceProxy()
    {
        if(siteCustomDomainService == null) {
            siteCustomDomainService = new SiteCustomDomainServiceProxy();
        }
        return siteCustomDomainService;
    }

    public UserServiceProxy getUserServiceProxy()
    {
        if(userService == null) {
            userService = new UserServiceProxy();
        }
        return userService;
    }

    public UserUsercodeServiceProxy getUserUsercodeServiceProxy()
    {
        if(userUsercodeService == null) {
            userUsercodeService = new UserUsercodeServiceProxy();
        }
        return userUsercodeService;
    }

    public UserResourcePermissionServiceProxy getUserResourcePermissionServiceProxy()
    {
        if(userResourcePermissionService == null) {
            userResourcePermissionService = new UserResourcePermissionServiceProxy();
        }
        return userResourcePermissionService;
    }

    public UserResourceProhibitionServiceProxy getUserResourceProhibitionServiceProxy()
    {
        if(userResourceProhibitionService == null) {
            userResourceProhibitionService = new UserResourceProhibitionServiceProxy();
        }
        return userResourceProhibitionService;
    }

    public RolePermissionServiceProxy getRolePermissionServiceProxy()
    {
        if(rolePermissionService == null) {
            rolePermissionService = new RolePermissionServiceProxy();
        }
        return rolePermissionService;
    }

    public UserRoleServiceProxy getUserRoleServiceProxy()
    {
        if(userRoleService == null) {
            userRoleService = new UserRoleServiceProxy();
        }
        return userRoleService;
    }

    public AppClientServiceProxy getAppClientServiceProxy()
    {
        if(appClientService == null) {
            appClientService = new AppClientServiceProxy();
        }
        return appClientService;
    }

    public ClientUserServiceProxy getClientUserServiceProxy()
    {
        if(clientUserService == null) {
            clientUserService = new ClientUserServiceProxy();
        }
        return clientUserService;
    }

    public UserCustomDomainServiceProxy getUserCustomDomainServiceProxy()
    {
        if(userCustomDomainService == null) {
            userCustomDomainService = new UserCustomDomainServiceProxy();
        }
        return userCustomDomainService;
    }

    public ClientSettingServiceProxy getClientSettingServiceProxy()
    {
        if(clientSettingService == null) {
            clientSettingService = new ClientSettingServiceProxy();
        }
        return clientSettingService;
    }

    public UserSettingServiceProxy getUserSettingServiceProxy()
    {
        if(userSettingService == null) {
            userSettingService = new UserSettingServiceProxy();
        }
        return userSettingService;
    }

    public VisitorSettingServiceProxy getVisitorSettingServiceProxy()
    {
        if(visitorSettingService == null) {
            visitorSettingService = new VisitorSettingServiceProxy();
        }
        return visitorSettingService;
    }

    public TwitterSummaryCardServiceProxy getTwitterSummaryCardServiceProxy()
    {
        if(twitterSummaryCardService == null) {
            twitterSummaryCardService = new TwitterSummaryCardServiceProxy();
        }
        return twitterSummaryCardService;
    }

    public TwitterPhotoCardServiceProxy getTwitterPhotoCardServiceProxy()
    {
        if(twitterPhotoCardService == null) {
            twitterPhotoCardService = new TwitterPhotoCardServiceProxy();
        }
        return twitterPhotoCardService;
    }

    public TwitterGalleryCardServiceProxy getTwitterGalleryCardServiceProxy()
    {
        if(twitterGalleryCardService == null) {
            twitterGalleryCardService = new TwitterGalleryCardServiceProxy();
        }
        return twitterGalleryCardService;
    }

    public TwitterAppCardServiceProxy getTwitterAppCardServiceProxy()
    {
        if(twitterAppCardService == null) {
            twitterAppCardService = new TwitterAppCardServiceProxy();
        }
        return twitterAppCardService;
    }

    public TwitterPlayerCardServiceProxy getTwitterPlayerCardServiceProxy()
    {
        if(twitterPlayerCardService == null) {
            twitterPlayerCardService = new TwitterPlayerCardServiceProxy();
        }
        return twitterPlayerCardService;
    }

    public TwitterProductCardServiceProxy getTwitterProductCardServiceProxy()
    {
        if(twitterProductCardService == null) {
            twitterProductCardService = new TwitterProductCardServiceProxy();
        }
        return twitterProductCardService;
    }

    public ShortPassageServiceProxy getShortPassageServiceProxy()
    {
        if(shortPassageService == null) {
            shortPassageService = new ShortPassageServiceProxy();
        }
        return shortPassageService;
    }

    public ShortLinkServiceProxy getShortLinkServiceProxy()
    {
        if(shortLinkService == null) {
            shortLinkService = new ShortLinkServiceProxy();
        }
        return shortLinkService;
    }

    public GeoLinkServiceProxy getGeoLinkServiceProxy()
    {
        if(geoLinkService == null) {
            geoLinkService = new GeoLinkServiceProxy();
        }
        return geoLinkService;
    }

    public QrCodeServiceProxy getQrCodeServiceProxy()
    {
        if(qrCodeService == null) {
            qrCodeService = new QrCodeServiceProxy();
        }
        return qrCodeService;
    }

    public LinkPassphraseServiceProxy getLinkPassphraseServiceProxy()
    {
        if(linkPassphraseService == null) {
            linkPassphraseService = new LinkPassphraseServiceProxy();
        }
        return linkPassphraseService;
    }

    public LinkMessageServiceProxy getLinkMessageServiceProxy()
    {
        if(linkMessageService == null) {
            linkMessageService = new LinkMessageServiceProxy();
        }
        return linkMessageService;
    }

    public LinkAlbumServiceProxy getLinkAlbumServiceProxy()
    {
        if(linkAlbumService == null) {
            linkAlbumService = new LinkAlbumServiceProxy();
        }
        return linkAlbumService;
    }

    public AlbumShortLinkServiceProxy getAlbumShortLinkServiceProxy()
    {
        if(albumShortLinkService == null) {
            albumShortLinkService = new AlbumShortLinkServiceProxy();
        }
        return albumShortLinkService;
    }

    public KeywordFolderServiceProxy getKeywordFolderServiceProxy()
    {
        if(keywordFolderService == null) {
            keywordFolderService = new KeywordFolderServiceProxy();
        }
        return keywordFolderService;
    }

    public BookmarkFolderServiceProxy getBookmarkFolderServiceProxy()
    {
        if(bookmarkFolderService == null) {
            bookmarkFolderService = new BookmarkFolderServiceProxy();
        }
        return bookmarkFolderService;
    }

    public KeywordLinkServiceProxy getKeywordLinkServiceProxy()
    {
        if(keywordLinkService == null) {
            keywordLinkService = new KeywordLinkServiceProxy();
        }
        return keywordLinkService;
    }

    public BookmarkLinkServiceProxy getBookmarkLinkServiceProxy()
    {
        if(bookmarkLinkService == null) {
            bookmarkLinkService = new BookmarkLinkServiceProxy();
        }
        return bookmarkLinkService;
    }

    public SpeedDialServiceProxy getSpeedDialServiceProxy()
    {
        if(speedDialService == null) {
            speedDialService = new SpeedDialServiceProxy();
        }
        return speedDialService;
    }

    public KeywordFolderImportServiceProxy getKeywordFolderImportServiceProxy()
    {
        if(keywordFolderImportService == null) {
            keywordFolderImportService = new KeywordFolderImportServiceProxy();
        }
        return keywordFolderImportService;
    }

    public BookmarkFolderImportServiceProxy getBookmarkFolderImportServiceProxy()
    {
        if(bookmarkFolderImportService == null) {
            bookmarkFolderImportService = new BookmarkFolderImportServiceProxy();
        }
        return bookmarkFolderImportService;
    }

    public KeywordCrowdTallyServiceProxy getKeywordCrowdTallyServiceProxy()
    {
        if(keywordCrowdTallyService == null) {
            keywordCrowdTallyService = new KeywordCrowdTallyServiceProxy();
        }
        return keywordCrowdTallyService;
    }

    public BookmarkCrowdTallyServiceProxy getBookmarkCrowdTallyServiceProxy()
    {
        if(bookmarkCrowdTallyService == null) {
            bookmarkCrowdTallyService = new BookmarkCrowdTallyServiceProxy();
        }
        return bookmarkCrowdTallyService;
    }

    public DomainInfoServiceProxy getDomainInfoServiceProxy()
    {
        if(domainInfoService == null) {
            domainInfoService = new DomainInfoServiceProxy();
        }
        return domainInfoService;
    }

    public UrlRatingServiceProxy getUrlRatingServiceProxy()
    {
        if(urlRatingService == null) {
            urlRatingService = new UrlRatingServiceProxy();
        }
        return urlRatingService;
    }

    public UserRatingServiceProxy getUserRatingServiceProxy()
    {
        if(userRatingService == null) {
            userRatingService = new UserRatingServiceProxy();
        }
        return userRatingService;
    }

    public AbuseTagServiceProxy getAbuseTagServiceProxy()
    {
        if(abuseTagService == null) {
            abuseTagService = new AbuseTagServiceProxy();
        }
        return abuseTagService;
    }

    public ServiceInfoServiceProxy getServiceInfoServiceProxy()
    {
        if(serviceInfoService == null) {
            serviceInfoService = new ServiceInfoServiceProxy();
        }
        return serviceInfoService;
    }

    public FiveTenServiceProxy getFiveTenServiceProxy()
    {
        if(fiveTenService == null) {
            fiveTenService = new FiveTenServiceProxy();
        }
        return fiveTenService;
    }

}
