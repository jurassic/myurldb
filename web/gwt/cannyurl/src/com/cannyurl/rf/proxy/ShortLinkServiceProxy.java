package com.cannyurl.rf.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheEntry;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;


import com.myurldb.ws.BaseException;
import com.myurldb.ws.exception.NotImplementedException;
import com.myurldb.ws.core.StatusCode;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.KeyListStub;
import com.myurldb.ws.stub.ReferrerInfoStructStub;
import com.myurldb.ws.stub.ReferrerInfoStructListStub;
import com.myurldb.ws.stub.ShortLinkStub;
import com.myurldb.ws.stub.ShortLinkListStub;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.util.StringUtil;
import com.cannyurl.rf.auth.TwoLeggedOAuthClientUtil;

import com.cannyurl.rf.config.Config;


public class ShortLinkServiceProxy extends AbstractBaseServiceProxy implements ShortLinkService
{
    private static final Logger log = Logger.getLogger(ShortLinkServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding ws.resource.impl class.
    private final static String RESOURCE_SHORTLINK = "shortLinks";


    // Cache service
    private Cache mCache = null;

    private void initCache()
    {
        Boolean cacheEnabled = Config.getInstance().isCacheEnabled();
        if(cacheEnabled != null && cacheEnabled == true) {
            try {
                Map<String, Object> props = new HashMap<String, Object>();
                Integer cacheExpirationDelta = Config.getInstance().getCacheExpirationDelta();
                // if(cacheExpirationDelta == null || cacheExpirationDelta <= 0) {
                //     cacheExpirationDelta = 1800; // ???
                // }
                // props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                if(cacheExpirationDelta != null && cacheExpirationDelta > 0) {   // ???
                    props.put(GCacheFactory.EXPIRATION_DELTA, cacheExpirationDelta);
                }
                CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
                mCache = cacheFactory.createCache(props);
            } catch (CacheException e) {
                log.log(Level.WARNING, "Failed to create Cache service.", e);
            } catch (Exception e) {
                log.log(Level.WARNING, "Failed to create Cache service due to unknown error.", e);
            }
        } else {
            log.log(Level.FINE, "Cache is not enabled.");
        }
    }

    protected Cache getCache()
    {
        if(mCache == null) {
            initCache();
        }
        return mCache;
    }

    public ShortLinkServiceProxy()
    {
        initCache();
    }


    protected WebResource getShortLinkWebResource()
    {
        return getWebResource(RESOURCE_SHORTLINK);
    }
    protected WebResource getShortLinkWebResource(String path)
    {
        return getWebResource(RESOURCE_SHORTLINK, path);
    }
    protected WebResource getShortLinkWebResourceByGuid(String guid)
    {
        return getShortLinkWebResource(guid);
    }


    @Override
    public ShortLink getShortLink(String guid) throws BaseException
    {
        ShortLink bean = null;

        String key = getResourcePath(RESOURCE_SHORTLINK, guid);
        CacheEntry entry = null;
        if(mCache != null) {
            entry = mCache.getCacheEntry(key);
            if(entry != null) {
                // TBD: eTag, lastModified, expires, etc....
            }
        }

        WebResource webResource = getShortLinkWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ShortLinkStub stub = clientResponse.getEntity(ShortLinkStub.class);
                bean = MarshalHelper.convertShortLinkToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ShortLink bean = " + bean);
                break;
            // case StatusCode.NOT_MODIFIED:
            //     // TBD
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Object getShortLink(String guid, String field) throws BaseException
    {
        ShortLink bean = getShortLink(guid);
        if(bean == null) {
            return null;
        }

        // TBD
        if(field.equals("guid")) {
            return bean.getGuid();  // Should be the same as arg guid.
        } else if(field.equals("appClient")) {
            return bean.getAppClient();
        } else if(field.equals("clientRootDomain")) {
            return bean.getClientRootDomain();
        } else if(field.equals("owner")) {
            return bean.getOwner();
        } else if(field.equals("longUrlDomain")) {
            return bean.getLongUrlDomain();
        } else if(field.equals("longUrl")) {
            return bean.getLongUrl();
        } else if(field.equals("longUrlFull")) {
            return bean.getLongUrlFull();
        } else if(field.equals("longUrlHash")) {
            return bean.getLongUrlHash();
        } else if(field.equals("reuseExistingShortUrl")) {
            return bean.isReuseExistingShortUrl();
        } else if(field.equals("failIfShortUrlExists")) {
            return bean.isFailIfShortUrlExists();
        } else if(field.equals("domain")) {
            return bean.getDomain();
        } else if(field.equals("domainType")) {
            return bean.getDomainType();
        } else if(field.equals("usercode")) {
            return bean.getUsercode();
        } else if(field.equals("username")) {
            return bean.getUsername();
        } else if(field.equals("tokenPrefix")) {
            return bean.getTokenPrefix();
        } else if(field.equals("token")) {
            return bean.getToken();
        } else if(field.equals("tokenType")) {
            return bean.getTokenType();
        } else if(field.equals("sassyTokenType")) {
            return bean.getSassyTokenType();
        } else if(field.equals("shortUrl")) {
            return bean.getShortUrl();
        } else if(field.equals("shortPassage")) {
            return bean.getShortPassage();
        } else if(field.equals("redirectType")) {
            return bean.getRedirectType();
        } else if(field.equals("flashDuration")) {
            return bean.getFlashDuration();
        } else if(field.equals("accessType")) {
            return bean.getAccessType();
        } else if(field.equals("viewType")) {
            return bean.getViewType();
        } else if(field.equals("shareType")) {
            return bean.getShareType();
        } else if(field.equals("readOnly")) {
            return bean.isReadOnly();
        } else if(field.equals("displayMessage")) {
            return bean.getDisplayMessage();
        } else if(field.equals("shortMessage")) {
            return bean.getShortMessage();
        } else if(field.equals("keywordEnabled")) {
            return bean.isKeywordEnabled();
        } else if(field.equals("bookmarkEnabled")) {
            return bean.isBookmarkEnabled();
        } else if(field.equals("referrerInfo")) {
            return bean.getReferrerInfo();
        } else if(field.equals("status")) {
            return bean.getStatus();
        } else if(field.equals("note")) {
            return bean.getNote();
        } else if(field.equals("expirationTime")) {
            return bean.getExpirationTime();
        } else if(field.equals("createdTime")) {
            return bean.getCreatedTime();
        } else if(field.equals("modifiedTime")) {
            return bean.getModifiedTime();
        }

        return null;
    }

    @Override
    public List<ShortLink> getShortLinks(List<String> guids) throws BaseException
    {
        throw new NotImplementedException("To be implemented.");
    }

    @Override
    public List<ShortLink> getAllShortLinks() throws BaseException
    {
        return getAllShortLinks(null, null, null);
    }

    @Override
    public List<ShortLink> getAllShortLinks(String ordering, Long offset, Integer count) throws BaseException
    {
    	List<ShortLink> list = null;
    	
        WebResource webResource = getShortLinkWebResource(RESOURCE_PATH_ALL);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ShortLinkListStub stub = clientResponse.getEntity(ShortLinkListStub.class);
                list = MarshalHelper.convertShortLinkListStubToBeanList(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ShortLink list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws BaseException
    {
    	List<String> list = null;
    	
        WebResource webResource = getShortLinkWebResource(RESOURCE_PATH_ALLKEYS);
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource.accept(getOutputMediaType()).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ShortLink key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params,
            List<String> values) throws BaseException
    {
        return findShortLinks(filter, ordering, params, values, null, null, null, null);
    }

    @Override
    public List<ShortLink> findShortLinks(String filter, String ordering, String params,
            List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
    	List<ShortLink> list = null;
    	
//        ClientResponse clientResponse = getShortLinkWebResource()
//        	.queryParam("filter", filter)
//        	.queryParam("ordering", ordering)
//        	.queryParam("params", params)
//        	//.queryParam("values", values)  // ???
//        	.accept(getOutputMediaType())
//        	.get(ClientResponse.class);

    	WebResource webResource = getShortLinkWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                ShortLinkListStub stub = clientResponse.getEntity(ShortLinkListStub.class);
                list = MarshalHelper.convertShortLinkListStubToBeanList(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ShortLink list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws BaseException
    {
    	List<String> list = null;

    	WebResource webResource = getShortLinkWebResource(RESOURCE_PATH_KEYS);
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(ordering != null && !ordering.isEmpty()) {
            webResource = webResource.queryParam("ordering", ordering);
        }
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(grouping != null && !grouping.isEmpty()) {
            webResource = webResource.queryParam("grouping", grouping);
        }
        if(unique != null) {
            webResource = webResource.queryParam("unique", unique.toString());
        }
        if(offset != null) {
            webResource = webResource.queryParam("offset", offset.toString());
        }
        if(count != null) {
            webResource = webResource.queryParam("count", count.toString());
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

    	ClientResponse clientResponse = webResource
    	    .accept(getOutputMediaType())
    	    .get(ClientResponse.class);
        
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                KeyListStub stub = clientResponse.getEntity(KeyListStub.class);
                list = stub.getList();
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ShortLink key list = " + list);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return list;
    }

    @Override
    public Long getCount(String filter, String params, List<String> values, String aggregate) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getShortLinkWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // It just returns all objects.... (possibly, ordered if ordering is not empty).
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface of the findXXX() method.
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }
        if(aggregate != null) {
            webResource = webResource.queryParam("aggregate", aggregate);
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.accept(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "ShortLink count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }

    @Override
    public String createShortLink(String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        ShortLinkBean bean = new ShortLinkBean(null, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status, note, expirationTime);
        return createShortLink(bean);        
    }

    @Override
    public String createShortLink(ShortLink bean) throws BaseException
    {
        String guid = null;
        ShortLinkStub stub = MarshalHelper.convertShortLinkToStub(bean);
        WebResource webResource = getShortLinkWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                guid = clientResponse.getEntity(String.class);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New ShortLink guid = " + guid);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New ShortLink resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return guid;
    }

    @Override
    public ShortLink constructShortLink(ShortLink bean) throws BaseException
    {
        ShortLinkStub stub = MarshalHelper.convertShortLinkToStub(bean);
        WebResource webResource = getShortLinkWebResource();

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).post(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                stub = clientResponse.getEntity(ShortLinkStub.class);
                bean = MarshalHelper.convertShortLinkToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New ShortLink bean = " + bean);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New ShortLink resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean updateShortLink(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStruct referrerInfo, String status, String note, Long expirationTime) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ShortLink guid is invalid.");
        	throw new BaseException("ShortLink guid is invalid.");
        }
        ShortLinkBean bean = new ShortLinkBean(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, MarshalHelper.convertReferrerInfoStructToBean(referrerInfo), status, note, expirationTime);
        return updateShortLink(bean);        
    }

    @Override
    public Boolean updateShortLink(ShortLink bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ShortLink object is invalid.");
        	throw new BaseException("ShortLink object is invalid.");
        }
        ShortLinkStub stub = MarshalHelper.convertShortLinkToStub(bean);

        WebResource webResource = getShortLinkWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        //ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(MediaType.TEXT_PLAIN).put(ClientResponse.class, stub);
        ClientResponse clientResponse = webResource.type(getInputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully updated the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public ShortLink refreshShortLink(ShortLink bean) throws BaseException
    {
        String guid = bean.getGuid();
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ShortLink object is invalid.");
        	throw new BaseException("ShortLink object is invalid.");
        }
        ShortLinkStub stub = MarshalHelper.convertShortLinkToStub(bean);

        WebResource webResource = getShortLinkWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).put(ClientResponse.class, stub);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                stub = clientResponse.getEntity(ShortLinkStub.class);
                bean = MarshalHelper.convertShortLinkToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully refreshed the ShortLink bean = " + bean);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return bean;
    }

    @Override
    public Boolean deleteShortLink(String guid) throws BaseException
    {
        if(guid == null || guid.length() == 0) {
            log.log(Level.WARNING, "ShortLink guid is invalid.");
        	throw new BaseException("ShortLink guid is invalid.");
        }

        WebResource webResource = getShortLinkWebResource(guid);

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        ClientResponse clientResponse = webResource.delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
            case StatusCode.NO_CONTENT:
            case StatusCode.RESET_CONTENT:
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Successfully deleted the user with guid = " + guid);
                return true;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return false;
    }

    @Override
    public Boolean deleteShortLink(ShortLink bean) throws BaseException
    {
        String guid = bean.getGuid();
        return deleteShortLink(guid);
    }

    @Override
    public Long deleteShortLinks(String filter, String params, List<String> values) throws BaseException
    {
        Long count = 0L;
     	WebResource webResource = getShortLinkWebResource();
    	if(filter == null || filter.isEmpty()) {
    	    // Should we allow empty filter????
    	    // Currently, the data access layer throws exception if the filter is empty.
    	} else {
    	    webResource = webResource.queryParam("filter", filter);
    	}
        if(filter != null && !filter.isEmpty()) {
            // params/values make sense only if filter is set.
            if(params != null && !params.isEmpty()) {
                webResource = webResource.queryParam("params", params);
            }
        	if(values != null) {
                // TBD: Is the order of "values" preserved????
                // If not, we'll need to change the interface. Or, try comma-separated list???
        	    for(String v : values) {
        	        webResource = webResource.queryParam("values", v);
        	    }
        	}
        }

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);

        // Order of the mime types ???
        ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN).delete(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.OK:
            case StatusCode.ACCEPTED:
                count = Long.parseLong(clientResponse.getEntity(String.class));
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Deleted ShortLinks: count = " + count);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
 
        return count;
    }


    // TBD
    // Temporary implementation. For now, just loop through all elements in the list.
    @Override
    public Integer createShortLinks(List<ShortLink> shortLinks) throws BaseException
    {
        log.finer("BEGIN");

        if(shortLinks == null) {
            log.log(Level.WARNING, "createShortLinks() failed because the input arg is null.");
            return -1;  // ???
        }
        int size = shortLinks.size();
        if(size == 0) {
            log.log(Level.WARNING, "createShortLinks() failed because the input list empty.");
            return 0;
        }

        int count = 0;
        for(ShortLink shortLink : shortLinks) {
            String guid = createShortLink(shortLink);
            if(guid != null) {  // TBD: Validate?
                count++;
            } else {
                // TBD: Retry?
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "createShortLinks() failed for at least one shortLink. Index = " + count);
            }
        }

        if(log.isLoggable(Level.FINER)) log.finer("END: count = " + count);
        return count;
    }

    // TBD
    //@Override
    //public Boolean updateShortLinks(List<ShortLink> shortLinks) throws BaseException
    //{
    //}

}
