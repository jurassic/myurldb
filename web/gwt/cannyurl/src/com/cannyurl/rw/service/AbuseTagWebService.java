package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AbuseTag;
import com.cannyurl.af.bean.AbuseTagBean;
import com.cannyurl.af.service.AbuseTagService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.AbuseTagJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AbuseTagWebService // implements AbuseTagService
{
    private static final Logger log = Logger.getLogger(AbuseTagWebService.class.getName());
     
    // Af service interface.
    private AbuseTagService mService = null;

    public AbuseTagWebService()
    {
        this(ServiceProxyFactory.getInstance().getAbuseTagServiceProxy());
    }
    public AbuseTagWebService(AbuseTagService service)
    {
        mService = service;
    }
    
    private AbuseTagService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getAbuseTagServiceProxy();
        }
        return mService;
    }
    
    
    public AbuseTagJsBean getAbuseTag(String guid) throws WebException
    {
        try {
            AbuseTag abuseTag = getService().getAbuseTag(guid);
            AbuseTagJsBean bean = convertAbuseTagToJsBean(abuseTag);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAbuseTag(String guid, String field) throws WebException
    {
        try {
            return getService().getAbuseTag(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AbuseTagJsBean> getAbuseTags(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AbuseTagJsBean> jsBeans = new ArrayList<AbuseTagJsBean>();
            List<AbuseTag> abuseTags = getService().getAbuseTags(guids);
            if(abuseTags != null) {
                for(AbuseTag abuseTag : abuseTags) {
                    jsBeans.add(convertAbuseTagToJsBean(abuseTag));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AbuseTagJsBean> getAllAbuseTags() throws WebException
    {
        return getAllAbuseTags(null, null, null);
    }

    public List<AbuseTagJsBean> getAllAbuseTags(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<AbuseTagJsBean> jsBeans = new ArrayList<AbuseTagJsBean>();
            List<AbuseTag> abuseTags = getService().getAllAbuseTags(ordering, offset, count);
            if(abuseTags != null) {
                for(AbuseTag abuseTag : abuseTags) {
                    jsBeans.add(convertAbuseTagToJsBean(abuseTag));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllAbuseTagKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllAbuseTagKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AbuseTagJsBean> findAbuseTags(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAbuseTags(filter, ordering, params, values, null, null, null, null);
    }

    public List<AbuseTagJsBean> findAbuseTags(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<AbuseTagJsBean> jsBeans = new ArrayList<AbuseTagJsBean>();
            List<AbuseTag> abuseTags = getService().findAbuseTags(filter, ordering, params, values, grouping, unique, offset, count);
            if(abuseTags != null) {
                for(AbuseTag abuseTag : abuseTags) {
                    jsBeans.add(convertAbuseTagToJsBean(abuseTag));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findAbuseTagKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findAbuseTagKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAbuseTag(String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws WebException
    {
        try {
            return getService().createAbuseTag(shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAbuseTag(AbuseTagJsBean jsBean) throws WebException
    {
        try {
            AbuseTag abuseTag = convertAbuseTagJsBeanToBean(jsBean);
            return getService().createAbuseTag(abuseTag);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AbuseTagJsBean constructAbuseTag(AbuseTagJsBean jsBean) throws WebException
    {
        try {
            AbuseTag abuseTag = convertAbuseTagJsBeanToBean(jsBean);
            abuseTag = getService().constructAbuseTag(abuseTag);
            jsBean = convertAbuseTagToJsBean(abuseTag);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAbuseTag(String guid, String shortLink, String shortUrl, String longUrl, String longUrlHash, String user, String ipAddress, Integer tag, String comment, String status, String reviewer, String action, String note, String pastActions, Long reviewedTime, Long actionTime) throws WebException
    {
        try {
            return getService().updateAbuseTag(guid, shortLink, shortUrl, longUrl, longUrlHash, user, ipAddress, tag, comment, status, reviewer, action, note, pastActions, reviewedTime, actionTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAbuseTag(AbuseTagJsBean jsBean) throws WebException
    {
        try {
            AbuseTag abuseTag = convertAbuseTagJsBeanToBean(jsBean);
            return getService().updateAbuseTag(abuseTag);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AbuseTagJsBean refreshAbuseTag(AbuseTagJsBean jsBean) throws WebException
    {
        try {
            AbuseTag abuseTag = convertAbuseTagJsBeanToBean(jsBean);
            abuseTag = getService().refreshAbuseTag(abuseTag);
            jsBean = convertAbuseTagToJsBean(abuseTag);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAbuseTag(String guid) throws WebException
    {
        try {
            return getService().deleteAbuseTag(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAbuseTag(AbuseTagJsBean jsBean) throws WebException
    {
        try {
            AbuseTag abuseTag = convertAbuseTagJsBeanToBean(jsBean);
            return getService().deleteAbuseTag(abuseTag);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAbuseTags(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteAbuseTags(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static AbuseTagJsBean convertAbuseTagToJsBean(AbuseTag abuseTag)
    {
        AbuseTagJsBean jsBean = null;
        if(abuseTag != null) {
            jsBean = new AbuseTagJsBean();
            jsBean.setGuid(abuseTag.getGuid());
            jsBean.setShortLink(abuseTag.getShortLink());
            jsBean.setShortUrl(abuseTag.getShortUrl());
            jsBean.setLongUrl(abuseTag.getLongUrl());
            jsBean.setLongUrlHash(abuseTag.getLongUrlHash());
            jsBean.setUser(abuseTag.getUser());
            jsBean.setIpAddress(abuseTag.getIpAddress());
            jsBean.setTag(abuseTag.getTag());
            jsBean.setComment(abuseTag.getComment());
            jsBean.setStatus(abuseTag.getStatus());
            jsBean.setReviewer(abuseTag.getReviewer());
            jsBean.setAction(abuseTag.getAction());
            jsBean.setNote(abuseTag.getNote());
            jsBean.setPastActions(abuseTag.getPastActions());
            jsBean.setReviewedTime(abuseTag.getReviewedTime());
            jsBean.setActionTime(abuseTag.getActionTime());
            jsBean.setCreatedTime(abuseTag.getCreatedTime());
            jsBean.setModifiedTime(abuseTag.getModifiedTime());
        }
        return jsBean;
    }

    public static AbuseTag convertAbuseTagJsBeanToBean(AbuseTagJsBean jsBean)
    {
        AbuseTagBean abuseTag = null;
        if(jsBean != null) {
            abuseTag = new AbuseTagBean();
            abuseTag.setGuid(jsBean.getGuid());
            abuseTag.setShortLink(jsBean.getShortLink());
            abuseTag.setShortUrl(jsBean.getShortUrl());
            abuseTag.setLongUrl(jsBean.getLongUrl());
            abuseTag.setLongUrlHash(jsBean.getLongUrlHash());
            abuseTag.setUser(jsBean.getUser());
            abuseTag.setIpAddress(jsBean.getIpAddress());
            abuseTag.setTag(jsBean.getTag());
            abuseTag.setComment(jsBean.getComment());
            abuseTag.setStatus(jsBean.getStatus());
            abuseTag.setReviewer(jsBean.getReviewer());
            abuseTag.setAction(jsBean.getAction());
            abuseTag.setNote(jsBean.getNote());
            abuseTag.setPastActions(jsBean.getPastActions());
            abuseTag.setReviewedTime(jsBean.getReviewedTime());
            abuseTag.setActionTime(jsBean.getActionTime());
            abuseTag.setCreatedTime(jsBean.getCreatedTime());
            abuseTag.setModifiedTime(jsBean.getModifiedTime());
        }
        return abuseTag;
    }

}
