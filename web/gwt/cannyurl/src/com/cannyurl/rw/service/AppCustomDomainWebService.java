package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.AppCustomDomain;
import com.cannyurl.af.bean.AppCustomDomainBean;
import com.cannyurl.af.service.AppCustomDomainService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.AppCustomDomainJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class AppCustomDomainWebService // implements AppCustomDomainService
{
    private static final Logger log = Logger.getLogger(AppCustomDomainWebService.class.getName());
     
    // Af service interface.
    private AppCustomDomainService mService = null;

    public AppCustomDomainWebService()
    {
        this(ServiceProxyFactory.getInstance().getAppCustomDomainServiceProxy());
    }
    public AppCustomDomainWebService(AppCustomDomainService service)
    {
        mService = service;
    }
    
    private AppCustomDomainService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getAppCustomDomainServiceProxy();
        }
        return mService;
    }
    
    
    public AppCustomDomainJsBean getAppCustomDomain(String guid) throws WebException
    {
        try {
            AppCustomDomain appCustomDomain = getService().getAppCustomDomain(guid);
            AppCustomDomainJsBean bean = convertAppCustomDomainToJsBean(appCustomDomain);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getAppCustomDomain(String guid, String field) throws WebException
    {
        try {
            return getService().getAppCustomDomain(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AppCustomDomainJsBean> getAppCustomDomains(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<AppCustomDomainJsBean> jsBeans = new ArrayList<AppCustomDomainJsBean>();
            List<AppCustomDomain> appCustomDomains = getService().getAppCustomDomains(guids);
            if(appCustomDomains != null) {
                for(AppCustomDomain appCustomDomain : appCustomDomains) {
                    jsBeans.add(convertAppCustomDomainToJsBean(appCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AppCustomDomainJsBean> getAllAppCustomDomains() throws WebException
    {
        return getAllAppCustomDomains(null, null, null);
    }

    public List<AppCustomDomainJsBean> getAllAppCustomDomains(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<AppCustomDomainJsBean> jsBeans = new ArrayList<AppCustomDomainJsBean>();
            List<AppCustomDomain> appCustomDomains = getService().getAllAppCustomDomains(ordering, offset, count);
            if(appCustomDomains != null) {
                for(AppCustomDomain appCustomDomain : appCustomDomains) {
                    jsBeans.add(convertAppCustomDomainToJsBean(appCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllAppCustomDomainKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllAppCustomDomainKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<AppCustomDomainJsBean> findAppCustomDomains(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findAppCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    public List<AppCustomDomainJsBean> findAppCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<AppCustomDomainJsBean> jsBeans = new ArrayList<AppCustomDomainJsBean>();
            List<AppCustomDomain> appCustomDomains = getService().findAppCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
            if(appCustomDomains != null) {
                for(AppCustomDomain appCustomDomain : appCustomDomains) {
                    jsBeans.add(convertAppCustomDomainToJsBean(appCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findAppCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findAppCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAppCustomDomain(String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws WebException
    {
        try {
            return getService().createAppCustomDomain(appId, siteDomain, domain, verified, status, verifiedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createAppCustomDomain(AppCustomDomainJsBean jsBean) throws WebException
    {
        try {
            AppCustomDomain appCustomDomain = convertAppCustomDomainJsBeanToBean(jsBean);
            return getService().createAppCustomDomain(appCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AppCustomDomainJsBean constructAppCustomDomain(AppCustomDomainJsBean jsBean) throws WebException
    {
        try {
            AppCustomDomain appCustomDomain = convertAppCustomDomainJsBeanToBean(jsBean);
            appCustomDomain = getService().constructAppCustomDomain(appCustomDomain);
            jsBean = convertAppCustomDomainToJsBean(appCustomDomain);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateAppCustomDomain(String guid, String appId, String siteDomain, String domain, Boolean verified, String status, Long verifiedTime) throws WebException
    {
        try {
            return getService().updateAppCustomDomain(guid, appId, siteDomain, domain, verified, status, verifiedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateAppCustomDomain(AppCustomDomainJsBean jsBean) throws WebException
    {
        try {
            AppCustomDomain appCustomDomain = convertAppCustomDomainJsBeanToBean(jsBean);
            return getService().updateAppCustomDomain(appCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public AppCustomDomainJsBean refreshAppCustomDomain(AppCustomDomainJsBean jsBean) throws WebException
    {
        try {
            AppCustomDomain appCustomDomain = convertAppCustomDomainJsBeanToBean(jsBean);
            appCustomDomain = getService().refreshAppCustomDomain(appCustomDomain);
            jsBean = convertAppCustomDomainToJsBean(appCustomDomain);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAppCustomDomain(String guid) throws WebException
    {
        try {
            return getService().deleteAppCustomDomain(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteAppCustomDomain(AppCustomDomainJsBean jsBean) throws WebException
    {
        try {
            AppCustomDomain appCustomDomain = convertAppCustomDomainJsBeanToBean(jsBean);
            return getService().deleteAppCustomDomain(appCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteAppCustomDomains(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteAppCustomDomains(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static AppCustomDomainJsBean convertAppCustomDomainToJsBean(AppCustomDomain appCustomDomain)
    {
        AppCustomDomainJsBean jsBean = null;
        if(appCustomDomain != null) {
            jsBean = new AppCustomDomainJsBean();
            jsBean.setGuid(appCustomDomain.getGuid());
            jsBean.setAppId(appCustomDomain.getAppId());
            jsBean.setSiteDomain(appCustomDomain.getSiteDomain());
            jsBean.setDomain(appCustomDomain.getDomain());
            jsBean.setVerified(appCustomDomain.isVerified());
            jsBean.setStatus(appCustomDomain.getStatus());
            jsBean.setVerifiedTime(appCustomDomain.getVerifiedTime());
            jsBean.setCreatedTime(appCustomDomain.getCreatedTime());
            jsBean.setModifiedTime(appCustomDomain.getModifiedTime());
        }
        return jsBean;
    }

    public static AppCustomDomain convertAppCustomDomainJsBeanToBean(AppCustomDomainJsBean jsBean)
    {
        AppCustomDomainBean appCustomDomain = null;
        if(jsBean != null) {
            appCustomDomain = new AppCustomDomainBean();
            appCustomDomain.setGuid(jsBean.getGuid());
            appCustomDomain.setAppId(jsBean.getAppId());
            appCustomDomain.setSiteDomain(jsBean.getSiteDomain());
            appCustomDomain.setDomain(jsBean.getDomain());
            appCustomDomain.setVerified(jsBean.isVerified());
            appCustomDomain.setStatus(jsBean.getStatus());
            appCustomDomain.setVerifiedTime(jsBean.getVerifiedTime());
            appCustomDomain.setCreatedTime(jsBean.getCreatedTime());
            appCustomDomain.setModifiedTime(jsBean.getModifiedTime());
        }
        return appCustomDomain;
    }

}
