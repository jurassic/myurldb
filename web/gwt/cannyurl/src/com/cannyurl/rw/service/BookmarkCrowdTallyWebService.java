package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkCrowdTally;
import com.cannyurl.af.bean.BookmarkCrowdTallyBean;
import com.cannyurl.af.service.BookmarkCrowdTallyService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.BookmarkCrowdTallyJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkCrowdTallyWebService // implements BookmarkCrowdTallyService
{
    private static final Logger log = Logger.getLogger(BookmarkCrowdTallyWebService.class.getName());
     
    // Af service interface.
    private BookmarkCrowdTallyService mService = null;

    public BookmarkCrowdTallyWebService()
    {
        this(ServiceProxyFactory.getInstance().getBookmarkCrowdTallyServiceProxy());
    }
    public BookmarkCrowdTallyWebService(BookmarkCrowdTallyService service)
    {
        mService = service;
    }
    
    private BookmarkCrowdTallyService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getBookmarkCrowdTallyServiceProxy();
        }
        return mService;
    }
    
    
    public BookmarkCrowdTallyJsBean getBookmarkCrowdTally(String guid) throws WebException
    {
        try {
            BookmarkCrowdTally bookmarkCrowdTally = getService().getBookmarkCrowdTally(guid);
            BookmarkCrowdTallyJsBean bean = convertBookmarkCrowdTallyToJsBean(bookmarkCrowdTally);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getBookmarkCrowdTally(String guid, String field) throws WebException
    {
        try {
            return getService().getBookmarkCrowdTally(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkCrowdTallyJsBean> getBookmarkCrowdTallies(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkCrowdTallyJsBean> jsBeans = new ArrayList<BookmarkCrowdTallyJsBean>();
            List<BookmarkCrowdTally> bookmarkCrowdTallies = getService().getBookmarkCrowdTallies(guids);
            if(bookmarkCrowdTallies != null) {
                for(BookmarkCrowdTally bookmarkCrowdTally : bookmarkCrowdTallies) {
                    jsBeans.add(convertBookmarkCrowdTallyToJsBean(bookmarkCrowdTally));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkCrowdTallyJsBean> getAllBookmarkCrowdTallies() throws WebException
    {
        return getAllBookmarkCrowdTallies(null, null, null);
    }

    public List<BookmarkCrowdTallyJsBean> getAllBookmarkCrowdTallies(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<BookmarkCrowdTallyJsBean> jsBeans = new ArrayList<BookmarkCrowdTallyJsBean>();
            List<BookmarkCrowdTally> bookmarkCrowdTallies = getService().getAllBookmarkCrowdTallies(ordering, offset, count);
            if(bookmarkCrowdTallies != null) {
                for(BookmarkCrowdTally bookmarkCrowdTally : bookmarkCrowdTallies) {
                    jsBeans.add(convertBookmarkCrowdTallyToJsBean(bookmarkCrowdTally));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllBookmarkCrowdTallyKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllBookmarkCrowdTallyKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkCrowdTallyJsBean> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findBookmarkCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }

    public List<BookmarkCrowdTallyJsBean> findBookmarkCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<BookmarkCrowdTallyJsBean> jsBeans = new ArrayList<BookmarkCrowdTallyJsBean>();
            List<BookmarkCrowdTally> bookmarkCrowdTallies = getService().findBookmarkCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
            if(bookmarkCrowdTallies != null) {
                for(BookmarkCrowdTally bookmarkCrowdTally : bookmarkCrowdTallies) {
                    jsBeans.add(convertBookmarkCrowdTallyToJsBean(bookmarkCrowdTally));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findBookmarkCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findBookmarkCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws WebException
    {
        try {
            return getService().createBookmarkCrowdTally(user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkCrowdTally(BookmarkCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            BookmarkCrowdTally bookmarkCrowdTally = convertBookmarkCrowdTallyJsBeanToBean(jsBean);
            return getService().createBookmarkCrowdTally(bookmarkCrowdTally);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkCrowdTallyJsBean constructBookmarkCrowdTally(BookmarkCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            BookmarkCrowdTally bookmarkCrowdTally = convertBookmarkCrowdTallyJsBeanToBean(jsBean);
            bookmarkCrowdTally = getService().constructBookmarkCrowdTally(bookmarkCrowdTally);
            jsBean = convertBookmarkCrowdTallyToJsBean(bookmarkCrowdTally);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateBookmarkCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String bookmarkFolder, String contentTag, String referenceElement, String elementType, String keywordLink) throws WebException
    {
        try {
            return getService().updateBookmarkCrowdTally(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, bookmarkFolder, contentTag, referenceElement, elementType, keywordLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateBookmarkCrowdTally(BookmarkCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            BookmarkCrowdTally bookmarkCrowdTally = convertBookmarkCrowdTallyJsBeanToBean(jsBean);
            return getService().updateBookmarkCrowdTally(bookmarkCrowdTally);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkCrowdTallyJsBean refreshBookmarkCrowdTally(BookmarkCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            BookmarkCrowdTally bookmarkCrowdTally = convertBookmarkCrowdTallyJsBeanToBean(jsBean);
            bookmarkCrowdTally = getService().refreshBookmarkCrowdTally(bookmarkCrowdTally);
            jsBean = convertBookmarkCrowdTallyToJsBean(bookmarkCrowdTally);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkCrowdTally(String guid) throws WebException
    {
        try {
            return getService().deleteBookmarkCrowdTally(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkCrowdTally(BookmarkCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            BookmarkCrowdTally bookmarkCrowdTally = convertBookmarkCrowdTallyJsBeanToBean(jsBean);
            return getService().deleteBookmarkCrowdTally(bookmarkCrowdTally);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteBookmarkCrowdTallies(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteBookmarkCrowdTallies(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static BookmarkCrowdTallyJsBean convertBookmarkCrowdTallyToJsBean(BookmarkCrowdTally bookmarkCrowdTally)
    {
        BookmarkCrowdTallyJsBean jsBean = null;
        if(bookmarkCrowdTally != null) {
            jsBean = new BookmarkCrowdTallyJsBean();
            jsBean.setGuid(bookmarkCrowdTally.getGuid());
            jsBean.setUser(bookmarkCrowdTally.getUser());
            jsBean.setShortLink(bookmarkCrowdTally.getShortLink());
            jsBean.setDomain(bookmarkCrowdTally.getDomain());
            jsBean.setToken(bookmarkCrowdTally.getToken());
            jsBean.setLongUrl(bookmarkCrowdTally.getLongUrl());
            jsBean.setShortUrl(bookmarkCrowdTally.getShortUrl());
            jsBean.setTallyDate(bookmarkCrowdTally.getTallyDate());
            jsBean.setStatus(bookmarkCrowdTally.getStatus());
            jsBean.setNote(bookmarkCrowdTally.getNote());
            jsBean.setExpirationTime(bookmarkCrowdTally.getExpirationTime());
            jsBean.setBookmarkFolder(bookmarkCrowdTally.getBookmarkFolder());
            jsBean.setContentTag(bookmarkCrowdTally.getContentTag());
            jsBean.setReferenceElement(bookmarkCrowdTally.getReferenceElement());
            jsBean.setElementType(bookmarkCrowdTally.getElementType());
            jsBean.setKeywordLink(bookmarkCrowdTally.getKeywordLink());
            jsBean.setCreatedTime(bookmarkCrowdTally.getCreatedTime());
            jsBean.setModifiedTime(bookmarkCrowdTally.getModifiedTime());
        }
        return jsBean;
    }

    public static BookmarkCrowdTally convertBookmarkCrowdTallyJsBeanToBean(BookmarkCrowdTallyJsBean jsBean)
    {
        BookmarkCrowdTallyBean bookmarkCrowdTally = null;
        if(jsBean != null) {
            bookmarkCrowdTally = new BookmarkCrowdTallyBean();
            bookmarkCrowdTally.setGuid(jsBean.getGuid());
            bookmarkCrowdTally.setUser(jsBean.getUser());
            bookmarkCrowdTally.setShortLink(jsBean.getShortLink());
            bookmarkCrowdTally.setDomain(jsBean.getDomain());
            bookmarkCrowdTally.setToken(jsBean.getToken());
            bookmarkCrowdTally.setLongUrl(jsBean.getLongUrl());
            bookmarkCrowdTally.setShortUrl(jsBean.getShortUrl());
            bookmarkCrowdTally.setTallyDate(jsBean.getTallyDate());
            bookmarkCrowdTally.setStatus(jsBean.getStatus());
            bookmarkCrowdTally.setNote(jsBean.getNote());
            bookmarkCrowdTally.setExpirationTime(jsBean.getExpirationTime());
            bookmarkCrowdTally.setBookmarkFolder(jsBean.getBookmarkFolder());
            bookmarkCrowdTally.setContentTag(jsBean.getContentTag());
            bookmarkCrowdTally.setReferenceElement(jsBean.getReferenceElement());
            bookmarkCrowdTally.setElementType(jsBean.getElementType());
            bookmarkCrowdTally.setKeywordLink(jsBean.getKeywordLink());
            bookmarkCrowdTally.setCreatedTime(jsBean.getCreatedTime());
            bookmarkCrowdTally.setModifiedTime(jsBean.getModifiedTime());
        }
        return bookmarkCrowdTally;
    }

}
