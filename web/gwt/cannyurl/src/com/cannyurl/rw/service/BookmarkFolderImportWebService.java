package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.BookmarkFolderImport;
import com.cannyurl.af.bean.BookmarkFolderImportBean;
import com.cannyurl.af.service.BookmarkFolderImportService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.BookmarkFolderImportJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class BookmarkFolderImportWebService // implements BookmarkFolderImportService
{
    private static final Logger log = Logger.getLogger(BookmarkFolderImportWebService.class.getName());
     
    // Af service interface.
    private BookmarkFolderImportService mService = null;

    public BookmarkFolderImportWebService()
    {
        this(ServiceProxyFactory.getInstance().getBookmarkFolderImportServiceProxy());
    }
    public BookmarkFolderImportWebService(BookmarkFolderImportService service)
    {
        mService = service;
    }
    
    private BookmarkFolderImportService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getBookmarkFolderImportServiceProxy();
        }
        return mService;
    }
    
    
    public BookmarkFolderImportJsBean getBookmarkFolderImport(String guid) throws WebException
    {
        try {
            BookmarkFolderImport bookmarkFolderImport = getService().getBookmarkFolderImport(guid);
            BookmarkFolderImportJsBean bean = convertBookmarkFolderImportToJsBean(bookmarkFolderImport);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getBookmarkFolderImport(String guid, String field) throws WebException
    {
        try {
            return getService().getBookmarkFolderImport(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkFolderImportJsBean> getBookmarkFolderImports(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<BookmarkFolderImportJsBean> jsBeans = new ArrayList<BookmarkFolderImportJsBean>();
            List<BookmarkFolderImport> bookmarkFolderImports = getService().getBookmarkFolderImports(guids);
            if(bookmarkFolderImports != null) {
                for(BookmarkFolderImport bookmarkFolderImport : bookmarkFolderImports) {
                    jsBeans.add(convertBookmarkFolderImportToJsBean(bookmarkFolderImport));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkFolderImportJsBean> getAllBookmarkFolderImports() throws WebException
    {
        return getAllBookmarkFolderImports(null, null, null);
    }

    public List<BookmarkFolderImportJsBean> getAllBookmarkFolderImports(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<BookmarkFolderImportJsBean> jsBeans = new ArrayList<BookmarkFolderImportJsBean>();
            List<BookmarkFolderImport> bookmarkFolderImports = getService().getAllBookmarkFolderImports(ordering, offset, count);
            if(bookmarkFolderImports != null) {
                for(BookmarkFolderImport bookmarkFolderImport : bookmarkFolderImports) {
                    jsBeans.add(convertBookmarkFolderImportToJsBean(bookmarkFolderImport));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllBookmarkFolderImportKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllBookmarkFolderImportKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<BookmarkFolderImportJsBean> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findBookmarkFolderImports(filter, ordering, params, values, null, null, null, null);
    }

    public List<BookmarkFolderImportJsBean> findBookmarkFolderImports(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<BookmarkFolderImportJsBean> jsBeans = new ArrayList<BookmarkFolderImportJsBean>();
            List<BookmarkFolderImport> bookmarkFolderImports = getService().findBookmarkFolderImports(filter, ordering, params, values, grouping, unique, offset, count);
            if(bookmarkFolderImports != null) {
                for(BookmarkFolderImport bookmarkFolderImport : bookmarkFolderImports) {
                    jsBeans.add(convertBookmarkFolderImportToJsBean(bookmarkFolderImport));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findBookmarkFolderImportKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findBookmarkFolderImportKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkFolderImport(String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws WebException
    {
        try {
            return getService().createBookmarkFolderImport(user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createBookmarkFolderImport(BookmarkFolderImportJsBean jsBean) throws WebException
    {
        try {
            BookmarkFolderImport bookmarkFolderImport = convertBookmarkFolderImportJsBeanToBean(jsBean);
            return getService().createBookmarkFolderImport(bookmarkFolderImport);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkFolderImportJsBean constructBookmarkFolderImport(BookmarkFolderImportJsBean jsBean) throws WebException
    {
        try {
            BookmarkFolderImport bookmarkFolderImport = convertBookmarkFolderImportJsBeanToBean(jsBean);
            bookmarkFolderImport = getService().constructBookmarkFolderImport(bookmarkFolderImport);
            jsBean = convertBookmarkFolderImportToJsBean(bookmarkFolderImport);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateBookmarkFolderImport(String guid, String user, Integer precedence, String importType, String importedFolder, String importedFolderUser, String importedFolderTitle, String importedFolderPath, String status, String note, String bookmarkFolder) throws WebException
    {
        try {
            return getService().updateBookmarkFolderImport(guid, user, precedence, importType, importedFolder, importedFolderUser, importedFolderTitle, importedFolderPath, status, note, bookmarkFolder);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateBookmarkFolderImport(BookmarkFolderImportJsBean jsBean) throws WebException
    {
        try {
            BookmarkFolderImport bookmarkFolderImport = convertBookmarkFolderImportJsBeanToBean(jsBean);
            return getService().updateBookmarkFolderImport(bookmarkFolderImport);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public BookmarkFolderImportJsBean refreshBookmarkFolderImport(BookmarkFolderImportJsBean jsBean) throws WebException
    {
        try {
            BookmarkFolderImport bookmarkFolderImport = convertBookmarkFolderImportJsBeanToBean(jsBean);
            bookmarkFolderImport = getService().refreshBookmarkFolderImport(bookmarkFolderImport);
            jsBean = convertBookmarkFolderImportToJsBean(bookmarkFolderImport);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkFolderImport(String guid) throws WebException
    {
        try {
            return getService().deleteBookmarkFolderImport(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteBookmarkFolderImport(BookmarkFolderImportJsBean jsBean) throws WebException
    {
        try {
            BookmarkFolderImport bookmarkFolderImport = convertBookmarkFolderImportJsBeanToBean(jsBean);
            return getService().deleteBookmarkFolderImport(bookmarkFolderImport);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteBookmarkFolderImports(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteBookmarkFolderImports(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static BookmarkFolderImportJsBean convertBookmarkFolderImportToJsBean(BookmarkFolderImport bookmarkFolderImport)
    {
        BookmarkFolderImportJsBean jsBean = null;
        if(bookmarkFolderImport != null) {
            jsBean = new BookmarkFolderImportJsBean();
            jsBean.setGuid(bookmarkFolderImport.getGuid());
            jsBean.setUser(bookmarkFolderImport.getUser());
            jsBean.setPrecedence(bookmarkFolderImport.getPrecedence());
            jsBean.setImportType(bookmarkFolderImport.getImportType());
            jsBean.setImportedFolder(bookmarkFolderImport.getImportedFolder());
            jsBean.setImportedFolderUser(bookmarkFolderImport.getImportedFolderUser());
            jsBean.setImportedFolderTitle(bookmarkFolderImport.getImportedFolderTitle());
            jsBean.setImportedFolderPath(bookmarkFolderImport.getImportedFolderPath());
            jsBean.setStatus(bookmarkFolderImport.getStatus());
            jsBean.setNote(bookmarkFolderImport.getNote());
            jsBean.setBookmarkFolder(bookmarkFolderImport.getBookmarkFolder());
            jsBean.setCreatedTime(bookmarkFolderImport.getCreatedTime());
            jsBean.setModifiedTime(bookmarkFolderImport.getModifiedTime());
        }
        return jsBean;
    }

    public static BookmarkFolderImport convertBookmarkFolderImportJsBeanToBean(BookmarkFolderImportJsBean jsBean)
    {
        BookmarkFolderImportBean bookmarkFolderImport = null;
        if(jsBean != null) {
            bookmarkFolderImport = new BookmarkFolderImportBean();
            bookmarkFolderImport.setGuid(jsBean.getGuid());
            bookmarkFolderImport.setUser(jsBean.getUser());
            bookmarkFolderImport.setPrecedence(jsBean.getPrecedence());
            bookmarkFolderImport.setImportType(jsBean.getImportType());
            bookmarkFolderImport.setImportedFolder(jsBean.getImportedFolder());
            bookmarkFolderImport.setImportedFolderUser(jsBean.getImportedFolderUser());
            bookmarkFolderImport.setImportedFolderTitle(jsBean.getImportedFolderTitle());
            bookmarkFolderImport.setImportedFolderPath(jsBean.getImportedFolderPath());
            bookmarkFolderImport.setStatus(jsBean.getStatus());
            bookmarkFolderImport.setNote(jsBean.getNote());
            bookmarkFolderImport.setBookmarkFolder(jsBean.getBookmarkFolder());
            bookmarkFolderImport.setCreatedTime(jsBean.getCreatedTime());
            bookmarkFolderImport.setModifiedTime(jsBean.getModifiedTime());
        }
        return bookmarkFolderImport;
    }

}
