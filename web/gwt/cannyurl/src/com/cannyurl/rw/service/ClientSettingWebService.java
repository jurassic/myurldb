package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ClientSetting;
import com.cannyurl.af.bean.ClientSettingBean;
import com.cannyurl.af.service.ClientSettingService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ClientSettingJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ClientSettingWebService // implements ClientSettingService
{
    private static final Logger log = Logger.getLogger(ClientSettingWebService.class.getName());
     
    // Af service interface.
    private ClientSettingService mService = null;

    public ClientSettingWebService()
    {
        this(ServiceProxyFactory.getInstance().getClientSettingServiceProxy());
    }
    public ClientSettingWebService(ClientSettingService service)
    {
        mService = service;
    }
    
    private ClientSettingService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getClientSettingServiceProxy();
        }
        return mService;
    }
    
    
    public ClientSettingJsBean getClientSetting(String guid) throws WebException
    {
        try {
            ClientSetting clientSetting = getService().getClientSetting(guid);
            ClientSettingJsBean bean = convertClientSettingToJsBean(clientSetting);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getClientSetting(String guid, String field) throws WebException
    {
        try {
            return getService().getClientSetting(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ClientSettingJsBean> getClientSettings(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ClientSettingJsBean> jsBeans = new ArrayList<ClientSettingJsBean>();
            List<ClientSetting> clientSettings = getService().getClientSettings(guids);
            if(clientSettings != null) {
                for(ClientSetting clientSetting : clientSettings) {
                    jsBeans.add(convertClientSettingToJsBean(clientSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ClientSettingJsBean> getAllClientSettings() throws WebException
    {
        return getAllClientSettings(null, null, null);
    }

    public List<ClientSettingJsBean> getAllClientSettings(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<ClientSettingJsBean> jsBeans = new ArrayList<ClientSettingJsBean>();
            List<ClientSetting> clientSettings = getService().getAllClientSettings(ordering, offset, count);
            if(clientSettings != null) {
                for(ClientSetting clientSetting : clientSettings) {
                    jsBeans.add(convertClientSettingToJsBean(clientSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllClientSettingKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllClientSettingKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ClientSettingJsBean> findClientSettings(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findClientSettings(filter, ordering, params, values, null, null, null, null);
    }

    public List<ClientSettingJsBean> findClientSettings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<ClientSettingJsBean> jsBeans = new ArrayList<ClientSettingJsBean>();
            List<ClientSetting> clientSettings = getService().findClientSettings(filter, ordering, params, values, grouping, unique, offset, count);
            if(clientSettings != null) {
                for(ClientSetting clientSetting : clientSettings) {
                    jsBeans.add(convertClientSettingToJsBean(clientSetting));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findClientSettingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findClientSettingKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createClientSetting(String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws WebException
    {
        try {
            return getService().createClientSetting(appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createClientSetting(ClientSettingJsBean jsBean) throws WebException
    {
        try {
            ClientSetting clientSetting = convertClientSettingJsBeanToBean(jsBean);
            return getService().createClientSetting(clientSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ClientSettingJsBean constructClientSetting(ClientSettingJsBean jsBean) throws WebException
    {
        try {
            ClientSetting clientSetting = convertClientSettingJsBeanToBean(jsBean);
            clientSetting = getService().constructClientSetting(clientSetting);
            jsBean = convertClientSettingToJsBean(clientSetting);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateClientSetting(String guid, String appClient, String admin, Boolean autoRedirectEnabled, Boolean viewEnabled, Boolean shareEnabled, String defaultBaseDomain, String defaultDomainType, String defaultTokenType, String defaultRedirectType, Long defaultFlashDuration, String defaultAccessType, String defaultViewType) throws WebException
    {
        try {
            return getService().updateClientSetting(guid, appClient, admin, autoRedirectEnabled, viewEnabled, shareEnabled, defaultBaseDomain, defaultDomainType, defaultTokenType, defaultRedirectType, defaultFlashDuration, defaultAccessType, defaultViewType);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateClientSetting(ClientSettingJsBean jsBean) throws WebException
    {
        try {
            ClientSetting clientSetting = convertClientSettingJsBeanToBean(jsBean);
            return getService().updateClientSetting(clientSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ClientSettingJsBean refreshClientSetting(ClientSettingJsBean jsBean) throws WebException
    {
        try {
            ClientSetting clientSetting = convertClientSettingJsBeanToBean(jsBean);
            clientSetting = getService().refreshClientSetting(clientSetting);
            jsBean = convertClientSettingToJsBean(clientSetting);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteClientSetting(String guid) throws WebException
    {
        try {
            return getService().deleteClientSetting(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteClientSetting(ClientSettingJsBean jsBean) throws WebException
    {
        try {
            ClientSetting clientSetting = convertClientSettingJsBeanToBean(jsBean);
            return getService().deleteClientSetting(clientSetting);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteClientSettings(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteClientSettings(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static ClientSettingJsBean convertClientSettingToJsBean(ClientSetting clientSetting)
    {
        ClientSettingJsBean jsBean = null;
        if(clientSetting != null) {
            jsBean = new ClientSettingJsBean();
            jsBean.setGuid(clientSetting.getGuid());
            jsBean.setAppClient(clientSetting.getAppClient());
            jsBean.setAdmin(clientSetting.getAdmin());
            jsBean.setAutoRedirectEnabled(clientSetting.isAutoRedirectEnabled());
            jsBean.setViewEnabled(clientSetting.isViewEnabled());
            jsBean.setShareEnabled(clientSetting.isShareEnabled());
            jsBean.setDefaultBaseDomain(clientSetting.getDefaultBaseDomain());
            jsBean.setDefaultDomainType(clientSetting.getDefaultDomainType());
            jsBean.setDefaultTokenType(clientSetting.getDefaultTokenType());
            jsBean.setDefaultRedirectType(clientSetting.getDefaultRedirectType());
            jsBean.setDefaultFlashDuration(clientSetting.getDefaultFlashDuration());
            jsBean.setDefaultAccessType(clientSetting.getDefaultAccessType());
            jsBean.setDefaultViewType(clientSetting.getDefaultViewType());
            jsBean.setCreatedTime(clientSetting.getCreatedTime());
            jsBean.setModifiedTime(clientSetting.getModifiedTime());
        }
        return jsBean;
    }

    public static ClientSetting convertClientSettingJsBeanToBean(ClientSettingJsBean jsBean)
    {
        ClientSettingBean clientSetting = null;
        if(jsBean != null) {
            clientSetting = new ClientSettingBean();
            clientSetting.setGuid(jsBean.getGuid());
            clientSetting.setAppClient(jsBean.getAppClient());
            clientSetting.setAdmin(jsBean.getAdmin());
            clientSetting.setAutoRedirectEnabled(jsBean.isAutoRedirectEnabled());
            clientSetting.setViewEnabled(jsBean.isViewEnabled());
            clientSetting.setShareEnabled(jsBean.isShareEnabled());
            clientSetting.setDefaultBaseDomain(jsBean.getDefaultBaseDomain());
            clientSetting.setDefaultDomainType(jsBean.getDefaultDomainType());
            clientSetting.setDefaultTokenType(jsBean.getDefaultTokenType());
            clientSetting.setDefaultRedirectType(jsBean.getDefaultRedirectType());
            clientSetting.setDefaultFlashDuration(jsBean.getDefaultFlashDuration());
            clientSetting.setDefaultAccessType(jsBean.getDefaultAccessType());
            clientSetting.setDefaultViewType(jsBean.getDefaultViewType());
            clientSetting.setCreatedTime(jsBean.getCreatedTime());
            clientSetting.setModifiedTime(jsBean.getModifiedTime());
        }
        return clientSetting;
    }

}
