package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ExternalServiceApiKeyStruct;
import com.cannyurl.af.bean.ExternalServiceApiKeyStructBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ExternalServiceApiKeyStructJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ExternalServiceApiKeyStructWebService // implements ExternalServiceApiKeyStructService
{
    private static final Logger log = Logger.getLogger(ExternalServiceApiKeyStructWebService.class.getName());
     
    public static ExternalServiceApiKeyStructJsBean convertExternalServiceApiKeyStructToJsBean(ExternalServiceApiKeyStruct externalServiceApiKeyStruct)
    {
        ExternalServiceApiKeyStructJsBean jsBean = null;
        if(externalServiceApiKeyStruct != null) {
            jsBean = new ExternalServiceApiKeyStructJsBean();
            jsBean.setUuid(externalServiceApiKeyStruct.getUuid());
            jsBean.setService(externalServiceApiKeyStruct.getService());
            jsBean.setKey(externalServiceApiKeyStruct.getKey());
            jsBean.setSecret(externalServiceApiKeyStruct.getSecret());
            jsBean.setNote(externalServiceApiKeyStruct.getNote());
        }
        return jsBean;
    }

    public static ExternalServiceApiKeyStruct convertExternalServiceApiKeyStructJsBeanToBean(ExternalServiceApiKeyStructJsBean jsBean)
    {
        ExternalServiceApiKeyStructBean externalServiceApiKeyStruct = null;
        if(jsBean != null) {
            externalServiceApiKeyStruct = new ExternalServiceApiKeyStructBean();
            externalServiceApiKeyStruct.setUuid(jsBean.getUuid());
            externalServiceApiKeyStruct.setService(jsBean.getService());
            externalServiceApiKeyStruct.setKey(jsBean.getKey());
            externalServiceApiKeyStruct.setSecret(jsBean.getSecret());
            externalServiceApiKeyStruct.setNote(jsBean.getNote());
        }
        return externalServiceApiKeyStruct;
    }

}
