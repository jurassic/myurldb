package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.GaeAppStruct;
import com.cannyurl.af.bean.GaeAppStructBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.GaeAppStructJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GaeAppStructWebService // implements GaeAppStructService
{
    private static final Logger log = Logger.getLogger(GaeAppStructWebService.class.getName());
     
    public static GaeAppStructJsBean convertGaeAppStructToJsBean(GaeAppStruct gaeAppStruct)
    {
        GaeAppStructJsBean jsBean = null;
        if(gaeAppStruct != null) {
            jsBean = new GaeAppStructJsBean();
            jsBean.setGroupId(gaeAppStruct.getGroupId());
            jsBean.setAppId(gaeAppStruct.getAppId());
            jsBean.setAppDomain(gaeAppStruct.getAppDomain());
            jsBean.setNamespace(gaeAppStruct.getNamespace());
            jsBean.setAcl(gaeAppStruct.getAcl());
            jsBean.setNote(gaeAppStruct.getNote());
        }
        return jsBean;
    }

    public static GaeAppStruct convertGaeAppStructJsBeanToBean(GaeAppStructJsBean jsBean)
    {
        GaeAppStructBean gaeAppStruct = null;
        if(jsBean != null) {
            gaeAppStruct = new GaeAppStructBean();
            gaeAppStruct.setGroupId(jsBean.getGroupId());
            gaeAppStruct.setAppId(jsBean.getAppId());
            gaeAppStruct.setAppDomain(jsBean.getAppDomain());
            gaeAppStruct.setNamespace(jsBean.getNamespace());
            gaeAppStruct.setAcl(jsBean.getAcl());
            gaeAppStruct.setNote(jsBean.getNote());
        }
        return gaeAppStruct;
    }

}
