package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.GeoCoordinateStruct;
import com.cannyurl.af.bean.GeoCoordinateStructBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.GeoCoordinateStructJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GeoCoordinateStructWebService // implements GeoCoordinateStructService
{
    private static final Logger log = Logger.getLogger(GeoCoordinateStructWebService.class.getName());
     
    public static GeoCoordinateStructJsBean convertGeoCoordinateStructToJsBean(GeoCoordinateStruct geoCoordinateStruct)
    {
        GeoCoordinateStructJsBean jsBean = null;
        if(geoCoordinateStruct != null) {
            jsBean = new GeoCoordinateStructJsBean();
            jsBean.setUuid(geoCoordinateStruct.getUuid());
            jsBean.setLatitude(geoCoordinateStruct.getLatitude());
            jsBean.setLongitude(geoCoordinateStruct.getLongitude());
            jsBean.setAltitude(geoCoordinateStruct.getAltitude());
            jsBean.setSensorUsed(geoCoordinateStruct.isSensorUsed());
            jsBean.setAccuracy(geoCoordinateStruct.getAccuracy());
            jsBean.setAltitudeAccuracy(geoCoordinateStruct.getAltitudeAccuracy());
            jsBean.setHeading(geoCoordinateStruct.getHeading());
            jsBean.setSpeed(geoCoordinateStruct.getSpeed());
            jsBean.setNote(geoCoordinateStruct.getNote());
        }
        return jsBean;
    }

    public static GeoCoordinateStruct convertGeoCoordinateStructJsBeanToBean(GeoCoordinateStructJsBean jsBean)
    {
        GeoCoordinateStructBean geoCoordinateStruct = null;
        if(jsBean != null) {
            geoCoordinateStruct = new GeoCoordinateStructBean();
            geoCoordinateStruct.setUuid(jsBean.getUuid());
            geoCoordinateStruct.setLatitude(jsBean.getLatitude());
            geoCoordinateStruct.setLongitude(jsBean.getLongitude());
            geoCoordinateStruct.setAltitude(jsBean.getAltitude());
            geoCoordinateStruct.setSensorUsed(jsBean.isSensorUsed());
            geoCoordinateStruct.setAccuracy(jsBean.getAccuracy());
            geoCoordinateStruct.setAltitudeAccuracy(jsBean.getAltitudeAccuracy());
            geoCoordinateStruct.setHeading(jsBean.getHeading());
            geoCoordinateStruct.setSpeed(jsBean.getSpeed());
            geoCoordinateStruct.setNote(jsBean.getNote());
        }
        return geoCoordinateStruct;
    }

}
