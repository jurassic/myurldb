package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.CellLatitudeLongitude;
import com.myurldb.ws.GeoCoordinateStruct;
import com.myurldb.ws.GeoLink;
import com.cannyurl.af.bean.GeoLinkBean;
import com.cannyurl.af.service.GeoLinkService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.CellLatitudeLongitudeJsBean;
import com.cannyurl.fe.bean.GeoCoordinateStructJsBean;
import com.cannyurl.fe.bean.GeoLinkJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class GeoLinkWebService // implements GeoLinkService
{
    private static final Logger log = Logger.getLogger(GeoLinkWebService.class.getName());
     
    // Af service interface.
    private GeoLinkService mService = null;

    public GeoLinkWebService()
    {
        this(ServiceProxyFactory.getInstance().getGeoLinkServiceProxy());
    }
    public GeoLinkWebService(GeoLinkService service)
    {
        mService = service;
    }
    
    private GeoLinkService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getGeoLinkServiceProxy();
        }
        return mService;
    }
    
    
    public GeoLinkJsBean getGeoLink(String guid) throws WebException
    {
        try {
            GeoLink geoLink = getService().getGeoLink(guid);
            GeoLinkJsBean bean = convertGeoLinkToJsBean(geoLink);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getGeoLink(String guid, String field) throws WebException
    {
        try {
            return getService().getGeoLink(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<GeoLinkJsBean> getGeoLinks(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<GeoLinkJsBean> jsBeans = new ArrayList<GeoLinkJsBean>();
            List<GeoLink> geoLinks = getService().getGeoLinks(guids);
            if(geoLinks != null) {
                for(GeoLink geoLink : geoLinks) {
                    jsBeans.add(convertGeoLinkToJsBean(geoLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<GeoLinkJsBean> getAllGeoLinks() throws WebException
    {
        return getAllGeoLinks(null, null, null);
    }

    public List<GeoLinkJsBean> getAllGeoLinks(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<GeoLinkJsBean> jsBeans = new ArrayList<GeoLinkJsBean>();
            List<GeoLink> geoLinks = getService().getAllGeoLinks(ordering, offset, count);
            if(geoLinks != null) {
                for(GeoLink geoLink : geoLinks) {
                    jsBeans.add(convertGeoLinkToJsBean(geoLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllGeoLinkKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllGeoLinkKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<GeoLinkJsBean> findGeoLinks(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findGeoLinks(filter, ordering, params, values, null, null, null, null);
    }

    public List<GeoLinkJsBean> findGeoLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<GeoLinkJsBean> jsBeans = new ArrayList<GeoLinkJsBean>();
            List<GeoLink> geoLinks = getService().findGeoLinks(filter, ordering, params, values, grouping, unique, offset, count);
            if(geoLinks != null) {
                for(GeoLink geoLink : geoLinks) {
                    jsBeans.add(convertGeoLinkToJsBean(geoLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findGeoLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findGeoLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createGeoLink(String shortLink, String shortUrl, GeoCoordinateStructJsBean geoCoordinate, CellLatitudeLongitudeJsBean geoCell, String status) throws WebException
    {
        try {
            return getService().createGeoLink(shortLink, shortUrl, GeoCoordinateStructWebService.convertGeoCoordinateStructJsBeanToBean(geoCoordinate), CellLatitudeLongitudeWebService.convertCellLatitudeLongitudeJsBeanToBean(geoCell), status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createGeoLink(GeoLinkJsBean jsBean) throws WebException
    {
        try {
            GeoLink geoLink = convertGeoLinkJsBeanToBean(jsBean);
            return getService().createGeoLink(geoLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public GeoLinkJsBean constructGeoLink(GeoLinkJsBean jsBean) throws WebException
    {
        try {
            GeoLink geoLink = convertGeoLinkJsBeanToBean(jsBean);
            geoLink = getService().constructGeoLink(geoLink);
            jsBean = convertGeoLinkToJsBean(geoLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateGeoLink(String guid, String shortLink, String shortUrl, GeoCoordinateStructJsBean geoCoordinate, CellLatitudeLongitudeJsBean geoCell, String status) throws WebException
    {
        try {
            return getService().updateGeoLink(guid, shortLink, shortUrl, GeoCoordinateStructWebService.convertGeoCoordinateStructJsBeanToBean(geoCoordinate), CellLatitudeLongitudeWebService.convertCellLatitudeLongitudeJsBeanToBean(geoCell), status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateGeoLink(GeoLinkJsBean jsBean) throws WebException
    {
        try {
            GeoLink geoLink = convertGeoLinkJsBeanToBean(jsBean);
            return getService().updateGeoLink(geoLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public GeoLinkJsBean refreshGeoLink(GeoLinkJsBean jsBean) throws WebException
    {
        try {
            GeoLink geoLink = convertGeoLinkJsBeanToBean(jsBean);
            geoLink = getService().refreshGeoLink(geoLink);
            jsBean = convertGeoLinkToJsBean(geoLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteGeoLink(String guid) throws WebException
    {
        try {
            return getService().deleteGeoLink(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteGeoLink(GeoLinkJsBean jsBean) throws WebException
    {
        try {
            GeoLink geoLink = convertGeoLinkJsBeanToBean(jsBean);
            return getService().deleteGeoLink(geoLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteGeoLinks(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteGeoLinks(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static GeoLinkJsBean convertGeoLinkToJsBean(GeoLink geoLink)
    {
        GeoLinkJsBean jsBean = null;
        if(geoLink != null) {
            jsBean = new GeoLinkJsBean();
            jsBean.setGuid(geoLink.getGuid());
            jsBean.setShortLink(geoLink.getShortLink());
            jsBean.setShortUrl(geoLink.getShortUrl());
            jsBean.setGeoCoordinate(GeoCoordinateStructWebService.convertGeoCoordinateStructToJsBean(geoLink.getGeoCoordinate()));
            jsBean.setGeoCell(CellLatitudeLongitudeWebService.convertCellLatitudeLongitudeToJsBean(geoLink.getGeoCell()));
            jsBean.setStatus(geoLink.getStatus());
            jsBean.setCreatedTime(geoLink.getCreatedTime());
            jsBean.setModifiedTime(geoLink.getModifiedTime());
        }
        return jsBean;
    }

    public static GeoLink convertGeoLinkJsBeanToBean(GeoLinkJsBean jsBean)
    {
        GeoLinkBean geoLink = null;
        if(jsBean != null) {
            geoLink = new GeoLinkBean();
            geoLink.setGuid(jsBean.getGuid());
            geoLink.setShortLink(jsBean.getShortLink());
            geoLink.setShortUrl(jsBean.getShortUrl());
            geoLink.setGeoCoordinate(GeoCoordinateStructWebService.convertGeoCoordinateStructJsBeanToBean(jsBean.getGeoCoordinate()));
            geoLink.setGeoCell(CellLatitudeLongitudeWebService.convertCellLatitudeLongitudeJsBeanToBean(jsBean.getGeoCell()));
            geoLink.setStatus(jsBean.getStatus());
            geoLink.setCreatedTime(jsBean.getCreatedTime());
            geoLink.setModifiedTime(jsBean.getModifiedTime());
        }
        return geoLink;
    }

}
