package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.HelpNotice;
import com.cannyurl.af.bean.HelpNoticeBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.HelpNoticeJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class HelpNoticeWebService // implements HelpNoticeService
{
    private static final Logger log = Logger.getLogger(HelpNoticeWebService.class.getName());
     
    public static HelpNoticeJsBean convertHelpNoticeToJsBean(HelpNotice helpNotice)
    {
        HelpNoticeJsBean jsBean = null;
        if(helpNotice != null) {
            jsBean = new HelpNoticeJsBean();
            jsBean.setUuid(helpNotice.getUuid());
            jsBean.setTitle(helpNotice.getTitle());
            jsBean.setContent(helpNotice.getContent());
            jsBean.setFormat(helpNotice.getFormat());
            jsBean.setNote(helpNotice.getNote());
            jsBean.setStatus(helpNotice.getStatus());
        }
        return jsBean;
    }

    public static HelpNotice convertHelpNoticeJsBeanToBean(HelpNoticeJsBean jsBean)
    {
        HelpNoticeBean helpNotice = null;
        if(jsBean != null) {
            helpNotice = new HelpNoticeBean();
            helpNotice.setUuid(jsBean.getUuid());
            helpNotice.setTitle(jsBean.getTitle());
            helpNotice.setContent(jsBean.getContent());
            helpNotice.setFormat(jsBean.getFormat());
            helpNotice.setNote(jsBean.getNote());
            helpNotice.setStatus(jsBean.getStatus());
        }
        return helpNotice;
    }

}
