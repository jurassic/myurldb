package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordCrowdTally;
import com.cannyurl.af.bean.KeywordCrowdTallyBean;
import com.cannyurl.af.service.KeywordCrowdTallyService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.KeywordCrowdTallyJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordCrowdTallyWebService // implements KeywordCrowdTallyService
{
    private static final Logger log = Logger.getLogger(KeywordCrowdTallyWebService.class.getName());
     
    // Af service interface.
    private KeywordCrowdTallyService mService = null;

    public KeywordCrowdTallyWebService()
    {
        this(ServiceProxyFactory.getInstance().getKeywordCrowdTallyServiceProxy());
    }
    public KeywordCrowdTallyWebService(KeywordCrowdTallyService service)
    {
        mService = service;
    }
    
    private KeywordCrowdTallyService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getKeywordCrowdTallyServiceProxy();
        }
        return mService;
    }
    
    
    public KeywordCrowdTallyJsBean getKeywordCrowdTally(String guid) throws WebException
    {
        try {
            KeywordCrowdTally keywordCrowdTally = getService().getKeywordCrowdTally(guid);
            KeywordCrowdTallyJsBean bean = convertKeywordCrowdTallyToJsBean(keywordCrowdTally);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getKeywordCrowdTally(String guid, String field) throws WebException
    {
        try {
            return getService().getKeywordCrowdTally(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordCrowdTallyJsBean> getKeywordCrowdTallies(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordCrowdTallyJsBean> jsBeans = new ArrayList<KeywordCrowdTallyJsBean>();
            List<KeywordCrowdTally> keywordCrowdTallies = getService().getKeywordCrowdTallies(guids);
            if(keywordCrowdTallies != null) {
                for(KeywordCrowdTally keywordCrowdTally : keywordCrowdTallies) {
                    jsBeans.add(convertKeywordCrowdTallyToJsBean(keywordCrowdTally));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordCrowdTallyJsBean> getAllKeywordCrowdTallies() throws WebException
    {
        return getAllKeywordCrowdTallies(null, null, null);
    }

    public List<KeywordCrowdTallyJsBean> getAllKeywordCrowdTallies(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<KeywordCrowdTallyJsBean> jsBeans = new ArrayList<KeywordCrowdTallyJsBean>();
            List<KeywordCrowdTally> keywordCrowdTallies = getService().getAllKeywordCrowdTallies(ordering, offset, count);
            if(keywordCrowdTallies != null) {
                for(KeywordCrowdTally keywordCrowdTally : keywordCrowdTallies) {
                    jsBeans.add(convertKeywordCrowdTallyToJsBean(keywordCrowdTally));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllKeywordCrowdTallyKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllKeywordCrowdTallyKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordCrowdTallyJsBean> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findKeywordCrowdTallies(filter, ordering, params, values, null, null, null, null);
    }

    public List<KeywordCrowdTallyJsBean> findKeywordCrowdTallies(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<KeywordCrowdTallyJsBean> jsBeans = new ArrayList<KeywordCrowdTallyJsBean>();
            List<KeywordCrowdTally> keywordCrowdTallies = getService().findKeywordCrowdTallies(filter, ordering, params, values, grouping, unique, offset, count);
            if(keywordCrowdTallies != null) {
                for(KeywordCrowdTally keywordCrowdTally : keywordCrowdTallies) {
                    jsBeans.add(convertKeywordCrowdTallyToJsBean(keywordCrowdTally));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findKeywordCrowdTallyKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findKeywordCrowdTallyKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordCrowdTally(String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws WebException
    {
        try {
            return getService().createKeywordCrowdTally(user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordCrowdTally(KeywordCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            KeywordCrowdTally keywordCrowdTally = convertKeywordCrowdTallyJsBeanToBean(jsBean);
            return getService().createKeywordCrowdTally(keywordCrowdTally);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordCrowdTallyJsBean constructKeywordCrowdTally(KeywordCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            KeywordCrowdTally keywordCrowdTally = convertKeywordCrowdTallyJsBeanToBean(jsBean);
            keywordCrowdTally = getService().constructKeywordCrowdTally(keywordCrowdTally);
            jsBean = convertKeywordCrowdTallyToJsBean(keywordCrowdTally);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateKeywordCrowdTally(String guid, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, String tallyDate, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean caseSensitive) throws WebException
    {
        try {
            return getService().updateKeywordCrowdTally(guid, user, shortLink, domain, token, longUrl, shortUrl, tallyDate, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, caseSensitive);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateKeywordCrowdTally(KeywordCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            KeywordCrowdTally keywordCrowdTally = convertKeywordCrowdTallyJsBeanToBean(jsBean);
            return getService().updateKeywordCrowdTally(keywordCrowdTally);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordCrowdTallyJsBean refreshKeywordCrowdTally(KeywordCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            KeywordCrowdTally keywordCrowdTally = convertKeywordCrowdTallyJsBeanToBean(jsBean);
            keywordCrowdTally = getService().refreshKeywordCrowdTally(keywordCrowdTally);
            jsBean = convertKeywordCrowdTallyToJsBean(keywordCrowdTally);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordCrowdTally(String guid) throws WebException
    {
        try {
            return getService().deleteKeywordCrowdTally(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordCrowdTally(KeywordCrowdTallyJsBean jsBean) throws WebException
    {
        try {
            KeywordCrowdTally keywordCrowdTally = convertKeywordCrowdTallyJsBeanToBean(jsBean);
            return getService().deleteKeywordCrowdTally(keywordCrowdTally);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteKeywordCrowdTallies(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteKeywordCrowdTallies(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static KeywordCrowdTallyJsBean convertKeywordCrowdTallyToJsBean(KeywordCrowdTally keywordCrowdTally)
    {
        KeywordCrowdTallyJsBean jsBean = null;
        if(keywordCrowdTally != null) {
            jsBean = new KeywordCrowdTallyJsBean();
            jsBean.setGuid(keywordCrowdTally.getGuid());
            jsBean.setUser(keywordCrowdTally.getUser());
            jsBean.setShortLink(keywordCrowdTally.getShortLink());
            jsBean.setDomain(keywordCrowdTally.getDomain());
            jsBean.setToken(keywordCrowdTally.getToken());
            jsBean.setLongUrl(keywordCrowdTally.getLongUrl());
            jsBean.setShortUrl(keywordCrowdTally.getShortUrl());
            jsBean.setTallyDate(keywordCrowdTally.getTallyDate());
            jsBean.setStatus(keywordCrowdTally.getStatus());
            jsBean.setNote(keywordCrowdTally.getNote());
            jsBean.setExpirationTime(keywordCrowdTally.getExpirationTime());
            jsBean.setKeywordFolder(keywordCrowdTally.getKeywordFolder());
            jsBean.setFolderPath(keywordCrowdTally.getFolderPath());
            jsBean.setKeyword(keywordCrowdTally.getKeyword());
            jsBean.setQueryKey(keywordCrowdTally.getQueryKey());
            jsBean.setScope(keywordCrowdTally.getScope());
            jsBean.setCaseSensitive(keywordCrowdTally.isCaseSensitive());
            jsBean.setCreatedTime(keywordCrowdTally.getCreatedTime());
            jsBean.setModifiedTime(keywordCrowdTally.getModifiedTime());
        }
        return jsBean;
    }

    public static KeywordCrowdTally convertKeywordCrowdTallyJsBeanToBean(KeywordCrowdTallyJsBean jsBean)
    {
        KeywordCrowdTallyBean keywordCrowdTally = null;
        if(jsBean != null) {
            keywordCrowdTally = new KeywordCrowdTallyBean();
            keywordCrowdTally.setGuid(jsBean.getGuid());
            keywordCrowdTally.setUser(jsBean.getUser());
            keywordCrowdTally.setShortLink(jsBean.getShortLink());
            keywordCrowdTally.setDomain(jsBean.getDomain());
            keywordCrowdTally.setToken(jsBean.getToken());
            keywordCrowdTally.setLongUrl(jsBean.getLongUrl());
            keywordCrowdTally.setShortUrl(jsBean.getShortUrl());
            keywordCrowdTally.setTallyDate(jsBean.getTallyDate());
            keywordCrowdTally.setStatus(jsBean.getStatus());
            keywordCrowdTally.setNote(jsBean.getNote());
            keywordCrowdTally.setExpirationTime(jsBean.getExpirationTime());
            keywordCrowdTally.setKeywordFolder(jsBean.getKeywordFolder());
            keywordCrowdTally.setFolderPath(jsBean.getFolderPath());
            keywordCrowdTally.setKeyword(jsBean.getKeyword());
            keywordCrowdTally.setQueryKey(jsBean.getQueryKey());
            keywordCrowdTally.setScope(jsBean.getScope());
            keywordCrowdTally.setCaseSensitive(jsBean.isCaseSensitive());
            keywordCrowdTally.setCreatedTime(jsBean.getCreatedTime());
            keywordCrowdTally.setModifiedTime(jsBean.getModifiedTime());
        }
        return keywordCrowdTally;
    }

}
