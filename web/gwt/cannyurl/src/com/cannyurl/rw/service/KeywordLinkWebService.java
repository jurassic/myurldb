package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.KeywordLink;
import com.cannyurl.af.bean.KeywordLinkBean;
import com.cannyurl.af.service.KeywordLinkService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.KeywordLinkJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class KeywordLinkWebService // implements KeywordLinkService
{
    private static final Logger log = Logger.getLogger(KeywordLinkWebService.class.getName());
     
    // Af service interface.
    private KeywordLinkService mService = null;

    public KeywordLinkWebService()
    {
        this(ServiceProxyFactory.getInstance().getKeywordLinkServiceProxy());
    }
    public KeywordLinkWebService(KeywordLinkService service)
    {
        mService = service;
    }
    
    private KeywordLinkService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getKeywordLinkServiceProxy();
        }
        return mService;
    }
    
    
    public KeywordLinkJsBean getKeywordLink(String guid) throws WebException
    {
        try {
            KeywordLink keywordLink = getService().getKeywordLink(guid);
            KeywordLinkJsBean bean = convertKeywordLinkToJsBean(keywordLink);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getKeywordLink(String guid, String field) throws WebException
    {
        try {
            return getService().getKeywordLink(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordLinkJsBean> getKeywordLinks(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<KeywordLinkJsBean> jsBeans = new ArrayList<KeywordLinkJsBean>();
            List<KeywordLink> keywordLinks = getService().getKeywordLinks(guids);
            if(keywordLinks != null) {
                for(KeywordLink keywordLink : keywordLinks) {
                    jsBeans.add(convertKeywordLinkToJsBean(keywordLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordLinkJsBean> getAllKeywordLinks() throws WebException
    {
        return getAllKeywordLinks(null, null, null);
    }

    public List<KeywordLinkJsBean> getAllKeywordLinks(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<KeywordLinkJsBean> jsBeans = new ArrayList<KeywordLinkJsBean>();
            List<KeywordLink> keywordLinks = getService().getAllKeywordLinks(ordering, offset, count);
            if(keywordLinks != null) {
                for(KeywordLink keywordLink : keywordLinks) {
                    jsBeans.add(convertKeywordLinkToJsBean(keywordLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllKeywordLinkKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllKeywordLinkKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<KeywordLinkJsBean> findKeywordLinks(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findKeywordLinks(filter, ordering, params, values, null, null, null, null);
    }

    public List<KeywordLinkJsBean> findKeywordLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<KeywordLinkJsBean> jsBeans = new ArrayList<KeywordLinkJsBean>();
            List<KeywordLink> keywordLinks = getService().findKeywordLinks(filter, ordering, params, values, grouping, unique, offset, count);
            if(keywordLinks != null) {
                for(KeywordLink keywordLink : keywordLinks) {
                    jsBeans.add(convertKeywordLinkToJsBean(keywordLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findKeywordLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findKeywordLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordLink(String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws WebException
    {
        try {
            return getService().createKeywordLink(appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createKeywordLink(KeywordLinkJsBean jsBean) throws WebException
    {
        try {
            KeywordLink keywordLink = convertKeywordLinkJsBeanToBean(jsBean);
            return getService().createKeywordLink(keywordLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordLinkJsBean constructKeywordLink(KeywordLinkJsBean jsBean) throws WebException
    {
        try {
            KeywordLink keywordLink = convertKeywordLinkJsBeanToBean(jsBean);
            keywordLink = getService().constructKeywordLink(keywordLink);
            jsBean = convertKeywordLinkToJsBean(keywordLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateKeywordLink(String guid, String appClient, String clientRootDomain, String user, String shortLink, String domain, String token, String longUrl, String shortUrl, Boolean internal, Boolean caching, String memo, String status, String note, Long expirationTime, String keywordFolder, String folderPath, String keyword, String queryKey, String scope, Boolean dynamic, Boolean caseSensitive) throws WebException
    {
        try {
            return getService().updateKeywordLink(guid, appClient, clientRootDomain, user, shortLink, domain, token, longUrl, shortUrl, internal, caching, memo, status, note, expirationTime, keywordFolder, folderPath, keyword, queryKey, scope, dynamic, caseSensitive);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateKeywordLink(KeywordLinkJsBean jsBean) throws WebException
    {
        try {
            KeywordLink keywordLink = convertKeywordLinkJsBeanToBean(jsBean);
            return getService().updateKeywordLink(keywordLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public KeywordLinkJsBean refreshKeywordLink(KeywordLinkJsBean jsBean) throws WebException
    {
        try {
            KeywordLink keywordLink = convertKeywordLinkJsBeanToBean(jsBean);
            keywordLink = getService().refreshKeywordLink(keywordLink);
            jsBean = convertKeywordLinkToJsBean(keywordLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordLink(String guid) throws WebException
    {
        try {
            return getService().deleteKeywordLink(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteKeywordLink(KeywordLinkJsBean jsBean) throws WebException
    {
        try {
            KeywordLink keywordLink = convertKeywordLinkJsBeanToBean(jsBean);
            return getService().deleteKeywordLink(keywordLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteKeywordLinks(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteKeywordLinks(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static KeywordLinkJsBean convertKeywordLinkToJsBean(KeywordLink keywordLink)
    {
        KeywordLinkJsBean jsBean = null;
        if(keywordLink != null) {
            jsBean = new KeywordLinkJsBean();
            jsBean.setGuid(keywordLink.getGuid());
            jsBean.setAppClient(keywordLink.getAppClient());
            jsBean.setClientRootDomain(keywordLink.getClientRootDomain());
            jsBean.setUser(keywordLink.getUser());
            jsBean.setShortLink(keywordLink.getShortLink());
            jsBean.setDomain(keywordLink.getDomain());
            jsBean.setToken(keywordLink.getToken());
            jsBean.setLongUrl(keywordLink.getLongUrl());
            jsBean.setShortUrl(keywordLink.getShortUrl());
            jsBean.setInternal(keywordLink.isInternal());
            jsBean.setCaching(keywordLink.isCaching());
            jsBean.setMemo(keywordLink.getMemo());
            jsBean.setStatus(keywordLink.getStatus());
            jsBean.setNote(keywordLink.getNote());
            jsBean.setExpirationTime(keywordLink.getExpirationTime());
            jsBean.setKeywordFolder(keywordLink.getKeywordFolder());
            jsBean.setFolderPath(keywordLink.getFolderPath());
            jsBean.setKeyword(keywordLink.getKeyword());
            jsBean.setQueryKey(keywordLink.getQueryKey());
            jsBean.setScope(keywordLink.getScope());
            jsBean.setDynamic(keywordLink.isDynamic());
            jsBean.setCaseSensitive(keywordLink.isCaseSensitive());
            jsBean.setCreatedTime(keywordLink.getCreatedTime());
            jsBean.setModifiedTime(keywordLink.getModifiedTime());
        }
        return jsBean;
    }

    public static KeywordLink convertKeywordLinkJsBeanToBean(KeywordLinkJsBean jsBean)
    {
        KeywordLinkBean keywordLink = null;
        if(jsBean != null) {
            keywordLink = new KeywordLinkBean();
            keywordLink.setGuid(jsBean.getGuid());
            keywordLink.setAppClient(jsBean.getAppClient());
            keywordLink.setClientRootDomain(jsBean.getClientRootDomain());
            keywordLink.setUser(jsBean.getUser());
            keywordLink.setShortLink(jsBean.getShortLink());
            keywordLink.setDomain(jsBean.getDomain());
            keywordLink.setToken(jsBean.getToken());
            keywordLink.setLongUrl(jsBean.getLongUrl());
            keywordLink.setShortUrl(jsBean.getShortUrl());
            keywordLink.setInternal(jsBean.isInternal());
            keywordLink.setCaching(jsBean.isCaching());
            keywordLink.setMemo(jsBean.getMemo());
            keywordLink.setStatus(jsBean.getStatus());
            keywordLink.setNote(jsBean.getNote());
            keywordLink.setExpirationTime(jsBean.getExpirationTime());
            keywordLink.setKeywordFolder(jsBean.getKeywordFolder());
            keywordLink.setFolderPath(jsBean.getFolderPath());
            keywordLink.setKeyword(jsBean.getKeyword());
            keywordLink.setQueryKey(jsBean.getQueryKey());
            keywordLink.setScope(jsBean.getScope());
            keywordLink.setDynamic(jsBean.isDynamic());
            keywordLink.setCaseSensitive(jsBean.isCaseSensitive());
            keywordLink.setCreatedTime(jsBean.getCreatedTime());
            keywordLink.setModifiedTime(jsBean.getModifiedTime());
        }
        return keywordLink;
    }

}
