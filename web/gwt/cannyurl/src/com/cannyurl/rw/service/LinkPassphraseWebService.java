package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.LinkPassphrase;
import com.cannyurl.af.bean.LinkPassphraseBean;
import com.cannyurl.af.service.LinkPassphraseService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.LinkPassphraseJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class LinkPassphraseWebService // implements LinkPassphraseService
{
    private static final Logger log = Logger.getLogger(LinkPassphraseWebService.class.getName());
     
    // Af service interface.
    private LinkPassphraseService mService = null;

    public LinkPassphraseWebService()
    {
        this(ServiceProxyFactory.getInstance().getLinkPassphraseServiceProxy());
    }
    public LinkPassphraseWebService(LinkPassphraseService service)
    {
        mService = service;
    }
    
    private LinkPassphraseService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getLinkPassphraseServiceProxy();
        }
        return mService;
    }
    
    
    public LinkPassphraseJsBean getLinkPassphrase(String guid) throws WebException
    {
        try {
            LinkPassphrase linkPassphrase = getService().getLinkPassphrase(guid);
            LinkPassphraseJsBean bean = convertLinkPassphraseToJsBean(linkPassphrase);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getLinkPassphrase(String guid, String field) throws WebException
    {
        try {
            return getService().getLinkPassphrase(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkPassphraseJsBean> getLinkPassphrases(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<LinkPassphraseJsBean> jsBeans = new ArrayList<LinkPassphraseJsBean>();
            List<LinkPassphrase> linkPassphrases = getService().getLinkPassphrases(guids);
            if(linkPassphrases != null) {
                for(LinkPassphrase linkPassphrase : linkPassphrases) {
                    jsBeans.add(convertLinkPassphraseToJsBean(linkPassphrase));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkPassphraseJsBean> getAllLinkPassphrases() throws WebException
    {
        return getAllLinkPassphrases(null, null, null);
    }

    public List<LinkPassphraseJsBean> getAllLinkPassphrases(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<LinkPassphraseJsBean> jsBeans = new ArrayList<LinkPassphraseJsBean>();
            List<LinkPassphrase> linkPassphrases = getService().getAllLinkPassphrases(ordering, offset, count);
            if(linkPassphrases != null) {
                for(LinkPassphrase linkPassphrase : linkPassphrases) {
                    jsBeans.add(convertLinkPassphraseToJsBean(linkPassphrase));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllLinkPassphraseKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllLinkPassphraseKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<LinkPassphraseJsBean> findLinkPassphrases(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findLinkPassphrases(filter, ordering, params, values, null, null, null, null);
    }

    public List<LinkPassphraseJsBean> findLinkPassphrases(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<LinkPassphraseJsBean> jsBeans = new ArrayList<LinkPassphraseJsBean>();
            List<LinkPassphrase> linkPassphrases = getService().findLinkPassphrases(filter, ordering, params, values, grouping, unique, offset, count);
            if(linkPassphrases != null) {
                for(LinkPassphrase linkPassphrase : linkPassphrases) {
                    jsBeans.add(convertLinkPassphraseToJsBean(linkPassphrase));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findLinkPassphraseKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findLinkPassphraseKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkPassphrase(String shortLink, String passphrase) throws WebException
    {
        try {
            return getService().createLinkPassphrase(shortLink, passphrase);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createLinkPassphrase(LinkPassphraseJsBean jsBean) throws WebException
    {
        try {
            LinkPassphrase linkPassphrase = convertLinkPassphraseJsBeanToBean(jsBean);
            return getService().createLinkPassphrase(linkPassphrase);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkPassphraseJsBean constructLinkPassphrase(LinkPassphraseJsBean jsBean) throws WebException
    {
        try {
            LinkPassphrase linkPassphrase = convertLinkPassphraseJsBeanToBean(jsBean);
            linkPassphrase = getService().constructLinkPassphrase(linkPassphrase);
            jsBean = convertLinkPassphraseToJsBean(linkPassphrase);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateLinkPassphrase(String guid, String shortLink, String passphrase) throws WebException
    {
        try {
            return getService().updateLinkPassphrase(guid, shortLink, passphrase);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateLinkPassphrase(LinkPassphraseJsBean jsBean) throws WebException
    {
        try {
            LinkPassphrase linkPassphrase = convertLinkPassphraseJsBeanToBean(jsBean);
            return getService().updateLinkPassphrase(linkPassphrase);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public LinkPassphraseJsBean refreshLinkPassphrase(LinkPassphraseJsBean jsBean) throws WebException
    {
        try {
            LinkPassphrase linkPassphrase = convertLinkPassphraseJsBeanToBean(jsBean);
            linkPassphrase = getService().refreshLinkPassphrase(linkPassphrase);
            jsBean = convertLinkPassphraseToJsBean(linkPassphrase);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkPassphrase(String guid) throws WebException
    {
        try {
            return getService().deleteLinkPassphrase(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteLinkPassphrase(LinkPassphraseJsBean jsBean) throws WebException
    {
        try {
            LinkPassphrase linkPassphrase = convertLinkPassphraseJsBeanToBean(jsBean);
            return getService().deleteLinkPassphrase(linkPassphrase);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteLinkPassphrases(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteLinkPassphrases(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static LinkPassphraseJsBean convertLinkPassphraseToJsBean(LinkPassphrase linkPassphrase)
    {
        LinkPassphraseJsBean jsBean = null;
        if(linkPassphrase != null) {
            jsBean = new LinkPassphraseJsBean();
            jsBean.setGuid(linkPassphrase.getGuid());
            jsBean.setShortLink(linkPassphrase.getShortLink());
            jsBean.setPassphrase(linkPassphrase.getPassphrase());
            jsBean.setCreatedTime(linkPassphrase.getCreatedTime());
            jsBean.setModifiedTime(linkPassphrase.getModifiedTime());
        }
        return jsBean;
    }

    public static LinkPassphrase convertLinkPassphraseJsBeanToBean(LinkPassphraseJsBean jsBean)
    {
        LinkPassphraseBean linkPassphrase = null;
        if(jsBean != null) {
            linkPassphrase = new LinkPassphraseBean();
            linkPassphrase.setGuid(jsBean.getGuid());
            linkPassphrase.setShortLink(jsBean.getShortLink());
            linkPassphrase.setPassphrase(jsBean.getPassphrase());
            linkPassphrase.setCreatedTime(jsBean.getCreatedTime());
            linkPassphrase.setModifiedTime(jsBean.getModifiedTime());
        }
        return linkPassphrase;
    }

}
