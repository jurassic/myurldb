package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ReferrerInfoStruct;
import com.cannyurl.af.bean.ReferrerInfoStructBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ReferrerInfoStructJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ReferrerInfoStructWebService // implements ReferrerInfoStructService
{
    private static final Logger log = Logger.getLogger(ReferrerInfoStructWebService.class.getName());
     
    public static ReferrerInfoStructJsBean convertReferrerInfoStructToJsBean(ReferrerInfoStruct referrerInfoStruct)
    {
        ReferrerInfoStructJsBean jsBean = null;
        if(referrerInfoStruct != null) {
            jsBean = new ReferrerInfoStructJsBean();
            jsBean.setReferer(referrerInfoStruct.getReferer());
            jsBean.setUserAgent(referrerInfoStruct.getUserAgent());
            jsBean.setLanguage(referrerInfoStruct.getLanguage());
            jsBean.setHostname(referrerInfoStruct.getHostname());
            jsBean.setIpAddress(referrerInfoStruct.getIpAddress());
            jsBean.setNote(referrerInfoStruct.getNote());
        }
        return jsBean;
    }

    public static ReferrerInfoStruct convertReferrerInfoStructJsBeanToBean(ReferrerInfoStructJsBean jsBean)
    {
        ReferrerInfoStructBean referrerInfoStruct = null;
        if(jsBean != null) {
            referrerInfoStruct = new ReferrerInfoStructBean();
            referrerInfoStruct.setReferer(jsBean.getReferer());
            referrerInfoStruct.setUserAgent(jsBean.getUserAgent());
            referrerInfoStruct.setLanguage(jsBean.getLanguage());
            referrerInfoStruct.setHostname(jsBean.getHostname());
            referrerInfoStruct.setIpAddress(jsBean.getIpAddress());
            referrerInfoStruct.setNote(jsBean.getNote());
        }
        return referrerInfoStruct;
    }

}
