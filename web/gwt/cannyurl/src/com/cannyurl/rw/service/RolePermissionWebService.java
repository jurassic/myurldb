package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.RolePermission;
import com.cannyurl.af.bean.RolePermissionBean;
import com.cannyurl.af.service.RolePermissionService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.RolePermissionJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class RolePermissionWebService // implements RolePermissionService
{
    private static final Logger log = Logger.getLogger(RolePermissionWebService.class.getName());
     
    // Af service interface.
    private RolePermissionService mService = null;

    public RolePermissionWebService()
    {
        this(ServiceProxyFactory.getInstance().getRolePermissionServiceProxy());
    }
    public RolePermissionWebService(RolePermissionService service)
    {
        mService = service;
    }
    
    private RolePermissionService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getRolePermissionServiceProxy();
        }
        return mService;
    }
    
    
    public RolePermissionJsBean getRolePermission(String guid) throws WebException
    {
        try {
            RolePermission rolePermission = getService().getRolePermission(guid);
            RolePermissionJsBean bean = convertRolePermissionToJsBean(rolePermission);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getRolePermission(String guid, String field) throws WebException
    {
        try {
            return getService().getRolePermission(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<RolePermissionJsBean> getRolePermissions(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<RolePermissionJsBean> jsBeans = new ArrayList<RolePermissionJsBean>();
            List<RolePermission> rolePermissions = getService().getRolePermissions(guids);
            if(rolePermissions != null) {
                for(RolePermission rolePermission : rolePermissions) {
                    jsBeans.add(convertRolePermissionToJsBean(rolePermission));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<RolePermissionJsBean> getAllRolePermissions() throws WebException
    {
        return getAllRolePermissions(null, null, null);
    }

    public List<RolePermissionJsBean> getAllRolePermissions(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<RolePermissionJsBean> jsBeans = new ArrayList<RolePermissionJsBean>();
            List<RolePermission> rolePermissions = getService().getAllRolePermissions(ordering, offset, count);
            if(rolePermissions != null) {
                for(RolePermission rolePermission : rolePermissions) {
                    jsBeans.add(convertRolePermissionToJsBean(rolePermission));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllRolePermissionKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllRolePermissionKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<RolePermissionJsBean> findRolePermissions(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findRolePermissions(filter, ordering, params, values, null, null, null, null);
    }

    public List<RolePermissionJsBean> findRolePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<RolePermissionJsBean> jsBeans = new ArrayList<RolePermissionJsBean>();
            List<RolePermission> rolePermissions = getService().findRolePermissions(filter, ordering, params, values, grouping, unique, offset, count);
            if(rolePermissions != null) {
                for(RolePermission rolePermission : rolePermissions) {
                    jsBeans.add(convertRolePermissionToJsBean(rolePermission));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findRolePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findRolePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createRolePermission(String role, String permissionName, String resource, String instance, String action, String status) throws WebException
    {
        try {
            return getService().createRolePermission(role, permissionName, resource, instance, action, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createRolePermission(RolePermissionJsBean jsBean) throws WebException
    {
        try {
            RolePermission rolePermission = convertRolePermissionJsBeanToBean(jsBean);
            return getService().createRolePermission(rolePermission);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public RolePermissionJsBean constructRolePermission(RolePermissionJsBean jsBean) throws WebException
    {
        try {
            RolePermission rolePermission = convertRolePermissionJsBeanToBean(jsBean);
            rolePermission = getService().constructRolePermission(rolePermission);
            jsBean = convertRolePermissionToJsBean(rolePermission);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateRolePermission(String guid, String role, String permissionName, String resource, String instance, String action, String status) throws WebException
    {
        try {
            return getService().updateRolePermission(guid, role, permissionName, resource, instance, action, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateRolePermission(RolePermissionJsBean jsBean) throws WebException
    {
        try {
            RolePermission rolePermission = convertRolePermissionJsBeanToBean(jsBean);
            return getService().updateRolePermission(rolePermission);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public RolePermissionJsBean refreshRolePermission(RolePermissionJsBean jsBean) throws WebException
    {
        try {
            RolePermission rolePermission = convertRolePermissionJsBeanToBean(jsBean);
            rolePermission = getService().refreshRolePermission(rolePermission);
            jsBean = convertRolePermissionToJsBean(rolePermission);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteRolePermission(String guid) throws WebException
    {
        try {
            return getService().deleteRolePermission(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteRolePermission(RolePermissionJsBean jsBean) throws WebException
    {
        try {
            RolePermission rolePermission = convertRolePermissionJsBeanToBean(jsBean);
            return getService().deleteRolePermission(rolePermission);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteRolePermissions(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteRolePermissions(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static RolePermissionJsBean convertRolePermissionToJsBean(RolePermission rolePermission)
    {
        RolePermissionJsBean jsBean = null;
        if(rolePermission != null) {
            jsBean = new RolePermissionJsBean();
            jsBean.setGuid(rolePermission.getGuid());
            jsBean.setRole(rolePermission.getRole());
            jsBean.setPermissionName(rolePermission.getPermissionName());
            jsBean.setResource(rolePermission.getResource());
            jsBean.setInstance(rolePermission.getInstance());
            jsBean.setAction(rolePermission.getAction());
            jsBean.setStatus(rolePermission.getStatus());
            jsBean.setCreatedTime(rolePermission.getCreatedTime());
            jsBean.setModifiedTime(rolePermission.getModifiedTime());
        }
        return jsBean;
    }

    public static RolePermission convertRolePermissionJsBeanToBean(RolePermissionJsBean jsBean)
    {
        RolePermissionBean rolePermission = null;
        if(jsBean != null) {
            rolePermission = new RolePermissionBean();
            rolePermission.setGuid(jsBean.getGuid());
            rolePermission.setRole(jsBean.getRole());
            rolePermission.setPermissionName(jsBean.getPermissionName());
            rolePermission.setResource(jsBean.getResource());
            rolePermission.setInstance(jsBean.getInstance());
            rolePermission.setAction(jsBean.getAction());
            rolePermission.setStatus(jsBean.getStatus());
            rolePermission.setCreatedTime(jsBean.getCreatedTime());
            rolePermission.setModifiedTime(jsBean.getModifiedTime());
        }
        return rolePermission;
    }

}
