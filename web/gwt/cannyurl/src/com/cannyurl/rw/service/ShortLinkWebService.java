package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ReferrerInfoStruct;
import com.myurldb.ws.ShortLink;
import com.cannyurl.af.bean.ShortLinkBean;
import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ReferrerInfoStructJsBean;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortLinkWebService // implements ShortLinkService
{
    private static final Logger log = Logger.getLogger(ShortLinkWebService.class.getName());
     
    // Af service interface.
    private ShortLinkService mService = null;

    public ShortLinkWebService()
    {
        this(ServiceProxyFactory.getInstance().getShortLinkServiceProxy());
    }
    public ShortLinkWebService(ShortLinkService service)
    {
        mService = service;
    }
    
    private ShortLinkService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getShortLinkServiceProxy();
        }
        return mService;
    }
    
    
    public ShortLinkJsBean getShortLink(String guid) throws WebException
    {
        try {
            ShortLink shortLink = getService().getShortLink(guid);
            ShortLinkJsBean bean = convertShortLinkToJsBean(shortLink);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getShortLink(String guid, String field) throws WebException
    {
        try {
            return getService().getShortLink(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ShortLinkJsBean> getShortLinks(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ShortLinkJsBean> jsBeans = new ArrayList<ShortLinkJsBean>();
            List<ShortLink> shortLinks = getService().getShortLinks(guids);
            if(shortLinks != null) {
                for(ShortLink shortLink : shortLinks) {
                    jsBeans.add(convertShortLinkToJsBean(shortLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ShortLinkJsBean> getAllShortLinks() throws WebException
    {
        return getAllShortLinks(null, null, null);
    }

    public List<ShortLinkJsBean> getAllShortLinks(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<ShortLinkJsBean> jsBeans = new ArrayList<ShortLinkJsBean>();
            List<ShortLink> shortLinks = getService().getAllShortLinks(ordering, offset, count);
            if(shortLinks != null) {
                for(ShortLink shortLink : shortLinks) {
                    jsBeans.add(convertShortLinkToJsBean(shortLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllShortLinkKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllShortLinkKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ShortLinkJsBean> findShortLinks(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findShortLinks(filter, ordering, params, values, null, null, null, null);
    }

    public List<ShortLinkJsBean> findShortLinks(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<ShortLinkJsBean> jsBeans = new ArrayList<ShortLinkJsBean>();
            List<ShortLink> shortLinks = getService().findShortLinks(filter, ordering, params, values, grouping, unique, offset, count);
            if(shortLinks != null) {
                for(ShortLink shortLink : shortLinks) {
                    jsBeans.add(convertShortLinkToJsBean(shortLink));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findShortLinkKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findShortLinkKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createShortLink(String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructJsBean referrerInfo, String status, String note, Long expirationTime) throws WebException
    {
        try {
            return getService().createShortLink(appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, note, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createShortLink(ShortLinkJsBean jsBean) throws WebException
    {
        try {
            ShortLink shortLink = convertShortLinkJsBeanToBean(jsBean);
            return getService().createShortLink(shortLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ShortLinkJsBean constructShortLink(ShortLinkJsBean jsBean) throws WebException
    {
        try {
            ShortLink shortLink = convertShortLinkJsBeanToBean(jsBean);
            shortLink = getService().constructShortLink(shortLink);
            jsBean = convertShortLinkToJsBean(shortLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateShortLink(String guid, String appClient, String clientRootDomain, String owner, String longUrlDomain, String longUrl, String longUrlFull, String longUrlHash, Boolean reuseExistingShortUrl, Boolean failIfShortUrlExists, String domain, String domainType, String usercode, String username, String tokenPrefix, String token, String tokenType, String sassyTokenType, String shortUrl, String shortPassage, String redirectType, Long flashDuration, String accessType, String viewType, String shareType, Boolean readOnly, String displayMessage, String shortMessage, Boolean keywordEnabled, Boolean bookmarkEnabled, ReferrerInfoStructJsBean referrerInfo, String status, String note, Long expirationTime) throws WebException
    {
        try {
            return getService().updateShortLink(guid, appClient, clientRootDomain, owner, longUrlDomain, longUrl, longUrlFull, longUrlHash, reuseExistingShortUrl, failIfShortUrlExists, domain, domainType, usercode, username, tokenPrefix, token, tokenType, sassyTokenType, shortUrl, shortPassage, redirectType, flashDuration, accessType, viewType, shareType, readOnly, displayMessage, shortMessage, keywordEnabled, bookmarkEnabled, ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(referrerInfo), status, note, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateShortLink(ShortLinkJsBean jsBean) throws WebException
    {
        try {
            ShortLink shortLink = convertShortLinkJsBeanToBean(jsBean);
            return getService().updateShortLink(shortLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ShortLinkJsBean refreshShortLink(ShortLinkJsBean jsBean) throws WebException
    {
        try {
            ShortLink shortLink = convertShortLinkJsBeanToBean(jsBean);
            shortLink = getService().refreshShortLink(shortLink);
            jsBean = convertShortLinkToJsBean(shortLink);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteShortLink(String guid) throws WebException
    {
        try {
            return getService().deleteShortLink(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteShortLink(ShortLinkJsBean jsBean) throws WebException
    {
        try {
            ShortLink shortLink = convertShortLinkJsBeanToBean(jsBean);
            return getService().deleteShortLink(shortLink);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteShortLinks(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteShortLinks(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static ShortLinkJsBean convertShortLinkToJsBean(ShortLink shortLink)
    {
        ShortLinkJsBean jsBean = null;
        if(shortLink != null) {
            jsBean = new ShortLinkJsBean();
            jsBean.setGuid(shortLink.getGuid());
            jsBean.setAppClient(shortLink.getAppClient());
            jsBean.setClientRootDomain(shortLink.getClientRootDomain());
            jsBean.setOwner(shortLink.getOwner());
            jsBean.setLongUrlDomain(shortLink.getLongUrlDomain());
            jsBean.setLongUrl(shortLink.getLongUrl());
            jsBean.setLongUrlFull(shortLink.getLongUrlFull());
            jsBean.setLongUrlHash(shortLink.getLongUrlHash());
            jsBean.setReuseExistingShortUrl(shortLink.isReuseExistingShortUrl());
            jsBean.setFailIfShortUrlExists(shortLink.isFailIfShortUrlExists());
            jsBean.setDomain(shortLink.getDomain());
            jsBean.setDomainType(shortLink.getDomainType());
            jsBean.setUsercode(shortLink.getUsercode());
            jsBean.setUsername(shortLink.getUsername());
            jsBean.setTokenPrefix(shortLink.getTokenPrefix());
            jsBean.setToken(shortLink.getToken());
            jsBean.setTokenType(shortLink.getTokenType());
            jsBean.setSassyTokenType(shortLink.getSassyTokenType());
            jsBean.setShortUrl(shortLink.getShortUrl());
            jsBean.setShortPassage(shortLink.getShortPassage());
            jsBean.setRedirectType(shortLink.getRedirectType());
            jsBean.setFlashDuration(shortLink.getFlashDuration());
            jsBean.setAccessType(shortLink.getAccessType());
            jsBean.setViewType(shortLink.getViewType());
            jsBean.setShareType(shortLink.getShareType());
            jsBean.setReadOnly(shortLink.isReadOnly());
            jsBean.setDisplayMessage(shortLink.getDisplayMessage());
            jsBean.setShortMessage(shortLink.getShortMessage());
            jsBean.setKeywordEnabled(shortLink.isKeywordEnabled());
            jsBean.setBookmarkEnabled(shortLink.isBookmarkEnabled());
            jsBean.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructToJsBean(shortLink.getReferrerInfo()));
            jsBean.setStatus(shortLink.getStatus());
            jsBean.setNote(shortLink.getNote());
            jsBean.setExpirationTime(shortLink.getExpirationTime());
            jsBean.setCreatedTime(shortLink.getCreatedTime());
            jsBean.setModifiedTime(shortLink.getModifiedTime());
        }
        return jsBean;
    }

    public static ShortLink convertShortLinkJsBeanToBean(ShortLinkJsBean jsBean)
    {
        ShortLinkBean shortLink = null;
        if(jsBean != null) {
            shortLink = new ShortLinkBean();
            shortLink.setGuid(jsBean.getGuid());
            shortLink.setAppClient(jsBean.getAppClient());
            shortLink.setClientRootDomain(jsBean.getClientRootDomain());
            shortLink.setOwner(jsBean.getOwner());
            shortLink.setLongUrlDomain(jsBean.getLongUrlDomain());
            shortLink.setLongUrl(jsBean.getLongUrl());
            shortLink.setLongUrlFull(jsBean.getLongUrlFull());
            shortLink.setLongUrlHash(jsBean.getLongUrlHash());
            shortLink.setReuseExistingShortUrl(jsBean.isReuseExistingShortUrl());
            shortLink.setFailIfShortUrlExists(jsBean.isFailIfShortUrlExists());
            shortLink.setDomain(jsBean.getDomain());
            shortLink.setDomainType(jsBean.getDomainType());
            shortLink.setUsercode(jsBean.getUsercode());
            shortLink.setUsername(jsBean.getUsername());
            shortLink.setTokenPrefix(jsBean.getTokenPrefix());
            shortLink.setToken(jsBean.getToken());
            shortLink.setTokenType(jsBean.getTokenType());
            shortLink.setSassyTokenType(jsBean.getSassyTokenType());
            shortLink.setShortUrl(jsBean.getShortUrl());
            shortLink.setShortPassage(jsBean.getShortPassage());
            shortLink.setRedirectType(jsBean.getRedirectType());
            shortLink.setFlashDuration(jsBean.getFlashDuration());
            shortLink.setAccessType(jsBean.getAccessType());
            shortLink.setViewType(jsBean.getViewType());
            shortLink.setShareType(jsBean.getShareType());
            shortLink.setReadOnly(jsBean.isReadOnly());
            shortLink.setDisplayMessage(jsBean.getDisplayMessage());
            shortLink.setShortMessage(jsBean.getShortMessage());
            shortLink.setKeywordEnabled(jsBean.isKeywordEnabled());
            shortLink.setBookmarkEnabled(jsBean.isBookmarkEnabled());
            shortLink.setReferrerInfo(ReferrerInfoStructWebService.convertReferrerInfoStructJsBeanToBean(jsBean.getReferrerInfo()));
            shortLink.setStatus(jsBean.getStatus());
            shortLink.setNote(jsBean.getNote());
            shortLink.setExpirationTime(jsBean.getExpirationTime());
            shortLink.setCreatedTime(jsBean.getCreatedTime());
            shortLink.setModifiedTime(jsBean.getModifiedTime());
        }
        return shortLink;
    }

}
