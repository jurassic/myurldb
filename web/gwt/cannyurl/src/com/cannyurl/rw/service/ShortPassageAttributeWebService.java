package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortPassageAttribute;
import com.cannyurl.af.bean.ShortPassageAttributeBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ShortPassageAttributeJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortPassageAttributeWebService // implements ShortPassageAttributeService
{
    private static final Logger log = Logger.getLogger(ShortPassageAttributeWebService.class.getName());
     
    public static ShortPassageAttributeJsBean convertShortPassageAttributeToJsBean(ShortPassageAttribute shortPassageAttribute)
    {
        ShortPassageAttributeJsBean jsBean = null;
        if(shortPassageAttribute != null) {
            jsBean = new ShortPassageAttributeJsBean();
            jsBean.setDomain(shortPassageAttribute.getDomain());
            jsBean.setTokenType(shortPassageAttribute.getTokenType());
            jsBean.setDisplayMessage(shortPassageAttribute.getDisplayMessage());
            jsBean.setRedirectType(shortPassageAttribute.getRedirectType());
            jsBean.setFlashDuration(shortPassageAttribute.getFlashDuration());
            jsBean.setAccessType(shortPassageAttribute.getAccessType());
            jsBean.setViewType(shortPassageAttribute.getViewType());
            jsBean.setShareType(shortPassageAttribute.getShareType());
        }
        return jsBean;
    }

    public static ShortPassageAttribute convertShortPassageAttributeJsBeanToBean(ShortPassageAttributeJsBean jsBean)
    {
        ShortPassageAttributeBean shortPassageAttribute = null;
        if(jsBean != null) {
            shortPassageAttribute = new ShortPassageAttributeBean();
            shortPassageAttribute.setDomain(jsBean.getDomain());
            shortPassageAttribute.setTokenType(jsBean.getTokenType());
            shortPassageAttribute.setDisplayMessage(jsBean.getDisplayMessage());
            shortPassageAttribute.setRedirectType(jsBean.getRedirectType());
            shortPassageAttribute.setFlashDuration(jsBean.getFlashDuration());
            shortPassageAttribute.setAccessType(jsBean.getAccessType());
            shortPassageAttribute.setViewType(jsBean.getViewType());
            shortPassageAttribute.setShareType(jsBean.getShareType());
        }
        return shortPassageAttribute;
    }

}
