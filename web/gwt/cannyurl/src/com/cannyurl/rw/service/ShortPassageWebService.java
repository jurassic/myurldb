package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortPassageAttribute;
import com.myurldb.ws.ShortPassage;
import com.cannyurl.af.bean.ShortPassageBean;
import com.cannyurl.af.service.ShortPassageService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ShortPassageAttributeJsBean;
import com.cannyurl.fe.bean.ShortPassageJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortPassageWebService // implements ShortPassageService
{
    private static final Logger log = Logger.getLogger(ShortPassageWebService.class.getName());
     
    // Af service interface.
    private ShortPassageService mService = null;

    public ShortPassageWebService()
    {
        this(ServiceProxyFactory.getInstance().getShortPassageServiceProxy());
    }
    public ShortPassageWebService(ShortPassageService service)
    {
        mService = service;
    }
    
    private ShortPassageService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getShortPassageServiceProxy();
        }
        return mService;
    }
    
    
    public ShortPassageJsBean getShortPassage(String guid) throws WebException
    {
        try {
            ShortPassage shortPassage = getService().getShortPassage(guid);
            ShortPassageJsBean bean = convertShortPassageToJsBean(shortPassage);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getShortPassage(String guid, String field) throws WebException
    {
        try {
            return getService().getShortPassage(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ShortPassageJsBean> getShortPassages(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<ShortPassageJsBean> jsBeans = new ArrayList<ShortPassageJsBean>();
            List<ShortPassage> shortPassages = getService().getShortPassages(guids);
            if(shortPassages != null) {
                for(ShortPassage shortPassage : shortPassages) {
                    jsBeans.add(convertShortPassageToJsBean(shortPassage));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ShortPassageJsBean> getAllShortPassages() throws WebException
    {
        return getAllShortPassages(null, null, null);
    }

    public List<ShortPassageJsBean> getAllShortPassages(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<ShortPassageJsBean> jsBeans = new ArrayList<ShortPassageJsBean>();
            List<ShortPassage> shortPassages = getService().getAllShortPassages(ordering, offset, count);
            if(shortPassages != null) {
                for(ShortPassage shortPassage : shortPassages) {
                    jsBeans.add(convertShortPassageToJsBean(shortPassage));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllShortPassageKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllShortPassageKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<ShortPassageJsBean> findShortPassages(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findShortPassages(filter, ordering, params, values, null, null, null, null);
    }

    public List<ShortPassageJsBean> findShortPassages(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<ShortPassageJsBean> jsBeans = new ArrayList<ShortPassageJsBean>();
            List<ShortPassage> shortPassages = getService().findShortPassages(filter, ordering, params, values, grouping, unique, offset, count);
            if(shortPassages != null) {
                for(ShortPassage shortPassage : shortPassages) {
                    jsBeans.add(convertShortPassageToJsBean(shortPassage));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findShortPassageKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findShortPassageKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createShortPassage(String owner, String longText, String shortText, ShortPassageAttributeJsBean attribute, Boolean readOnly, String status, String note, Long expirationTime) throws WebException
    {
        try {
            return getService().createShortPassage(owner, longText, shortText, ShortPassageAttributeWebService.convertShortPassageAttributeJsBeanToBean(attribute), readOnly, status, note, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createShortPassage(ShortPassageJsBean jsBean) throws WebException
    {
        try {
            ShortPassage shortPassage = convertShortPassageJsBeanToBean(jsBean);
            return getService().createShortPassage(shortPassage);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ShortPassageJsBean constructShortPassage(ShortPassageJsBean jsBean) throws WebException
    {
        try {
            ShortPassage shortPassage = convertShortPassageJsBeanToBean(jsBean);
            shortPassage = getService().constructShortPassage(shortPassage);
            jsBean = convertShortPassageToJsBean(shortPassage);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateShortPassage(String guid, String owner, String longText, String shortText, ShortPassageAttributeJsBean attribute, Boolean readOnly, String status, String note, Long expirationTime) throws WebException
    {
        try {
            return getService().updateShortPassage(guid, owner, longText, shortText, ShortPassageAttributeWebService.convertShortPassageAttributeJsBeanToBean(attribute), readOnly, status, note, expirationTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateShortPassage(ShortPassageJsBean jsBean) throws WebException
    {
        try {
            ShortPassage shortPassage = convertShortPassageJsBeanToBean(jsBean);
            return getService().updateShortPassage(shortPassage);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public ShortPassageJsBean refreshShortPassage(ShortPassageJsBean jsBean) throws WebException
    {
        try {
            ShortPassage shortPassage = convertShortPassageJsBeanToBean(jsBean);
            shortPassage = getService().refreshShortPassage(shortPassage);
            jsBean = convertShortPassageToJsBean(shortPassage);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteShortPassage(String guid) throws WebException
    {
        try {
            return getService().deleteShortPassage(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteShortPassage(ShortPassageJsBean jsBean) throws WebException
    {
        try {
            ShortPassage shortPassage = convertShortPassageJsBeanToBean(jsBean);
            return getService().deleteShortPassage(shortPassage);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteShortPassages(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteShortPassages(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static ShortPassageJsBean convertShortPassageToJsBean(ShortPassage shortPassage)
    {
        ShortPassageJsBean jsBean = null;
        if(shortPassage != null) {
            jsBean = new ShortPassageJsBean();
            jsBean.setGuid(shortPassage.getGuid());
            jsBean.setOwner(shortPassage.getOwner());
            jsBean.setLongText(shortPassage.getLongText());
            jsBean.setShortText(shortPassage.getShortText());
            jsBean.setAttribute(ShortPassageAttributeWebService.convertShortPassageAttributeToJsBean(shortPassage.getAttribute()));
            jsBean.setReadOnly(shortPassage.isReadOnly());
            jsBean.setStatus(shortPassage.getStatus());
            jsBean.setNote(shortPassage.getNote());
            jsBean.setExpirationTime(shortPassage.getExpirationTime());
            jsBean.setCreatedTime(shortPassage.getCreatedTime());
            jsBean.setModifiedTime(shortPassage.getModifiedTime());
        }
        return jsBean;
    }

    public static ShortPassage convertShortPassageJsBeanToBean(ShortPassageJsBean jsBean)
    {
        ShortPassageBean shortPassage = null;
        if(jsBean != null) {
            shortPassage = new ShortPassageBean();
            shortPassage.setGuid(jsBean.getGuid());
            shortPassage.setOwner(jsBean.getOwner());
            shortPassage.setLongText(jsBean.getLongText());
            shortPassage.setShortText(jsBean.getShortText());
            shortPassage.setAttribute(ShortPassageAttributeWebService.convertShortPassageAttributeJsBeanToBean(jsBean.getAttribute()));
            shortPassage.setReadOnly(jsBean.isReadOnly());
            shortPassage.setStatus(jsBean.getStatus());
            shortPassage.setNote(jsBean.getNote());
            shortPassage.setExpirationTime(jsBean.getExpirationTime());
            shortPassage.setCreatedTime(jsBean.getCreatedTime());
            shortPassage.setModifiedTime(jsBean.getModifiedTime());
        }
        return shortPassage;
    }

}
