package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.SiteCustomDomain;
import com.cannyurl.af.bean.SiteCustomDomainBean;
import com.cannyurl.af.service.SiteCustomDomainService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.SiteCustomDomainJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class SiteCustomDomainWebService // implements SiteCustomDomainService
{
    private static final Logger log = Logger.getLogger(SiteCustomDomainWebService.class.getName());
     
    // Af service interface.
    private SiteCustomDomainService mService = null;

    public SiteCustomDomainWebService()
    {
        this(ServiceProxyFactory.getInstance().getSiteCustomDomainServiceProxy());
    }
    public SiteCustomDomainWebService(SiteCustomDomainService service)
    {
        mService = service;
    }
    
    private SiteCustomDomainService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getSiteCustomDomainServiceProxy();
        }
        return mService;
    }
    
    
    public SiteCustomDomainJsBean getSiteCustomDomain(String guid) throws WebException
    {
        try {
            SiteCustomDomain siteCustomDomain = getService().getSiteCustomDomain(guid);
            SiteCustomDomainJsBean bean = convertSiteCustomDomainToJsBean(siteCustomDomain);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getSiteCustomDomain(String guid, String field) throws WebException
    {
        try {
            return getService().getSiteCustomDomain(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SiteCustomDomainJsBean> getSiteCustomDomains(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<SiteCustomDomainJsBean> jsBeans = new ArrayList<SiteCustomDomainJsBean>();
            List<SiteCustomDomain> siteCustomDomains = getService().getSiteCustomDomains(guids);
            if(siteCustomDomains != null) {
                for(SiteCustomDomain siteCustomDomain : siteCustomDomains) {
                    jsBeans.add(convertSiteCustomDomainToJsBean(siteCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SiteCustomDomainJsBean> getAllSiteCustomDomains() throws WebException
    {
        return getAllSiteCustomDomains(null, null, null);
    }

    public List<SiteCustomDomainJsBean> getAllSiteCustomDomains(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<SiteCustomDomainJsBean> jsBeans = new ArrayList<SiteCustomDomainJsBean>();
            List<SiteCustomDomain> siteCustomDomains = getService().getAllSiteCustomDomains(ordering, offset, count);
            if(siteCustomDomains != null) {
                for(SiteCustomDomain siteCustomDomain : siteCustomDomains) {
                    jsBeans.add(convertSiteCustomDomainToJsBean(siteCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllSiteCustomDomainKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllSiteCustomDomainKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<SiteCustomDomainJsBean> findSiteCustomDomains(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findSiteCustomDomains(filter, ordering, params, values, null, null, null, null);
    }

    public List<SiteCustomDomainJsBean> findSiteCustomDomains(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<SiteCustomDomainJsBean> jsBeans = new ArrayList<SiteCustomDomainJsBean>();
            List<SiteCustomDomain> siteCustomDomains = getService().findSiteCustomDomains(filter, ordering, params, values, grouping, unique, offset, count);
            if(siteCustomDomains != null) {
                for(SiteCustomDomain siteCustomDomain : siteCustomDomains) {
                    jsBeans.add(convertSiteCustomDomainToJsBean(siteCustomDomain));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findSiteCustomDomainKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findSiteCustomDomainKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createSiteCustomDomain(String siteDomain, String domain, String status) throws WebException
    {
        try {
            return getService().createSiteCustomDomain(siteDomain, domain, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createSiteCustomDomain(SiteCustomDomainJsBean jsBean) throws WebException
    {
        try {
            SiteCustomDomain siteCustomDomain = convertSiteCustomDomainJsBeanToBean(jsBean);
            return getService().createSiteCustomDomain(siteCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public SiteCustomDomainJsBean constructSiteCustomDomain(SiteCustomDomainJsBean jsBean) throws WebException
    {
        try {
            SiteCustomDomain siteCustomDomain = convertSiteCustomDomainJsBeanToBean(jsBean);
            siteCustomDomain = getService().constructSiteCustomDomain(siteCustomDomain);
            jsBean = convertSiteCustomDomainToJsBean(siteCustomDomain);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateSiteCustomDomain(String guid, String siteDomain, String domain, String status) throws WebException
    {
        try {
            return getService().updateSiteCustomDomain(guid, siteDomain, domain, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateSiteCustomDomain(SiteCustomDomainJsBean jsBean) throws WebException
    {
        try {
            SiteCustomDomain siteCustomDomain = convertSiteCustomDomainJsBeanToBean(jsBean);
            return getService().updateSiteCustomDomain(siteCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public SiteCustomDomainJsBean refreshSiteCustomDomain(SiteCustomDomainJsBean jsBean) throws WebException
    {
        try {
            SiteCustomDomain siteCustomDomain = convertSiteCustomDomainJsBeanToBean(jsBean);
            siteCustomDomain = getService().refreshSiteCustomDomain(siteCustomDomain);
            jsBean = convertSiteCustomDomainToJsBean(siteCustomDomain);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteSiteCustomDomain(String guid) throws WebException
    {
        try {
            return getService().deleteSiteCustomDomain(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteSiteCustomDomain(SiteCustomDomainJsBean jsBean) throws WebException
    {
        try {
            SiteCustomDomain siteCustomDomain = convertSiteCustomDomainJsBeanToBean(jsBean);
            return getService().deleteSiteCustomDomain(siteCustomDomain);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteSiteCustomDomains(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteSiteCustomDomains(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static SiteCustomDomainJsBean convertSiteCustomDomainToJsBean(SiteCustomDomain siteCustomDomain)
    {
        SiteCustomDomainJsBean jsBean = null;
        if(siteCustomDomain != null) {
            jsBean = new SiteCustomDomainJsBean();
            jsBean.setGuid(siteCustomDomain.getGuid());
            jsBean.setSiteDomain(siteCustomDomain.getSiteDomain());
            jsBean.setDomain(siteCustomDomain.getDomain());
            jsBean.setStatus(siteCustomDomain.getStatus());
            jsBean.setCreatedTime(siteCustomDomain.getCreatedTime());
            jsBean.setModifiedTime(siteCustomDomain.getModifiedTime());
        }
        return jsBean;
    }

    public static SiteCustomDomain convertSiteCustomDomainJsBeanToBean(SiteCustomDomainJsBean jsBean)
    {
        SiteCustomDomainBean siteCustomDomain = null;
        if(jsBean != null) {
            siteCustomDomain = new SiteCustomDomainBean();
            siteCustomDomain.setGuid(jsBean.getGuid());
            siteCustomDomain.setSiteDomain(jsBean.getSiteDomain());
            siteCustomDomain.setDomain(jsBean.getDomain());
            siteCustomDomain.setStatus(jsBean.getStatus());
            siteCustomDomain.setCreatedTime(jsBean.getCreatedTime());
            siteCustomDomain.setModifiedTime(jsBean.getModifiedTime());
        }
        return siteCustomDomain;
    }

}
