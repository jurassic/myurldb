package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.StreetAddressStruct;
import com.cannyurl.af.bean.StreetAddressStructBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.StreetAddressStructJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class StreetAddressStructWebService // implements StreetAddressStructService
{
    private static final Logger log = Logger.getLogger(StreetAddressStructWebService.class.getName());
     
    public static StreetAddressStructJsBean convertStreetAddressStructToJsBean(StreetAddressStruct streetAddressStruct)
    {
        StreetAddressStructJsBean jsBean = null;
        if(streetAddressStruct != null) {
            jsBean = new StreetAddressStructJsBean();
            jsBean.setUuid(streetAddressStruct.getUuid());
            jsBean.setStreet1(streetAddressStruct.getStreet1());
            jsBean.setStreet2(streetAddressStruct.getStreet2());
            jsBean.setCity(streetAddressStruct.getCity());
            jsBean.setCounty(streetAddressStruct.getCounty());
            jsBean.setPostalCode(streetAddressStruct.getPostalCode());
            jsBean.setState(streetAddressStruct.getState());
            jsBean.setProvince(streetAddressStruct.getProvince());
            jsBean.setCountry(streetAddressStruct.getCountry());
            jsBean.setCountryName(streetAddressStruct.getCountryName());
            jsBean.setNote(streetAddressStruct.getNote());
        }
        return jsBean;
    }

    public static StreetAddressStruct convertStreetAddressStructJsBeanToBean(StreetAddressStructJsBean jsBean)
    {
        StreetAddressStructBean streetAddressStruct = null;
        if(jsBean != null) {
            streetAddressStruct = new StreetAddressStructBean();
            streetAddressStruct.setUuid(jsBean.getUuid());
            streetAddressStruct.setStreet1(jsBean.getStreet1());
            streetAddressStruct.setStreet2(jsBean.getStreet2());
            streetAddressStruct.setCity(jsBean.getCity());
            streetAddressStruct.setCounty(jsBean.getCounty());
            streetAddressStruct.setPostalCode(jsBean.getPostalCode());
            streetAddressStruct.setState(jsBean.getState());
            streetAddressStruct.setProvince(jsBean.getProvince());
            streetAddressStruct.setCountry(jsBean.getCountry());
            streetAddressStruct.setCountryName(jsBean.getCountryName());
            streetAddressStruct.setNote(jsBean.getNote());
        }
        return streetAddressStruct;
    }

}
