package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.cannyurl.af.bean.TwitterCardAppInfoBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterCardAppInfoWebService // implements TwitterCardAppInfoService
{
    private static final Logger log = Logger.getLogger(TwitterCardAppInfoWebService.class.getName());
     
    public static TwitterCardAppInfoJsBean convertTwitterCardAppInfoToJsBean(TwitterCardAppInfo twitterCardAppInfo)
    {
        TwitterCardAppInfoJsBean jsBean = null;
        if(twitterCardAppInfo != null) {
            jsBean = new TwitterCardAppInfoJsBean();
            jsBean.setName(twitterCardAppInfo.getName());
            jsBean.setId(twitterCardAppInfo.getId());
            jsBean.setUrl(twitterCardAppInfo.getUrl());
        }
        return jsBean;
    }

    public static TwitterCardAppInfo convertTwitterCardAppInfoJsBeanToBean(TwitterCardAppInfoJsBean jsBean)
    {
        TwitterCardAppInfoBean twitterCardAppInfo = null;
        if(jsBean != null) {
            twitterCardAppInfo = new TwitterCardAppInfoBean();
            twitterCardAppInfo.setName(jsBean.getName());
            twitterCardAppInfo.setId(jsBean.getId());
            twitterCardAppInfo.setUrl(jsBean.getUrl());
        }
        return twitterCardAppInfo;
    }

}
