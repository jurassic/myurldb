package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPhotoCard;
import com.cannyurl.af.bean.TwitterPhotoCardBean;
import com.cannyurl.af.service.TwitterPhotoCardService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterPhotoCardJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterPhotoCardWebService // implements TwitterPhotoCardService
{
    private static final Logger log = Logger.getLogger(TwitterPhotoCardWebService.class.getName());
     
    // Af service interface.
    private TwitterPhotoCardService mService = null;

    public TwitterPhotoCardWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterPhotoCardServiceProxy());
    }
    public TwitterPhotoCardWebService(TwitterPhotoCardService service)
    {
        mService = service;
    }
    
    private TwitterPhotoCardService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterPhotoCardServiceProxy();
        }
        return mService;
    }
    
    
    public TwitterPhotoCardJsBean getTwitterPhotoCard(String guid) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = getService().getTwitterPhotoCard(guid);
            TwitterPhotoCardJsBean bean = convertTwitterPhotoCardToJsBean(twitterPhotoCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterPhotoCard(String guid, String field) throws WebException
    {
        try {
            return getService().getTwitterPhotoCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPhotoCardJsBean> getTwitterPhotoCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterPhotoCardJsBean> jsBeans = new ArrayList<TwitterPhotoCardJsBean>();
            List<TwitterPhotoCard> twitterPhotoCards = getService().getTwitterPhotoCards(guids);
            if(twitterPhotoCards != null) {
                for(TwitterPhotoCard twitterPhotoCard : twitterPhotoCards) {
                    jsBeans.add(convertTwitterPhotoCardToJsBean(twitterPhotoCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPhotoCardJsBean> getAllTwitterPhotoCards() throws WebException
    {
        return getAllTwitterPhotoCards(null, null, null);
    }

    public List<TwitterPhotoCardJsBean> getAllTwitterPhotoCards(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<TwitterPhotoCardJsBean> jsBeans = new ArrayList<TwitterPhotoCardJsBean>();
            List<TwitterPhotoCard> twitterPhotoCards = getService().getAllTwitterPhotoCards(ordering, offset, count);
            if(twitterPhotoCards != null) {
                for(TwitterPhotoCard twitterPhotoCard : twitterPhotoCards) {
                    jsBeans.add(convertTwitterPhotoCardToJsBean(twitterPhotoCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllTwitterPhotoCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTwitterPhotoCardKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPhotoCardJsBean> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterPhotoCards(filter, ordering, params, values, null, null, null, null);
    }

    public List<TwitterPhotoCardJsBean> findTwitterPhotoCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<TwitterPhotoCardJsBean> jsBeans = new ArrayList<TwitterPhotoCardJsBean>();
            List<TwitterPhotoCard> twitterPhotoCards = getService().findTwitterPhotoCards(filter, ordering, params, values, grouping, unique, offset, count);
            if(twitterPhotoCards != null) {
                for(TwitterPhotoCard twitterPhotoCard : twitterPhotoCards) {
                    jsBeans.add(convertTwitterPhotoCardToJsBean(twitterPhotoCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findTwitterPhotoCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTwitterPhotoCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterPhotoCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getService().createTwitterPhotoCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            return getService().createTwitterPhotoCard(twitterPhotoCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterPhotoCardJsBean constructTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            twitterPhotoCard = getService().constructTwitterPhotoCard(twitterPhotoCard);
            jsBean = convertTwitterPhotoCardToJsBean(twitterPhotoCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterPhotoCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight) throws WebException
    {
        try {
            return getService().updateTwitterPhotoCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            return getService().updateTwitterPhotoCard(twitterPhotoCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterPhotoCardJsBean refreshTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            twitterPhotoCard = getService().refreshTwitterPhotoCard(twitterPhotoCard);
            jsBean = convertTwitterPhotoCardToJsBean(twitterPhotoCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterPhotoCard(String guid) throws WebException
    {
        try {
            return getService().deleteTwitterPhotoCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterPhotoCard(TwitterPhotoCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPhotoCard twitterPhotoCard = convertTwitterPhotoCardJsBeanToBean(jsBean);
            return getService().deleteTwitterPhotoCard(twitterPhotoCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterPhotoCards(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteTwitterPhotoCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterPhotoCardJsBean convertTwitterPhotoCardToJsBean(TwitterPhotoCard twitterPhotoCard)
    {
        TwitterPhotoCardJsBean jsBean = null;
        if(twitterPhotoCard != null) {
            jsBean = new TwitterPhotoCardJsBean();
            jsBean.setGuid(twitterPhotoCard.getGuid());
            jsBean.setCard(twitterPhotoCard.getCard());
            jsBean.setUrl(twitterPhotoCard.getUrl());
            jsBean.setTitle(twitterPhotoCard.getTitle());
            jsBean.setDescription(twitterPhotoCard.getDescription());
            jsBean.setSite(twitterPhotoCard.getSite());
            jsBean.setSiteId(twitterPhotoCard.getSiteId());
            jsBean.setCreator(twitterPhotoCard.getCreator());
            jsBean.setCreatorId(twitterPhotoCard.getCreatorId());
            jsBean.setImage(twitterPhotoCard.getImage());
            jsBean.setImageWidth(twitterPhotoCard.getImageWidth());
            jsBean.setImageHeight(twitterPhotoCard.getImageHeight());
            jsBean.setCreatedTime(twitterPhotoCard.getCreatedTime());
            jsBean.setModifiedTime(twitterPhotoCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterPhotoCard convertTwitterPhotoCardJsBeanToBean(TwitterPhotoCardJsBean jsBean)
    {
        TwitterPhotoCardBean twitterPhotoCard = null;
        if(jsBean != null) {
            twitterPhotoCard = new TwitterPhotoCardBean();
            twitterPhotoCard.setGuid(jsBean.getGuid());
            twitterPhotoCard.setCard(jsBean.getCard());
            twitterPhotoCard.setUrl(jsBean.getUrl());
            twitterPhotoCard.setTitle(jsBean.getTitle());
            twitterPhotoCard.setDescription(jsBean.getDescription());
            twitterPhotoCard.setSite(jsBean.getSite());
            twitterPhotoCard.setSiteId(jsBean.getSiteId());
            twitterPhotoCard.setCreator(jsBean.getCreator());
            twitterPhotoCard.setCreatorId(jsBean.getCreatorId());
            twitterPhotoCard.setImage(jsBean.getImage());
            twitterPhotoCard.setImageWidth(jsBean.getImageWidth());
            twitterPhotoCard.setImageHeight(jsBean.getImageHeight());
            twitterPhotoCard.setCreatedTime(jsBean.getCreatedTime());
            twitterPhotoCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterPhotoCard;
    }

}
