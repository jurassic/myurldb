package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterPlayerCard;
import com.cannyurl.af.bean.TwitterPlayerCardBean;
import com.cannyurl.af.service.TwitterPlayerCardService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterPlayerCardJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterPlayerCardWebService // implements TwitterPlayerCardService
{
    private static final Logger log = Logger.getLogger(TwitterPlayerCardWebService.class.getName());
     
    // Af service interface.
    private TwitterPlayerCardService mService = null;

    public TwitterPlayerCardWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterPlayerCardServiceProxy());
    }
    public TwitterPlayerCardWebService(TwitterPlayerCardService service)
    {
        mService = service;
    }
    
    private TwitterPlayerCardService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterPlayerCardServiceProxy();
        }
        return mService;
    }
    
    
    public TwitterPlayerCardJsBean getTwitterPlayerCard(String guid) throws WebException
    {
        try {
            TwitterPlayerCard twitterPlayerCard = getService().getTwitterPlayerCard(guid);
            TwitterPlayerCardJsBean bean = convertTwitterPlayerCardToJsBean(twitterPlayerCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterPlayerCard(String guid, String field) throws WebException
    {
        try {
            return getService().getTwitterPlayerCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPlayerCardJsBean> getTwitterPlayerCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterPlayerCardJsBean> jsBeans = new ArrayList<TwitterPlayerCardJsBean>();
            List<TwitterPlayerCard> twitterPlayerCards = getService().getTwitterPlayerCards(guids);
            if(twitterPlayerCards != null) {
                for(TwitterPlayerCard twitterPlayerCard : twitterPlayerCards) {
                    jsBeans.add(convertTwitterPlayerCardToJsBean(twitterPlayerCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPlayerCardJsBean> getAllTwitterPlayerCards() throws WebException
    {
        return getAllTwitterPlayerCards(null, null, null);
    }

    public List<TwitterPlayerCardJsBean> getAllTwitterPlayerCards(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<TwitterPlayerCardJsBean> jsBeans = new ArrayList<TwitterPlayerCardJsBean>();
            List<TwitterPlayerCard> twitterPlayerCards = getService().getAllTwitterPlayerCards(ordering, offset, count);
            if(twitterPlayerCards != null) {
                for(TwitterPlayerCard twitterPlayerCard : twitterPlayerCards) {
                    jsBeans.add(convertTwitterPlayerCardToJsBean(twitterPlayerCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllTwitterPlayerCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTwitterPlayerCardKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterPlayerCardJsBean> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterPlayerCards(filter, ordering, params, values, null, null, null, null);
    }

    public List<TwitterPlayerCardJsBean> findTwitterPlayerCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<TwitterPlayerCardJsBean> jsBeans = new ArrayList<TwitterPlayerCardJsBean>();
            List<TwitterPlayerCard> twitterPlayerCards = getService().findTwitterPlayerCards(filter, ordering, params, values, grouping, unique, offset, count);
            if(twitterPlayerCards != null) {
                for(TwitterPlayerCard twitterPlayerCard : twitterPlayerCards) {
                    jsBeans.add(convertTwitterPlayerCardToJsBean(twitterPlayerCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findTwitterPlayerCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTwitterPlayerCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterPlayerCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws WebException
    {
        try {
            return getService().createTwitterPlayerCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterPlayerCard(TwitterPlayerCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPlayerCard twitterPlayerCard = convertTwitterPlayerCardJsBeanToBean(jsBean);
            return getService().createTwitterPlayerCard(twitterPlayerCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterPlayerCardJsBean constructTwitterPlayerCard(TwitterPlayerCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPlayerCard twitterPlayerCard = convertTwitterPlayerCardJsBeanToBean(jsBean);
            twitterPlayerCard = getService().constructTwitterPlayerCard(twitterPlayerCard);
            jsBean = convertTwitterPlayerCardToJsBean(twitterPlayerCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterPlayerCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, String player, Integer playerWidth, Integer playerHeight, String playerStream, String playerStreamContentType) throws WebException
    {
        try {
            return getService().updateTwitterPlayerCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, player, playerWidth, playerHeight, playerStream, playerStreamContentType);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterPlayerCard(TwitterPlayerCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPlayerCard twitterPlayerCard = convertTwitterPlayerCardJsBeanToBean(jsBean);
            return getService().updateTwitterPlayerCard(twitterPlayerCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterPlayerCardJsBean refreshTwitterPlayerCard(TwitterPlayerCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPlayerCard twitterPlayerCard = convertTwitterPlayerCardJsBeanToBean(jsBean);
            twitterPlayerCard = getService().refreshTwitterPlayerCard(twitterPlayerCard);
            jsBean = convertTwitterPlayerCardToJsBean(twitterPlayerCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterPlayerCard(String guid) throws WebException
    {
        try {
            return getService().deleteTwitterPlayerCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterPlayerCard(TwitterPlayerCardJsBean jsBean) throws WebException
    {
        try {
            TwitterPlayerCard twitterPlayerCard = convertTwitterPlayerCardJsBeanToBean(jsBean);
            return getService().deleteTwitterPlayerCard(twitterPlayerCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterPlayerCards(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteTwitterPlayerCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterPlayerCardJsBean convertTwitterPlayerCardToJsBean(TwitterPlayerCard twitterPlayerCard)
    {
        TwitterPlayerCardJsBean jsBean = null;
        if(twitterPlayerCard != null) {
            jsBean = new TwitterPlayerCardJsBean();
            jsBean.setGuid(twitterPlayerCard.getGuid());
            jsBean.setCard(twitterPlayerCard.getCard());
            jsBean.setUrl(twitterPlayerCard.getUrl());
            jsBean.setTitle(twitterPlayerCard.getTitle());
            jsBean.setDescription(twitterPlayerCard.getDescription());
            jsBean.setSite(twitterPlayerCard.getSite());
            jsBean.setSiteId(twitterPlayerCard.getSiteId());
            jsBean.setCreator(twitterPlayerCard.getCreator());
            jsBean.setCreatorId(twitterPlayerCard.getCreatorId());
            jsBean.setImage(twitterPlayerCard.getImage());
            jsBean.setImageWidth(twitterPlayerCard.getImageWidth());
            jsBean.setImageHeight(twitterPlayerCard.getImageHeight());
            jsBean.setPlayer(twitterPlayerCard.getPlayer());
            jsBean.setPlayerWidth(twitterPlayerCard.getPlayerWidth());
            jsBean.setPlayerHeight(twitterPlayerCard.getPlayerHeight());
            jsBean.setPlayerStream(twitterPlayerCard.getPlayerStream());
            jsBean.setPlayerStreamContentType(twitterPlayerCard.getPlayerStreamContentType());
            jsBean.setCreatedTime(twitterPlayerCard.getCreatedTime());
            jsBean.setModifiedTime(twitterPlayerCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterPlayerCard convertTwitterPlayerCardJsBeanToBean(TwitterPlayerCardJsBean jsBean)
    {
        TwitterPlayerCardBean twitterPlayerCard = null;
        if(jsBean != null) {
            twitterPlayerCard = new TwitterPlayerCardBean();
            twitterPlayerCard.setGuid(jsBean.getGuid());
            twitterPlayerCard.setCard(jsBean.getCard());
            twitterPlayerCard.setUrl(jsBean.getUrl());
            twitterPlayerCard.setTitle(jsBean.getTitle());
            twitterPlayerCard.setDescription(jsBean.getDescription());
            twitterPlayerCard.setSite(jsBean.getSite());
            twitterPlayerCard.setSiteId(jsBean.getSiteId());
            twitterPlayerCard.setCreator(jsBean.getCreator());
            twitterPlayerCard.setCreatorId(jsBean.getCreatorId());
            twitterPlayerCard.setImage(jsBean.getImage());
            twitterPlayerCard.setImageWidth(jsBean.getImageWidth());
            twitterPlayerCard.setImageHeight(jsBean.getImageHeight());
            twitterPlayerCard.setPlayer(jsBean.getPlayer());
            twitterPlayerCard.setPlayerWidth(jsBean.getPlayerWidth());
            twitterPlayerCard.setPlayerHeight(jsBean.getPlayerHeight());
            twitterPlayerCard.setPlayerStream(jsBean.getPlayerStream());
            twitterPlayerCard.setPlayerStreamContentType(jsBean.getPlayerStreamContentType());
            twitterPlayerCard.setCreatedTime(jsBean.getCreatedTime());
            twitterPlayerCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterPlayerCard;
    }

}
