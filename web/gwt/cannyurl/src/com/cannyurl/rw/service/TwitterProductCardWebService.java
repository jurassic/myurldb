package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.TwitterCardAppInfo;
import com.myurldb.ws.TwitterCardProductData;
import com.myurldb.ws.TwitterProductCard;
import com.cannyurl.af.bean.TwitterProductCardBean;
import com.cannyurl.af.service.TwitterProductCardService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.TwitterCardAppInfoJsBean;
import com.cannyurl.fe.bean.TwitterCardProductDataJsBean;
import com.cannyurl.fe.bean.TwitterProductCardJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class TwitterProductCardWebService // implements TwitterProductCardService
{
    private static final Logger log = Logger.getLogger(TwitterProductCardWebService.class.getName());
     
    // Af service interface.
    private TwitterProductCardService mService = null;

    public TwitterProductCardWebService()
    {
        this(ServiceProxyFactory.getInstance().getTwitterProductCardServiceProxy());
    }
    public TwitterProductCardWebService(TwitterProductCardService service)
    {
        mService = service;
    }
    
    private TwitterProductCardService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getTwitterProductCardServiceProxy();
        }
        return mService;
    }
    
    
    public TwitterProductCardJsBean getTwitterProductCard(String guid) throws WebException
    {
        try {
            TwitterProductCard twitterProductCard = getService().getTwitterProductCard(guid);
            TwitterProductCardJsBean bean = convertTwitterProductCardToJsBean(twitterProductCard);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getTwitterProductCard(String guid, String field) throws WebException
    {
        try {
            return getService().getTwitterProductCard(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterProductCardJsBean> getTwitterProductCards(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<TwitterProductCardJsBean> jsBeans = new ArrayList<TwitterProductCardJsBean>();
            List<TwitterProductCard> twitterProductCards = getService().getTwitterProductCards(guids);
            if(twitterProductCards != null) {
                for(TwitterProductCard twitterProductCard : twitterProductCards) {
                    jsBeans.add(convertTwitterProductCardToJsBean(twitterProductCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterProductCardJsBean> getAllTwitterProductCards() throws WebException
    {
        return getAllTwitterProductCards(null, null, null);
    }

    public List<TwitterProductCardJsBean> getAllTwitterProductCards(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<TwitterProductCardJsBean> jsBeans = new ArrayList<TwitterProductCardJsBean>();
            List<TwitterProductCard> twitterProductCards = getService().getAllTwitterProductCards(ordering, offset, count);
            if(twitterProductCards != null) {
                for(TwitterProductCard twitterProductCard : twitterProductCards) {
                    jsBeans.add(convertTwitterProductCardToJsBean(twitterProductCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllTwitterProductCardKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllTwitterProductCardKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<TwitterProductCardJsBean> findTwitterProductCards(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findTwitterProductCards(filter, ordering, params, values, null, null, null, null);
    }

    public List<TwitterProductCardJsBean> findTwitterProductCards(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<TwitterProductCardJsBean> jsBeans = new ArrayList<TwitterProductCardJsBean>();
            List<TwitterProductCard> twitterProductCards = getService().findTwitterProductCards(filter, ordering, params, values, grouping, unique, offset, count);
            if(twitterProductCards != null) {
                for(TwitterProductCard twitterProductCard : twitterProductCards) {
                    jsBeans.add(convertTwitterProductCardToJsBean(twitterProductCard));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findTwitterProductCardKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findTwitterProductCardKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterProductCard(String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductDataJsBean data1, TwitterCardProductDataJsBean data2) throws WebException
    {
        try {
            return getService().createTwitterProductCard(card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, TwitterCardProductDataWebService.convertTwitterCardProductDataJsBeanToBean(data1), TwitterCardProductDataWebService.convertTwitterCardProductDataJsBeanToBean(data2));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createTwitterProductCard(TwitterProductCardJsBean jsBean) throws WebException
    {
        try {
            TwitterProductCard twitterProductCard = convertTwitterProductCardJsBeanToBean(jsBean);
            return getService().createTwitterProductCard(twitterProductCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterProductCardJsBean constructTwitterProductCard(TwitterProductCardJsBean jsBean) throws WebException
    {
        try {
            TwitterProductCard twitterProductCard = convertTwitterProductCardJsBeanToBean(jsBean);
            twitterProductCard = getService().constructTwitterProductCard(twitterProductCard);
            jsBean = convertTwitterProductCardToJsBean(twitterProductCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateTwitterProductCard(String guid, String card, String url, String title, String description, String site, String siteId, String creator, String creatorId, String image, Integer imageWidth, Integer imageHeight, TwitterCardProductDataJsBean data1, TwitterCardProductDataJsBean data2) throws WebException
    {
        try {
            return getService().updateTwitterProductCard(guid, card, url, title, description, site, siteId, creator, creatorId, image, imageWidth, imageHeight, TwitterCardProductDataWebService.convertTwitterCardProductDataJsBeanToBean(data1), TwitterCardProductDataWebService.convertTwitterCardProductDataJsBeanToBean(data2));
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateTwitterProductCard(TwitterProductCardJsBean jsBean) throws WebException
    {
        try {
            TwitterProductCard twitterProductCard = convertTwitterProductCardJsBeanToBean(jsBean);
            return getService().updateTwitterProductCard(twitterProductCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public TwitterProductCardJsBean refreshTwitterProductCard(TwitterProductCardJsBean jsBean) throws WebException
    {
        try {
            TwitterProductCard twitterProductCard = convertTwitterProductCardJsBeanToBean(jsBean);
            twitterProductCard = getService().refreshTwitterProductCard(twitterProductCard);
            jsBean = convertTwitterProductCardToJsBean(twitterProductCard);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterProductCard(String guid) throws WebException
    {
        try {
            return getService().deleteTwitterProductCard(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteTwitterProductCard(TwitterProductCardJsBean jsBean) throws WebException
    {
        try {
            TwitterProductCard twitterProductCard = convertTwitterProductCardJsBeanToBean(jsBean);
            return getService().deleteTwitterProductCard(twitterProductCard);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteTwitterProductCards(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteTwitterProductCards(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static TwitterProductCardJsBean convertTwitterProductCardToJsBean(TwitterProductCard twitterProductCard)
    {
        TwitterProductCardJsBean jsBean = null;
        if(twitterProductCard != null) {
            jsBean = new TwitterProductCardJsBean();
            jsBean.setGuid(twitterProductCard.getGuid());
            jsBean.setCard(twitterProductCard.getCard());
            jsBean.setUrl(twitterProductCard.getUrl());
            jsBean.setTitle(twitterProductCard.getTitle());
            jsBean.setDescription(twitterProductCard.getDescription());
            jsBean.setSite(twitterProductCard.getSite());
            jsBean.setSiteId(twitterProductCard.getSiteId());
            jsBean.setCreator(twitterProductCard.getCreator());
            jsBean.setCreatorId(twitterProductCard.getCreatorId());
            jsBean.setImage(twitterProductCard.getImage());
            jsBean.setImageWidth(twitterProductCard.getImageWidth());
            jsBean.setImageHeight(twitterProductCard.getImageHeight());
            jsBean.setData1(TwitterCardProductDataWebService.convertTwitterCardProductDataToJsBean(twitterProductCard.getData1()));
            jsBean.setData2(TwitterCardProductDataWebService.convertTwitterCardProductDataToJsBean(twitterProductCard.getData2()));
            jsBean.setCreatedTime(twitterProductCard.getCreatedTime());
            jsBean.setModifiedTime(twitterProductCard.getModifiedTime());
        }
        return jsBean;
    }

    public static TwitterProductCard convertTwitterProductCardJsBeanToBean(TwitterProductCardJsBean jsBean)
    {
        TwitterProductCardBean twitterProductCard = null;
        if(jsBean != null) {
            twitterProductCard = new TwitterProductCardBean();
            twitterProductCard.setGuid(jsBean.getGuid());
            twitterProductCard.setCard(jsBean.getCard());
            twitterProductCard.setUrl(jsBean.getUrl());
            twitterProductCard.setTitle(jsBean.getTitle());
            twitterProductCard.setDescription(jsBean.getDescription());
            twitterProductCard.setSite(jsBean.getSite());
            twitterProductCard.setSiteId(jsBean.getSiteId());
            twitterProductCard.setCreator(jsBean.getCreator());
            twitterProductCard.setCreatorId(jsBean.getCreatorId());
            twitterProductCard.setImage(jsBean.getImage());
            twitterProductCard.setImageWidth(jsBean.getImageWidth());
            twitterProductCard.setImageHeight(jsBean.getImageHeight());
            twitterProductCard.setData1(TwitterCardProductDataWebService.convertTwitterCardProductDataJsBeanToBean(jsBean.getData1()));
            twitterProductCard.setData2(TwitterCardProductDataWebService.convertTwitterCardProductDataJsBeanToBean(jsBean.getData2()));
            twitterProductCard.setCreatedTime(jsBean.getCreatedTime());
            twitterProductCard.setModifiedTime(jsBean.getModifiedTime());
        }
        return twitterProductCard;
    }

}
