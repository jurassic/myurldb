package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UrlRating;
import com.cannyurl.af.bean.UrlRatingBean;
import com.cannyurl.af.service.UrlRatingService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UrlRatingJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UrlRatingWebService // implements UrlRatingService
{
    private static final Logger log = Logger.getLogger(UrlRatingWebService.class.getName());
     
    // Af service interface.
    private UrlRatingService mService = null;

    public UrlRatingWebService()
    {
        this(ServiceProxyFactory.getInstance().getUrlRatingServiceProxy());
    }
    public UrlRatingWebService(UrlRatingService service)
    {
        mService = service;
    }
    
    private UrlRatingService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUrlRatingServiceProxy();
        }
        return mService;
    }
    
    
    public UrlRatingJsBean getUrlRating(String guid) throws WebException
    {
        try {
            UrlRating urlRating = getService().getUrlRating(guid);
            UrlRatingJsBean bean = convertUrlRatingToJsBean(urlRating);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUrlRating(String guid, String field) throws WebException
    {
        try {
            return getService().getUrlRating(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UrlRatingJsBean> getUrlRatings(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UrlRatingJsBean> jsBeans = new ArrayList<UrlRatingJsBean>();
            List<UrlRating> urlRatings = getService().getUrlRatings(guids);
            if(urlRatings != null) {
                for(UrlRating urlRating : urlRatings) {
                    jsBeans.add(convertUrlRatingToJsBean(urlRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UrlRatingJsBean> getAllUrlRatings() throws WebException
    {
        return getAllUrlRatings(null, null, null);
    }

    public List<UrlRatingJsBean> getAllUrlRatings(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<UrlRatingJsBean> jsBeans = new ArrayList<UrlRatingJsBean>();
            List<UrlRating> urlRatings = getService().getAllUrlRatings(ordering, offset, count);
            if(urlRatings != null) {
                for(UrlRating urlRating : urlRatings) {
                    jsBeans.add(convertUrlRatingToJsBean(urlRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUrlRatingKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUrlRatingKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UrlRatingJsBean> findUrlRatings(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUrlRatings(filter, ordering, params, values, null, null, null, null);
    }

    public List<UrlRatingJsBean> findUrlRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<UrlRatingJsBean> jsBeans = new ArrayList<UrlRatingJsBean>();
            List<UrlRating> urlRatings = getService().findUrlRatings(filter, ordering, params, values, grouping, unique, offset, count);
            if(urlRatings != null) {
                for(UrlRating urlRating : urlRatings) {
                    jsBeans.add(convertUrlRatingToJsBean(urlRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUrlRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUrlRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUrlRating(String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime) throws WebException
    {
        try {
            return getService().createUrlRating(domain, longUrl, longUrlHash, preview, flag, rating, note, ratedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            return getService().createUrlRating(urlRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UrlRatingJsBean constructUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            urlRating = getService().constructUrlRating(urlRating);
            jsBean = convertUrlRatingToJsBean(urlRating);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUrlRating(String guid, String domain, String longUrl, String longUrlHash, String preview, String flag, Double rating, String note, Long ratedTime) throws WebException
    {
        try {
            return getService().updateUrlRating(guid, domain, longUrl, longUrlHash, preview, flag, rating, note, ratedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            return getService().updateUrlRating(urlRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UrlRatingJsBean refreshUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            urlRating = getService().refreshUrlRating(urlRating);
            jsBean = convertUrlRatingToJsBean(urlRating);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUrlRating(String guid) throws WebException
    {
        try {
            return getService().deleteUrlRating(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUrlRating(UrlRatingJsBean jsBean) throws WebException
    {
        try {
            UrlRating urlRating = convertUrlRatingJsBeanToBean(jsBean);
            return getService().deleteUrlRating(urlRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUrlRatings(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteUrlRatings(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UrlRatingJsBean convertUrlRatingToJsBean(UrlRating urlRating)
    {
        UrlRatingJsBean jsBean = null;
        if(urlRating != null) {
            jsBean = new UrlRatingJsBean();
            jsBean.setGuid(urlRating.getGuid());
            jsBean.setDomain(urlRating.getDomain());
            jsBean.setLongUrl(urlRating.getLongUrl());
            jsBean.setLongUrlHash(urlRating.getLongUrlHash());
            jsBean.setPreview(urlRating.getPreview());
            jsBean.setFlag(urlRating.getFlag());
            jsBean.setRating(urlRating.getRating());
            jsBean.setNote(urlRating.getNote());
            jsBean.setRatedTime(urlRating.getRatedTime());
            jsBean.setCreatedTime(urlRating.getCreatedTime());
            jsBean.setModifiedTime(urlRating.getModifiedTime());
        }
        return jsBean;
    }

    public static UrlRating convertUrlRatingJsBeanToBean(UrlRatingJsBean jsBean)
    {
        UrlRatingBean urlRating = null;
        if(jsBean != null) {
            urlRating = new UrlRatingBean();
            urlRating.setGuid(jsBean.getGuid());
            urlRating.setDomain(jsBean.getDomain());
            urlRating.setLongUrl(jsBean.getLongUrl());
            urlRating.setLongUrlHash(jsBean.getLongUrlHash());
            urlRating.setPreview(jsBean.getPreview());
            urlRating.setFlag(jsBean.getFlag());
            urlRating.setRating(jsBean.getRating());
            urlRating.setNote(jsBean.getNote());
            urlRating.setRatedTime(jsBean.getRatedTime());
            urlRating.setCreatedTime(jsBean.getCreatedTime());
            urlRating.setModifiedTime(jsBean.getModifiedTime());
        }
        return urlRating;
    }

}
