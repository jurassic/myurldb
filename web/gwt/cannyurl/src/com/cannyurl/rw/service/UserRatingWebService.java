package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRating;
import com.cannyurl.af.bean.UserRatingBean;
import com.cannyurl.af.service.UserRatingService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserRatingJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserRatingWebService // implements UserRatingService
{
    private static final Logger log = Logger.getLogger(UserRatingWebService.class.getName());
     
    // Af service interface.
    private UserRatingService mService = null;

    public UserRatingWebService()
    {
        this(ServiceProxyFactory.getInstance().getUserRatingServiceProxy());
    }
    public UserRatingWebService(UserRatingService service)
    {
        mService = service;
    }
    
    private UserRatingService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUserRatingServiceProxy();
        }
        return mService;
    }
    
    
    public UserRatingJsBean getUserRating(String guid) throws WebException
    {
        try {
            UserRating userRating = getService().getUserRating(guid);
            UserRatingJsBean bean = convertUserRatingToJsBean(userRating);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserRating(String guid, String field) throws WebException
    {
        try {
            return getService().getUserRating(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserRatingJsBean> getUserRatings(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserRatingJsBean> jsBeans = new ArrayList<UserRatingJsBean>();
            List<UserRating> userRatings = getService().getUserRatings(guids);
            if(userRatings != null) {
                for(UserRating userRating : userRatings) {
                    jsBeans.add(convertUserRatingToJsBean(userRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserRatingJsBean> getAllUserRatings() throws WebException
    {
        return getAllUserRatings(null, null, null);
    }

    public List<UserRatingJsBean> getAllUserRatings(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<UserRatingJsBean> jsBeans = new ArrayList<UserRatingJsBean>();
            List<UserRating> userRatings = getService().getAllUserRatings(ordering, offset, count);
            if(userRatings != null) {
                for(UserRating userRating : userRatings) {
                    jsBeans.add(convertUserRatingToJsBean(userRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserRatingKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserRatingKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserRatingJsBean> findUserRatings(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserRatings(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserRatingJsBean> findUserRatings(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<UserRatingJsBean> jsBeans = new ArrayList<UserRatingJsBean>();
            List<UserRating> userRatings = getService().findUserRatings(filter, ordering, params, values, grouping, unique, offset, count);
            if(userRatings != null) {
                for(UserRating userRating : userRatings) {
                    jsBeans.add(convertUserRatingToJsBean(userRating));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserRatingKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserRatingKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserRating(String user, Double rating, String note, Long ratedTime) throws WebException
    {
        try {
            return getService().createUserRating(user, rating, note, ratedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserRating(UserRatingJsBean jsBean) throws WebException
    {
        try {
            UserRating userRating = convertUserRatingJsBeanToBean(jsBean);
            return getService().createUserRating(userRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserRatingJsBean constructUserRating(UserRatingJsBean jsBean) throws WebException
    {
        try {
            UserRating userRating = convertUserRatingJsBeanToBean(jsBean);
            userRating = getService().constructUserRating(userRating);
            jsBean = convertUserRatingToJsBean(userRating);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserRating(String guid, String user, Double rating, String note, Long ratedTime) throws WebException
    {
        try {
            return getService().updateUserRating(guid, user, rating, note, ratedTime);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserRating(UserRatingJsBean jsBean) throws WebException
    {
        try {
            UserRating userRating = convertUserRatingJsBeanToBean(jsBean);
            return getService().updateUserRating(userRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserRatingJsBean refreshUserRating(UserRatingJsBean jsBean) throws WebException
    {
        try {
            UserRating userRating = convertUserRatingJsBeanToBean(jsBean);
            userRating = getService().refreshUserRating(userRating);
            jsBean = convertUserRatingToJsBean(userRating);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserRating(String guid) throws WebException
    {
        try {
            return getService().deleteUserRating(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserRating(UserRatingJsBean jsBean) throws WebException
    {
        try {
            UserRating userRating = convertUserRatingJsBeanToBean(jsBean);
            return getService().deleteUserRating(userRating);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserRatings(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteUserRatings(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UserRatingJsBean convertUserRatingToJsBean(UserRating userRating)
    {
        UserRatingJsBean jsBean = null;
        if(userRating != null) {
            jsBean = new UserRatingJsBean();
            jsBean.setGuid(userRating.getGuid());
            jsBean.setUser(userRating.getUser());
            jsBean.setRating(userRating.getRating());
            jsBean.setNote(userRating.getNote());
            jsBean.setRatedTime(userRating.getRatedTime());
            jsBean.setCreatedTime(userRating.getCreatedTime());
            jsBean.setModifiedTime(userRating.getModifiedTime());
        }
        return jsBean;
    }

    public static UserRating convertUserRatingJsBeanToBean(UserRatingJsBean jsBean)
    {
        UserRatingBean userRating = null;
        if(jsBean != null) {
            userRating = new UserRatingBean();
            userRating.setGuid(jsBean.getGuid());
            userRating.setUser(jsBean.getUser());
            userRating.setRating(jsBean.getRating());
            userRating.setNote(jsBean.getNote());
            userRating.setRatedTime(jsBean.getRatedTime());
            userRating.setCreatedTime(jsBean.getCreatedTime());
            userRating.setModifiedTime(jsBean.getModifiedTime());
        }
        return userRating;
    }

}
