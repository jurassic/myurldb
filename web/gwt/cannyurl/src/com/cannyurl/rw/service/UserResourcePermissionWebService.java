package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserResourcePermission;
import com.cannyurl.af.bean.UserResourcePermissionBean;
import com.cannyurl.af.service.UserResourcePermissionService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserResourcePermissionJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserResourcePermissionWebService // implements UserResourcePermissionService
{
    private static final Logger log = Logger.getLogger(UserResourcePermissionWebService.class.getName());
     
    // Af service interface.
    private UserResourcePermissionService mService = null;

    public UserResourcePermissionWebService()
    {
        this(ServiceProxyFactory.getInstance().getUserResourcePermissionServiceProxy());
    }
    public UserResourcePermissionWebService(UserResourcePermissionService service)
    {
        mService = service;
    }
    
    private UserResourcePermissionService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUserResourcePermissionServiceProxy();
        }
        return mService;
    }
    
    
    public UserResourcePermissionJsBean getUserResourcePermission(String guid) throws WebException
    {
        try {
            UserResourcePermission userResourcePermission = getService().getUserResourcePermission(guid);
            UserResourcePermissionJsBean bean = convertUserResourcePermissionToJsBean(userResourcePermission);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserResourcePermission(String guid, String field) throws WebException
    {
        try {
            return getService().getUserResourcePermission(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserResourcePermissionJsBean> getUserResourcePermissions(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserResourcePermissionJsBean> jsBeans = new ArrayList<UserResourcePermissionJsBean>();
            List<UserResourcePermission> userResourcePermissions = getService().getUserResourcePermissions(guids);
            if(userResourcePermissions != null) {
                for(UserResourcePermission userResourcePermission : userResourcePermissions) {
                    jsBeans.add(convertUserResourcePermissionToJsBean(userResourcePermission));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserResourcePermissionJsBean> getAllUserResourcePermissions() throws WebException
    {
        return getAllUserResourcePermissions(null, null, null);
    }

    public List<UserResourcePermissionJsBean> getAllUserResourcePermissions(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<UserResourcePermissionJsBean> jsBeans = new ArrayList<UserResourcePermissionJsBean>();
            List<UserResourcePermission> userResourcePermissions = getService().getAllUserResourcePermissions(ordering, offset, count);
            if(userResourcePermissions != null) {
                for(UserResourcePermission userResourcePermission : userResourcePermissions) {
                    jsBeans.add(convertUserResourcePermissionToJsBean(userResourcePermission));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserResourcePermissionKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserResourcePermissionKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserResourcePermissionJsBean> findUserResourcePermissions(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserResourcePermissions(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserResourcePermissionJsBean> findUserResourcePermissions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<UserResourcePermissionJsBean> jsBeans = new ArrayList<UserResourcePermissionJsBean>();
            List<UserResourcePermission> userResourcePermissions = getService().findUserResourcePermissions(filter, ordering, params, values, grouping, unique, offset, count);
            if(userResourcePermissions != null) {
                for(UserResourcePermission userResourcePermission : userResourcePermissions) {
                    jsBeans.add(convertUserResourcePermissionToJsBean(userResourcePermission));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserResourcePermissionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserResourcePermissionKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserResourcePermission(String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws WebException
    {
        try {
            return getService().createUserResourcePermission(user, permissionName, resource, instance, action, permitted, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserResourcePermission(UserResourcePermissionJsBean jsBean) throws WebException
    {
        try {
            UserResourcePermission userResourcePermission = convertUserResourcePermissionJsBeanToBean(jsBean);
            return getService().createUserResourcePermission(userResourcePermission);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserResourcePermissionJsBean constructUserResourcePermission(UserResourcePermissionJsBean jsBean) throws WebException
    {
        try {
            UserResourcePermission userResourcePermission = convertUserResourcePermissionJsBeanToBean(jsBean);
            userResourcePermission = getService().constructUserResourcePermission(userResourcePermission);
            jsBean = convertUserResourcePermissionToJsBean(userResourcePermission);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserResourcePermission(String guid, String user, String permissionName, String resource, String instance, String action, Boolean permitted, String status) throws WebException
    {
        try {
            return getService().updateUserResourcePermission(guid, user, permissionName, resource, instance, action, permitted, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserResourcePermission(UserResourcePermissionJsBean jsBean) throws WebException
    {
        try {
            UserResourcePermission userResourcePermission = convertUserResourcePermissionJsBeanToBean(jsBean);
            return getService().updateUserResourcePermission(userResourcePermission);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserResourcePermissionJsBean refreshUserResourcePermission(UserResourcePermissionJsBean jsBean) throws WebException
    {
        try {
            UserResourcePermission userResourcePermission = convertUserResourcePermissionJsBeanToBean(jsBean);
            userResourcePermission = getService().refreshUserResourcePermission(userResourcePermission);
            jsBean = convertUserResourcePermissionToJsBean(userResourcePermission);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserResourcePermission(String guid) throws WebException
    {
        try {
            return getService().deleteUserResourcePermission(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserResourcePermission(UserResourcePermissionJsBean jsBean) throws WebException
    {
        try {
            UserResourcePermission userResourcePermission = convertUserResourcePermissionJsBeanToBean(jsBean);
            return getService().deleteUserResourcePermission(userResourcePermission);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserResourcePermissions(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteUserResourcePermissions(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UserResourcePermissionJsBean convertUserResourcePermissionToJsBean(UserResourcePermission userResourcePermission)
    {
        UserResourcePermissionJsBean jsBean = null;
        if(userResourcePermission != null) {
            jsBean = new UserResourcePermissionJsBean();
            jsBean.setGuid(userResourcePermission.getGuid());
            jsBean.setUser(userResourcePermission.getUser());
            jsBean.setPermissionName(userResourcePermission.getPermissionName());
            jsBean.setResource(userResourcePermission.getResource());
            jsBean.setInstance(userResourcePermission.getInstance());
            jsBean.setAction(userResourcePermission.getAction());
            jsBean.setPermitted(userResourcePermission.isPermitted());
            jsBean.setStatus(userResourcePermission.getStatus());
            jsBean.setCreatedTime(userResourcePermission.getCreatedTime());
            jsBean.setModifiedTime(userResourcePermission.getModifiedTime());
        }
        return jsBean;
    }

    public static UserResourcePermission convertUserResourcePermissionJsBeanToBean(UserResourcePermissionJsBean jsBean)
    {
        UserResourcePermissionBean userResourcePermission = null;
        if(jsBean != null) {
            userResourcePermission = new UserResourcePermissionBean();
            userResourcePermission.setGuid(jsBean.getGuid());
            userResourcePermission.setUser(jsBean.getUser());
            userResourcePermission.setPermissionName(jsBean.getPermissionName());
            userResourcePermission.setResource(jsBean.getResource());
            userResourcePermission.setInstance(jsBean.getInstance());
            userResourcePermission.setAction(jsBean.getAction());
            userResourcePermission.setPermitted(jsBean.isPermitted());
            userResourcePermission.setStatus(jsBean.getStatus());
            userResourcePermission.setCreatedTime(jsBean.getCreatedTime());
            userResourcePermission.setModifiedTime(jsBean.getModifiedTime());
        }
        return userResourcePermission;
    }

}
