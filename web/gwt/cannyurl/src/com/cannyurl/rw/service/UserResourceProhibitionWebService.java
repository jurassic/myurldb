package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserResourceProhibition;
import com.cannyurl.af.bean.UserResourceProhibitionBean;
import com.cannyurl.af.service.UserResourceProhibitionService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserResourceProhibitionJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserResourceProhibitionWebService // implements UserResourceProhibitionService
{
    private static final Logger log = Logger.getLogger(UserResourceProhibitionWebService.class.getName());
     
    // Af service interface.
    private UserResourceProhibitionService mService = null;

    public UserResourceProhibitionWebService()
    {
        this(ServiceProxyFactory.getInstance().getUserResourceProhibitionServiceProxy());
    }
    public UserResourceProhibitionWebService(UserResourceProhibitionService service)
    {
        mService = service;
    }
    
    private UserResourceProhibitionService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUserResourceProhibitionServiceProxy();
        }
        return mService;
    }
    
    
    public UserResourceProhibitionJsBean getUserResourceProhibition(String guid) throws WebException
    {
        try {
            UserResourceProhibition userResourceProhibition = getService().getUserResourceProhibition(guid);
            UserResourceProhibitionJsBean bean = convertUserResourceProhibitionToJsBean(userResourceProhibition);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserResourceProhibition(String guid, String field) throws WebException
    {
        try {
            return getService().getUserResourceProhibition(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserResourceProhibitionJsBean> getUserResourceProhibitions(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserResourceProhibitionJsBean> jsBeans = new ArrayList<UserResourceProhibitionJsBean>();
            List<UserResourceProhibition> userResourceProhibitions = getService().getUserResourceProhibitions(guids);
            if(userResourceProhibitions != null) {
                for(UserResourceProhibition userResourceProhibition : userResourceProhibitions) {
                    jsBeans.add(convertUserResourceProhibitionToJsBean(userResourceProhibition));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserResourceProhibitionJsBean> getAllUserResourceProhibitions() throws WebException
    {
        return getAllUserResourceProhibitions(null, null, null);
    }

    public List<UserResourceProhibitionJsBean> getAllUserResourceProhibitions(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<UserResourceProhibitionJsBean> jsBeans = new ArrayList<UserResourceProhibitionJsBean>();
            List<UserResourceProhibition> userResourceProhibitions = getService().getAllUserResourceProhibitions(ordering, offset, count);
            if(userResourceProhibitions != null) {
                for(UserResourceProhibition userResourceProhibition : userResourceProhibitions) {
                    jsBeans.add(convertUserResourceProhibitionToJsBean(userResourceProhibition));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserResourceProhibitionKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserResourceProhibitionKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserResourceProhibitionJsBean> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserResourceProhibitions(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserResourceProhibitionJsBean> findUserResourceProhibitions(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<UserResourceProhibitionJsBean> jsBeans = new ArrayList<UserResourceProhibitionJsBean>();
            List<UserResourceProhibition> userResourceProhibitions = getService().findUserResourceProhibitions(filter, ordering, params, values, grouping, unique, offset, count);
            if(userResourceProhibitions != null) {
                for(UserResourceProhibition userResourceProhibition : userResourceProhibitions) {
                    jsBeans.add(convertUserResourceProhibitionToJsBean(userResourceProhibition));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserResourceProhibitionKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserResourceProhibitionKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserResourceProhibition(String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws WebException
    {
        try {
            return getService().createUserResourceProhibition(user, permissionName, resource, instance, action, prohibited, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserResourceProhibition(UserResourceProhibitionJsBean jsBean) throws WebException
    {
        try {
            UserResourceProhibition userResourceProhibition = convertUserResourceProhibitionJsBeanToBean(jsBean);
            return getService().createUserResourceProhibition(userResourceProhibition);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserResourceProhibitionJsBean constructUserResourceProhibition(UserResourceProhibitionJsBean jsBean) throws WebException
    {
        try {
            UserResourceProhibition userResourceProhibition = convertUserResourceProhibitionJsBeanToBean(jsBean);
            userResourceProhibition = getService().constructUserResourceProhibition(userResourceProhibition);
            jsBean = convertUserResourceProhibitionToJsBean(userResourceProhibition);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserResourceProhibition(String guid, String user, String permissionName, String resource, String instance, String action, Boolean prohibited, String status) throws WebException
    {
        try {
            return getService().updateUserResourceProhibition(guid, user, permissionName, resource, instance, action, prohibited, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserResourceProhibition(UserResourceProhibitionJsBean jsBean) throws WebException
    {
        try {
            UserResourceProhibition userResourceProhibition = convertUserResourceProhibitionJsBeanToBean(jsBean);
            return getService().updateUserResourceProhibition(userResourceProhibition);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserResourceProhibitionJsBean refreshUserResourceProhibition(UserResourceProhibitionJsBean jsBean) throws WebException
    {
        try {
            UserResourceProhibition userResourceProhibition = convertUserResourceProhibitionJsBeanToBean(jsBean);
            userResourceProhibition = getService().refreshUserResourceProhibition(userResourceProhibition);
            jsBean = convertUserResourceProhibitionToJsBean(userResourceProhibition);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserResourceProhibition(String guid) throws WebException
    {
        try {
            return getService().deleteUserResourceProhibition(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserResourceProhibition(UserResourceProhibitionJsBean jsBean) throws WebException
    {
        try {
            UserResourceProhibition userResourceProhibition = convertUserResourceProhibitionJsBeanToBean(jsBean);
            return getService().deleteUserResourceProhibition(userResourceProhibition);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserResourceProhibitions(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteUserResourceProhibitions(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UserResourceProhibitionJsBean convertUserResourceProhibitionToJsBean(UserResourceProhibition userResourceProhibition)
    {
        UserResourceProhibitionJsBean jsBean = null;
        if(userResourceProhibition != null) {
            jsBean = new UserResourceProhibitionJsBean();
            jsBean.setGuid(userResourceProhibition.getGuid());
            jsBean.setUser(userResourceProhibition.getUser());
            jsBean.setPermissionName(userResourceProhibition.getPermissionName());
            jsBean.setResource(userResourceProhibition.getResource());
            jsBean.setInstance(userResourceProhibition.getInstance());
            jsBean.setAction(userResourceProhibition.getAction());
            jsBean.setProhibited(userResourceProhibition.isProhibited());
            jsBean.setStatus(userResourceProhibition.getStatus());
            jsBean.setCreatedTime(userResourceProhibition.getCreatedTime());
            jsBean.setModifiedTime(userResourceProhibition.getModifiedTime());
        }
        return jsBean;
    }

    public static UserResourceProhibition convertUserResourceProhibitionJsBeanToBean(UserResourceProhibitionJsBean jsBean)
    {
        UserResourceProhibitionBean userResourceProhibition = null;
        if(jsBean != null) {
            userResourceProhibition = new UserResourceProhibitionBean();
            userResourceProhibition.setGuid(jsBean.getGuid());
            userResourceProhibition.setUser(jsBean.getUser());
            userResourceProhibition.setPermissionName(jsBean.getPermissionName());
            userResourceProhibition.setResource(jsBean.getResource());
            userResourceProhibition.setInstance(jsBean.getInstance());
            userResourceProhibition.setAction(jsBean.getAction());
            userResourceProhibition.setProhibited(jsBean.isProhibited());
            userResourceProhibition.setStatus(jsBean.getStatus());
            userResourceProhibition.setCreatedTime(jsBean.getCreatedTime());
            userResourceProhibition.setModifiedTime(jsBean.getModifiedTime());
        }
        return userResourceProhibition;
    }

}
