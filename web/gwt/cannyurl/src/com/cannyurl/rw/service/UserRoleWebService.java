package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserRole;
import com.cannyurl.af.bean.UserRoleBean;
import com.cannyurl.af.service.UserRoleService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserRoleJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserRoleWebService // implements UserRoleService
{
    private static final Logger log = Logger.getLogger(UserRoleWebService.class.getName());
     
    // Af service interface.
    private UserRoleService mService = null;

    public UserRoleWebService()
    {
        this(ServiceProxyFactory.getInstance().getUserRoleServiceProxy());
    }
    public UserRoleWebService(UserRoleService service)
    {
        mService = service;
    }
    
    private UserRoleService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUserRoleServiceProxy();
        }
        return mService;
    }
    
    
    public UserRoleJsBean getUserRole(String guid) throws WebException
    {
        try {
            UserRole userRole = getService().getUserRole(guid);
            UserRoleJsBean bean = convertUserRoleToJsBean(userRole);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserRole(String guid, String field) throws WebException
    {
        try {
            return getService().getUserRole(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserRoleJsBean> getUserRoles(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserRoleJsBean> jsBeans = new ArrayList<UserRoleJsBean>();
            List<UserRole> userRoles = getService().getUserRoles(guids);
            if(userRoles != null) {
                for(UserRole userRole : userRoles) {
                    jsBeans.add(convertUserRoleToJsBean(userRole));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserRoleJsBean> getAllUserRoles() throws WebException
    {
        return getAllUserRoles(null, null, null);
    }

    public List<UserRoleJsBean> getAllUserRoles(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<UserRoleJsBean> jsBeans = new ArrayList<UserRoleJsBean>();
            List<UserRole> userRoles = getService().getAllUserRoles(ordering, offset, count);
            if(userRoles != null) {
                for(UserRole userRole : userRoles) {
                    jsBeans.add(convertUserRoleToJsBean(userRole));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserRoleKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserRoleKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserRoleJsBean> findUserRoles(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserRoles(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserRoleJsBean> findUserRoles(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<UserRoleJsBean> jsBeans = new ArrayList<UserRoleJsBean>();
            List<UserRole> userRoles = getService().findUserRoles(filter, ordering, params, values, grouping, unique, offset, count);
            if(userRoles != null) {
                for(UserRole userRole : userRoles) {
                    jsBeans.add(convertUserRoleToJsBean(userRole));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserRoleKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserRoleKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserRole(String user, String role, String status) throws WebException
    {
        try {
            return getService().createUserRole(user, role, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserRole(UserRoleJsBean jsBean) throws WebException
    {
        try {
            UserRole userRole = convertUserRoleJsBeanToBean(jsBean);
            return getService().createUserRole(userRole);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserRoleJsBean constructUserRole(UserRoleJsBean jsBean) throws WebException
    {
        try {
            UserRole userRole = convertUserRoleJsBeanToBean(jsBean);
            userRole = getService().constructUserRole(userRole);
            jsBean = convertUserRoleToJsBean(userRole);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserRole(String guid, String user, String role, String status) throws WebException
    {
        try {
            return getService().updateUserRole(guid, user, role, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserRole(UserRoleJsBean jsBean) throws WebException
    {
        try {
            UserRole userRole = convertUserRoleJsBeanToBean(jsBean);
            return getService().updateUserRole(userRole);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserRoleJsBean refreshUserRole(UserRoleJsBean jsBean) throws WebException
    {
        try {
            UserRole userRole = convertUserRoleJsBeanToBean(jsBean);
            userRole = getService().refreshUserRole(userRole);
            jsBean = convertUserRoleToJsBean(userRole);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserRole(String guid) throws WebException
    {
        try {
            return getService().deleteUserRole(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserRole(UserRoleJsBean jsBean) throws WebException
    {
        try {
            UserRole userRole = convertUserRoleJsBeanToBean(jsBean);
            return getService().deleteUserRole(userRole);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserRoles(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteUserRoles(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UserRoleJsBean convertUserRoleToJsBean(UserRole userRole)
    {
        UserRoleJsBean jsBean = null;
        if(userRole != null) {
            jsBean = new UserRoleJsBean();
            jsBean.setGuid(userRole.getGuid());
            jsBean.setUser(userRole.getUser());
            jsBean.setRole(userRole.getRole());
            jsBean.setStatus(userRole.getStatus());
            jsBean.setCreatedTime(userRole.getCreatedTime());
            jsBean.setModifiedTime(userRole.getModifiedTime());
        }
        return jsBean;
    }

    public static UserRole convertUserRoleJsBeanToBean(UserRoleJsBean jsBean)
    {
        UserRoleBean userRole = null;
        if(jsBean != null) {
            userRole = new UserRoleBean();
            userRole.setGuid(jsBean.getGuid());
            userRole.setUser(jsBean.getUser());
            userRole.setRole(jsBean.getRole());
            userRole.setStatus(jsBean.getStatus());
            userRole.setCreatedTime(jsBean.getCreatedTime());
            userRole.setModifiedTime(jsBean.getModifiedTime());
        }
        return userRole;
    }

}
