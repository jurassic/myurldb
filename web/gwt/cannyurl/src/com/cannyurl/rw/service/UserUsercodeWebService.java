package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.UserUsercode;
import com.cannyurl.af.bean.UserUsercodeBean;
import com.cannyurl.af.service.UserUsercodeService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.UserUsercodeJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class UserUsercodeWebService // implements UserUsercodeService
{
    private static final Logger log = Logger.getLogger(UserUsercodeWebService.class.getName());
     
    // Af service interface.
    private UserUsercodeService mService = null;

    public UserUsercodeWebService()
    {
        this(ServiceProxyFactory.getInstance().getUserUsercodeServiceProxy());
    }
    public UserUsercodeWebService(UserUsercodeService service)
    {
        mService = service;
    }
    
    private UserUsercodeService getService()
    {
        if(mService == null) {
            mService = ServiceProxyFactory.getInstance().getUserUsercodeServiceProxy();
        }
        return mService;
    }
    
    
    public UserUsercodeJsBean getUserUsercode(String guid) throws WebException
    {
        try {
            UserUsercode userUsercode = getService().getUserUsercode(guid);
            UserUsercodeJsBean bean = convertUserUsercodeToJsBean(userUsercode);
            return bean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Object getUserUsercode(String guid, String field) throws WebException
    {
        try {
            return getService().getUserUsercode(guid, field);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserUsercodeJsBean> getUserUsercodes(List<String> guids) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<UserUsercodeJsBean> jsBeans = new ArrayList<UserUsercodeJsBean>();
            List<UserUsercode> userUsercodes = getService().getUserUsercodes(guids);
            if(userUsercodes != null) {
                for(UserUsercode userUsercode : userUsercodes) {
                    jsBeans.add(convertUserUsercodeToJsBean(userUsercode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserUsercodeJsBean> getAllUserUsercodes() throws WebException
    {
        return getAllUserUsercodes(null, null, null);
    }

    public List<UserUsercodeJsBean> getAllUserUsercodes(String ordering, Long offset, Integer count) throws WebException
    {
        try {
            List<UserUsercodeJsBean> jsBeans = new ArrayList<UserUsercodeJsBean>();
            List<UserUsercode> userUsercodes = getService().getAllUserUsercodes(ordering, offset, count);
            if(userUsercodes != null) {
                for(UserUsercode userUsercode : userUsercodes) {
                    jsBeans.add(convertUserUsercodeToJsBean(userUsercode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> getAllUserUsercodeKeys(String ordering, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().getAllUserUsercodeKeys(ordering, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<UserUsercodeJsBean> findUserUsercodes(String filter, String ordering, String params, List<String> values) throws WebException
    {
        return findUserUsercodes(filter, ordering, params, values, null, null, null, null);
    }

    public List<UserUsercodeJsBean> findUserUsercodes(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        try {
            List<UserUsercodeJsBean> jsBeans = new ArrayList<UserUsercodeJsBean>();
            List<UserUsercode> userUsercodes = getService().findUserUsercodes(filter, ordering, params, values, grouping, unique, offset, count);
            if(userUsercodes != null) {
                for(UserUsercode userUsercode : userUsercodes) {
                    jsBeans.add(convertUserUsercodeToJsBean(userUsercode));
                }
            }
            return jsBeans;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public List<String> findUserUsercodeKeys(String filter, String ordering, String params, List<String> values, String grouping, Boolean unique, Long offset, Integer count) throws WebException
    {
        log.finer("BEGIN");

        try {
            List<String> guids = getService().findUserUsercodeKeys(filter, ordering, params, values, grouping, unique, offset, count);
            return guids;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long getCount(String filter, String params, List<String> values, String aggregate) throws WebException
    {
        try {
            Long count = getService().getCount(filter, params, values, aggregate);
            return count;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserUsercode(String admin, String user, String username, String usercode, String status) throws WebException
    {
        try {
            return getService().createUserUsercode(admin, user, username, usercode, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public String createUserUsercode(UserUsercodeJsBean jsBean) throws WebException
    {
        try {
            UserUsercode userUsercode = convertUserUsercodeJsBeanToBean(jsBean);
            return getService().createUserUsercode(userUsercode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserUsercodeJsBean constructUserUsercode(UserUsercodeJsBean jsBean) throws WebException
    {
        try {
            UserUsercode userUsercode = convertUserUsercodeJsBeanToBean(jsBean);
            userUsercode = getService().constructUserUsercode(userUsercode);
            jsBean = convertUserUsercodeToJsBean(userUsercode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean updateUserUsercode(String guid, String admin, String user, String username, String usercode, String status) throws WebException
    {
        try {
            return getService().updateUserUsercode(guid, admin, user, username, usercode, status);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }
        
    public Boolean updateUserUsercode(UserUsercodeJsBean jsBean) throws WebException
    {
        try {
            UserUsercode userUsercode = convertUserUsercodeJsBeanToBean(jsBean);
            return getService().updateUserUsercode(userUsercode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public UserUsercodeJsBean refreshUserUsercode(UserUsercodeJsBean jsBean) throws WebException
    {
        try {
            UserUsercode userUsercode = convertUserUsercodeJsBeanToBean(jsBean);
            userUsercode = getService().refreshUserUsercode(userUsercode);
            jsBean = convertUserUsercodeToJsBean(userUsercode);
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserUsercode(String guid) throws WebException
    {
        try {
            return getService().deleteUserUsercode(guid);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Boolean deleteUserUsercode(UserUsercodeJsBean jsBean) throws WebException
    {
        try {
            UserUsercode userUsercode = convertUserUsercodeJsBeanToBean(jsBean);
            return getService().deleteUserUsercode(userUsercode);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public Long deleteUserUsercodes(String filter, String params, List<String> values) throws WebException
    {
        try {
            return getService().deleteUserUsercodes(filter, params, values);
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    public static UserUsercodeJsBean convertUserUsercodeToJsBean(UserUsercode userUsercode)
    {
        UserUsercodeJsBean jsBean = null;
        if(userUsercode != null) {
            jsBean = new UserUsercodeJsBean();
            jsBean.setGuid(userUsercode.getGuid());
            jsBean.setAdmin(userUsercode.getAdmin());
            jsBean.setUser(userUsercode.getUser());
            jsBean.setUsername(userUsercode.getUsername());
            jsBean.setUsercode(userUsercode.getUsercode());
            jsBean.setStatus(userUsercode.getStatus());
            jsBean.setCreatedTime(userUsercode.getCreatedTime());
            jsBean.setModifiedTime(userUsercode.getModifiedTime());
        }
        return jsBean;
    }

    public static UserUsercode convertUserUsercodeJsBeanToBean(UserUsercodeJsBean jsBean)
    {
        UserUsercodeBean userUsercode = null;
        if(jsBean != null) {
            userUsercode = new UserUsercodeBean();
            userUsercode.setGuid(jsBean.getGuid());
            userUsercode.setAdmin(jsBean.getAdmin());
            userUsercode.setUser(jsBean.getUser());
            userUsercode.setUsername(jsBean.getUsername());
            userUsercode.setUsercode(jsBean.getUsercode());
            userUsercode.setStatus(jsBean.getStatus());
            userUsercode.setCreatedTime(jsBean.getCreatedTime());
            userUsercode.setModifiedTime(jsBean.getModifiedTime());
        }
        return userUsercode;
    }

}
