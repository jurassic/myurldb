package com.cannyurl.rw.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import com.myurldb.ws.BaseException;
import com.myurldb.ws.WebProfileStruct;
import com.cannyurl.af.bean.WebProfileStructBean;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.WebProfileStructJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class WebProfileStructWebService // implements WebProfileStructService
{
    private static final Logger log = Logger.getLogger(WebProfileStructWebService.class.getName());
     
    public static WebProfileStructJsBean convertWebProfileStructToJsBean(WebProfileStruct webProfileStruct)
    {
        WebProfileStructJsBean jsBean = null;
        if(webProfileStruct != null) {
            jsBean = new WebProfileStructJsBean();
            jsBean.setUuid(webProfileStruct.getUuid());
            jsBean.setType(webProfileStruct.getType());
            jsBean.setServiceName(webProfileStruct.getServiceName());
            jsBean.setServiceUrl(webProfileStruct.getServiceUrl());
            jsBean.setProfileUrl(webProfileStruct.getProfileUrl());
            jsBean.setNote(webProfileStruct.getNote());
        }
        return jsBean;
    }

    public static WebProfileStruct convertWebProfileStructJsBeanToBean(WebProfileStructJsBean jsBean)
    {
        WebProfileStructBean webProfileStruct = null;
        if(jsBean != null) {
            webProfileStruct = new WebProfileStructBean();
            webProfileStruct.setUuid(jsBean.getUuid());
            webProfileStruct.setType(jsBean.getType());
            webProfileStruct.setServiceName(jsBean.getServiceName());
            webProfileStruct.setServiceUrl(jsBean.getServiceUrl());
            webProfileStruct.setProfileUrl(jsBean.getProfileUrl());
            webProfileStruct.setNote(jsBean.getNote());
        }
        return webProfileStruct;
    }

}
