package com.cannyurl.web.proxy;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;

import com.cannyurl.af.util.MarshalHelper;
import com.cannyurl.af.util.StringUtil;
import com.cannyurl.rf.auth.TwoLeggedOAuthClientUtil;
import com.cannyurl.rf.proxy.AbstractBaseServiceProxy;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortLink;
import com.myurldb.ws.core.StatusCode;
import com.myurldb.ws.stub.ErrorStub;
import com.myurldb.ws.stub.ShortLinkStub;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


// Note: The real reason for "web" packages (web/proxy, web/service, web/servlet)
//       is to introduct convenience wrappers for "findXXX()" methods....


public class ShortLinkWebServiceProxy extends AbstractBaseServiceProxy
{
    private static final Logger log = Logger.getLogger(ShortLinkWebServiceProxy.class.getName());

    // TBD: Ths resource name should match the path specified in the corresponding app.resource class.
    private final static String RESOURCE_SHORTLINK = "shortlink";


    // Cache service
    private Cache mCache = null;

    public ShortLinkWebServiceProxy()
    {
        try {
            Map<String, Object> props = new HashMap<String, Object>();
            props.put(GCacheFactory.EXPIRATION_DELTA, 1800);     // TBD: Get this from Config...
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            mCache = cacheFactory.createCache(props);
        } catch (CacheException e) {
            log.log(Level.WARNING, "Failed to create Cache service.", e);
        }
    }


    protected WebResource getShortLinkWebResource()
    {
        return getWebResource(RESOURCE_SHORTLINK);
    }
    protected WebResource getShortLinkWebResource(String path)
    {
        return getWebResource(RESOURCE_SHORTLINK, path);
    }
    protected WebResource getShortLinkWebResourceByGuid(String guid)
    {
        return getShortLinkWebResource(guid);
    }


    public ShortLink constructShortLink(String longUrl) throws BaseException
    {
        return constructShortLink(longUrl, null, null, null);
    }
    public ShortLink constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode) throws BaseException
    {
        return constructShortLink(longUrl, user, username, usercode, null, null);
    }
    public ShortLink constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType) throws BaseException
    {
        return constructShortLink(longUrl, user, username, usercode, domain, domainType, null, null, null);
    }
    public ShortLink constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType, 
            String token, 
            String tokenType, 
            String sassyTokenType) throws BaseException
    {
        return constructShortLink(longUrl, user, username, usercode, domain, domainType, token, tokenType, sassyTokenType, null, null);
    }
    public ShortLink constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType, 
            String token, 
            String tokenType, 
            String sassyTokenType, 
            String redirectType, 
            Long flashDuration) throws BaseException
    {
        return constructShortLink(longUrl, user, username, usercode, domain, domainType, token, tokenType, sassyTokenType, redirectType, flashDuration, null);
    }
    public ShortLink constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType, 
            String token, 
            String tokenType, 
            String sassyTokenType, 
            String redirectType, 
            Long flashDuration,
            String shortMessage) throws BaseException
    {
    	// ???
//        String key = getResourcePath(RESOURCE_SHORTLINK, longUrl);
        if(mCache != null) {
//            CacheEntry entry = mCache.getCacheEntry(key);
//            if(entry != null) {
//                // TBD: eTag, lastModified, expires, etc....
//                // ???
//                Object obj = entry.getValue();
//                if(obj instanceof ShortLinkBean) {
//                    log.info("shortLink returned from cache! key = " + key);
//                    return (ShortLinkBean) obj;
//                }
//            }
        }

        ShortLinkStub stub = new ShortLinkStub();
    	WebResource webResource = getShortLinkWebResource();

    	if(longUrl == null || longUrl.isEmpty()) {
    	    // Should we allow empty longUrl????
    	} else {
    	    webResource = webResource.queryParam("longUrl", longUrl);
    	    stub.setLongUrl(longUrl);
    	}
        if(user != null) {
            webResource = webResource.queryParam("user", user);
            stub.setOwner(user);
        }
        if(username != null) {
            webResource = webResource.queryParam("username", username);
            stub.setUsername(username);
        }
        if(usercode != null) {
            webResource = webResource.queryParam("usercode", usercode);
            stub.setUsercode(usercode);
        }
        if(domain != null) {
            webResource = webResource.queryParam("domain", domain);
            stub.setDomain(domain);
        }
        if(domainType != null) {
            webResource = webResource.queryParam("domainType", domainType);
            stub.setDomainType(domainType);
        }
        if(token != null) {
            webResource = webResource.queryParam("token", token);
            stub.setToken(token);
        }
        if(tokenType != null) {
            webResource = webResource.queryParam("tokenType", tokenType);
            stub.setTokenType(tokenType);
        }
        if(sassyTokenType != null) {
            webResource = webResource.queryParam("sassyTokenType", sassyTokenType);
            stub.setSassyTokenType(sassyTokenType);
        }
        if(redirectType != null) {
            webResource = webResource.queryParam("redirectType", redirectType);
            stub.setRedirectType(redirectType);
        }
        if(flashDuration != null) {
            webResource = webResource.queryParam("flashDuration", flashDuration.toString());
            stub.setFlashDuration(flashDuration);
        }
        if(shortMessage != null) {
            webResource = webResource.queryParam("shortMessage", shortMessage);
            stub.setShortMessage(shortMessage);
        }
        // ...

        // Return value.
        ShortLink shortLink = null;

        // TBD: Based on config.... ?
        webResource = TwoLeggedOAuthClientUtil.addClientFilter(webResource);
        ClientResponse clientResponse = webResource.type(getInputMediaType()).accept(getOutputMediaType()).post(ClientResponse.class);
        int status = clientResponse.getStatus();
        if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "Status = " + status);
        if(StatusCode.isSuccessful(status)) {
            switch(status) {
            case StatusCode.CREATED:
            case StatusCode.ACCEPTED:
                stub = clientResponse.getEntity(ShortLinkStub.class);
                shortLink = MarshalHelper.convertShortLinkToBean(stub);
                if(log.isLoggable(Level.FINE)) log.log(Level.FINE, "New ShortLink shortLink = " + shortLink);
                String createdUri = clientResponse.getLocation().toString();
                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "New ShortLink resource uri = " + createdUri);
                break;
            default:
                String response = clientResponse.getEntity(String.class); // ???
                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unexpected response: " + response);
            	throw new BaseException("Unexpected response: " + response);
            }
        } else if (StatusCode.isError(status)) {
            ErrorStub error = null;
        	try {
                error = clientResponse.getEntity(ErrorStub.class);
        	} catch(Exception e) {
        		log.log(Level.WARNING, "Failed to unmarshal ErrorStub: ", e);
        		try {
            		InputStream is = clientResponse.getEntityInputStream();
		            String errorStr = StringUtil.readInputStream(is);
		            error = new ErrorStub();
		            error.setMessage(errorStr);
		            error.setResource(RESOURCE_SHORTLINK);  // ???
				} catch (Exception e1) {
	        		log.log(Level.WARNING, "Failed to read clientResponse error entity input stream: ", e1);
	        		throw new BaseException("Failed to unmarshal ErrorStub, and failed to read clientResponse error entity input stream.", e1);
				}        		
        	}
            handleError(status, error);
        } else {
            String response = clientResponse.getEntity(String.class); // ???
            if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Unrecognized response: " + response);
        	throw new BaseException("Unrecognized response:" + response);
        }
        return shortLink;
    }


}
