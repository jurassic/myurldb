package com.cannyurl.web.service;

import java.util.logging.Logger;

import com.cannyurl.af.service.ShortLinkService;
import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.rf.proxy.ServiceProxyFactory;
import com.cannyurl.rw.service.ShortLinkWebService;
import com.cannyurl.web.proxy.ShortLinkWebServiceProxy;
import com.myurldb.ws.BaseException;
import com.myurldb.ws.ShortLink;


// Note: The real reason for "web" packages (web/proxy, web/service, web/servlet)
//       is to introduct convenience wrappers for "findXXX()" methods....


// Does not maintain any state. (For now)
// Hmm... Make it a singleton???
public class ShortLinkProxyWebService
{
    private static final Logger log = Logger.getLogger(ShortLinkProxyWebService.class.getName());
     
    // Af service interface.
    private ShortLinkWebServiceProxy mServiceProxy = null;

    public ShortLinkProxyWebService()
    {
        //this(new ShortLinkWebServiceProxy());
        this(null);
    }
    public ShortLinkProxyWebService(ShortLinkWebServiceProxy serviceProxy)
    {
        mServiceProxy = serviceProxy;
    }
    
    private ShortLinkWebServiceProxy getServiceProxy()
    {
        if(mServiceProxy == null) {
            mServiceProxy = new ShortLinkWebServiceProxy();
        }
        return mServiceProxy;
    }
    

    public ShortLinkJsBean constructShortLink(String longUrl) throws WebException
    {
        return constructShortLink(longUrl, null, null, null);
    }
    public ShortLinkJsBean constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode) throws WebException
    {
        return constructShortLink(longUrl, user, username, usercode, null, null);
    }
    public ShortLinkJsBean constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType) throws WebException
    {
        return constructShortLink(longUrl, user, username, usercode, domain, domainType, null, null, null);
    }
    public ShortLinkJsBean constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType, 
            String token, 
            String tokenType, 
            String sassyTokenType) throws WebException
    {
        return constructShortLink(longUrl, user, username, usercode, domain, domainType, token, tokenType, sassyTokenType, null, null);
    }
    public ShortLinkJsBean constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType, 
            String token, 
            String tokenType, 
            String sassyTokenType, 
            String redirectType, 
            Long flashDuration) throws WebException
    {
        return constructShortLink(longUrl, user, username, usercode, domain, domainType, token, tokenType, sassyTokenType, redirectType, flashDuration, null);
    }
    public ShortLinkJsBean constructShortLink(String longUrl, 
            String user,
            String username,
            String usercode,
            String domain, 
            String domainType, 
            String token, 
            String tokenType, 
            String sassyTokenType, 
            String redirectType, 
            Long flashDuration,
            String shortMessage) throws WebException
    {
        try {
            ShortLinkJsBean jsBean = null;
            ShortLink shortLink = getServiceProxy().constructShortLink(longUrl, user, username, usercode, domain, domainType, token, tokenType, sassyTokenType, redirectType, flashDuration, shortMessage);
            if(shortLink != null) {
                jsBean = ShortLinkWebService.convertShortLinkToJsBean(shortLink);
            }
            return jsBean;
        } catch(BaseException bex) {
            throw new WebException(bex);
        }
    }

    
}
