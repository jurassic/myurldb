package com.cannyurl.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cannyurl.fe.WebException;
import com.cannyurl.fe.bean.ShortLinkJsBean;
import com.cannyurl.rw.service.ShortLinkWebService;
import com.cannyurl.rw.service.UserWebService;
import com.cannyurl.web.service.ShortLinkProxyWebService;
import com.myurldb.ws.core.StatusCode;


// Note: The real reason for "web" packages (web/proxy, web/service, web/servlet)
//       is to introduct convenience wrappers for "findXXX()" methods....


public class ShortLinkServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(ShortLinkServlet.class.getName());
    
    // temporary
    private static final String QUERY_PARAM_LONGURL = "longUrl";
    private static final String QUERY_PARAM_DOMAIN = "domain";
    private static final String QUERY_PARAM_DOMAINTYPE = "domainType";
    private static final String QUERY_PARAM_USER = "user";
    private static final String QUERY_PARAM_USERNAME = "username";
    private static final String QUERY_PARAM_USERCODE = "usercode";
    private static final String QUERY_PARAM_TOKEN = "token";
    private static final String QUERY_PARAM_TOKENTYPE = "tokenType";
    private static final String QUERY_PARAM_SASSYTOKENTYPE = "sassyTokenType";
    private static final String QUERY_PARAM_REDIRECTTYPE = "redirectType";
    private static final String QUERY_PARAM_FLASHDURATION = "flashDuration";
    private static final String QUERY_PARAM_SHORTMESSAGE = "shortMessage";

    
    // TBD: Is this safe for concurrent calls???
    private UserWebService userWebService = null;
    private ShortLinkWebService shortLinkWebService = null;
    // etc...

    private UserWebService getUserService()
    {
        if(userWebService == null) {
            userWebService = new UserWebService();
        }
        return userWebService;
    }
    private ShortLinkWebService getShortLinkService()
    {
        if(shortLinkWebService == null) {
            shortLinkWebService = new ShortLinkWebService();
        }
        return shortLinkWebService;
    }
    // etc. ...

    

    @Override
    public void init() throws ServletException
    {
        super.init();        
    }

    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        log.info("doPost(): TOP");
   
        // TBD:
        String requestUrl = req.getRequestURL().toString();
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        String queryString = req.getQueryString();
        if(log.isLoggable(Level.FINE)) log.fine("requestUrl = " + requestUrl);
        if(log.isLoggable(Level.FINE)) log.fine("contextPath = " + contextPath);
        if(log.isLoggable(Level.FINE)) log.fine("servletPath = " + servletPath);
        if(log.isLoggable(Level.FINE)) log.fine("pathInfo = " + pathInfo);
        if(log.isLoggable(Level.FINE)) log.fine("queryString = " + queryString);

        String longUrl = req.getParameter(QUERY_PARAM_LONGURL);
        if(longUrl == null || longUrl.isEmpty()) {
            resp.setStatus(StatusCode.BAD_REQUEST);  // ???
            return;
        }
        if(log.isLoggable(Level.FINE)) log.fine("longUrl = " + longUrl);

        String user = req.getParameter(QUERY_PARAM_USER);
        if(log.isLoggable(Level.FINE)) log.fine("user = " + user);
        String username = req.getParameter(QUERY_PARAM_USERNAME);
        if(log.isLoggable(Level.FINE)) log.fine("username = " + username);
        String usercode = req.getParameter(QUERY_PARAM_USERCODE);
        if(log.isLoggable(Level.FINE)) log.fine("usercode = " + usercode);
        String domain = req.getParameter(QUERY_PARAM_DOMAIN);
        if(log.isLoggable(Level.FINE)) log.fine("domain = " + domain);
        String domainType = req.getParameter(QUERY_PARAM_DOMAINTYPE);
        if(log.isLoggable(Level.FINE)) log.fine("domainType = " + domainType);
        String token = req.getParameter(QUERY_PARAM_TOKEN);
        if(log.isLoggable(Level.FINE)) log.fine("token = " + token);
        String tokenType = req.getParameter(QUERY_PARAM_TOKENTYPE);
        if(log.isLoggable(Level.FINE)) log.fine("tokenType = " + tokenType);
        String sassyTokenType = req.getParameter(QUERY_PARAM_SASSYTOKENTYPE);
        if(log.isLoggable(Level.FINE)) log.fine("sassyTokenType = " + sassyTokenType);
        String redirectType = req.getParameter(QUERY_PARAM_REDIRECTTYPE);
        if(log.isLoggable(Level.FINE)) log.fine("redirectType = " + redirectType);
        String strFlashDuration = req.getParameter(QUERY_PARAM_FLASHDURATION);
        Long flashDuration = null;
        try {
            flashDuration = Long.parseLong(strFlashDuration);
        } catch(Exception e) {
            // Ignore
            if(log.isLoggable(Level.INFO)) log.info("Failed to parse flashDuration: " + strFlashDuration);
        }
        if(log.isLoggable(Level.FINE)) log.fine("flashDuration = " + flashDuration);
        String shortMessage = req.getParameter(QUERY_PARAM_SHORTMESSAGE);
        if(log.isLoggable(Level.FINE)) log.fine("shortMessage = " + shortMessage);

        
        ShortLinkProxyWebService proxyWebService = new ShortLinkProxyWebService();
        ShortLinkJsBean shortLink = null;
        try {
            shortLink = proxyWebService.constructShortLink(longUrl, user, username, usercode, domain, domainType, token, tokenType, sassyTokenType, redirectType, flashDuration, shortMessage);
        } catch (WebException e) {
            log.log(Level.WARNING, "Failed to retrieve shortLink", e);
        }
       
        if(shortLink != null) {
            String jsonStr = shortLink.toJsonString();
            resp.setContentType("application/json");  // ????
            PrintWriter writer = resp.getWriter();
            writer.print(jsonStr);
            resp.setStatus(StatusCode.OK);  
        } else {
            resp.setStatus(StatusCode.NOT_FOUND);  // ???
        }
        // ...
        
        log.info("doPost(): BOTTOM");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        throw new ServletException("Not implemented.");
    }
    
    
}
